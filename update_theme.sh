#!/bin/bash

THEME_DIR=themes/plandocument
VERSION=0.1.5

mkdir -p themes
if [ -d "$THEME_DIR" ]; then rm -Rf $THEME_DIR; fi
unzip /l/Hugo/plandocument-$VERSION.zip -d $THEME_DIR
