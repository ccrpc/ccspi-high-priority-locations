---
title: "County Road 2200 E and Homer Lake Road"
draft: false
weight: 10
bannerHeading: County Road 2200 E and Homer Lake Road.
bannerText: >
  The intersection of County Road 2200 E and Homer Lake Road
bannerAction: Provide Feedback
bannerUrl: /contact
---

## Introduction
### Background

This study was performed to analyze the safety of the intersection of County Road 2200 E and Homer Lake Road, an unsignalized intersection in rural Champaign County. **Map 1** shows the location and aerial view of the intersection. From the intersection, Downtown St. Joseph is accessible by driving north along County Road 2200 E, while the Homer Lake Forest Preserve is located to the east along Homer Lake Road.

<iframe src="https://maps.ccrpc.org/ccspi-rural-intx-homerlake/" width="100%"  height="600" allowfullscreen="true"></iframe>

Both County Road 2200 E and Homer Lake Road have limited access to adjacent land uses. There are two private driveways on the east approach of the intersection—one is approximately 150 ft away, and the other is approximately 350 ft away. There is no roadway access within 150 ft of the intersection on the north, south, and west approaches of the intersection.

## Existing Conditions
This existing conditions analysis evaluates the physical and operational conditions of the intersection and its roadway approaches. 

The study intersection is an All-Way Stop-Controlled (AWSC) intersection with a skew angle of about 72 degrees. Until June 2016, it was a Two-Way Stop-Controlled (TWSC) intersection with stop signs placed on Homer Lake Road; at this point stop signs were also added to County Road 2200 E. A single median centerline and stop line are present only at the south approach of this intersection. There are no median centerlines and no stop line markings on the north, east, and west approaches. Both County Road 2200 E and Homer Lake Road are undivided two-lane roadways that function as minor arterials. The lane width of Homer Lake Road is about 10 feet, and the lane width of County Road 2200 E is about 12 feet. Lighting is provided on the east approach of the intersection. There are no obstacle objects along the roadways. **Figure 1** and **Table 1** summarize the control type, the geometry, and the physical condition of the intersection and the corresponding approaches.    

{{<image src="control_diagram_2.png" 
    caption="Figure 1 - Intersection Overview"
    position="medium" 
    alt=" ">}}

<rpc-table url="RuralIntxPhysicalCond.csv" table-title="Table 1 - Geometry and Physical Condition of the Intersection Approaches"> </rpc-table>

{{<accordion>}}
  {{<accordion-content title="Intersection corners">}}

  Figure 2.1-Figure 2.2 show the physical conditions of the intersection corners. 
  <br></br>
  {{<image src="RuralIntx_IntxCorner1.PNG" 
    caption="Figure 2.1 - Intersection of County Road 2200 E and Homer Lake Road (looking NW from SE corner of the intersection)"
    position="medium" 
    alt=" ">}}

  {{<image src="RuralIntx_IntxCorner2.PNG" 
    caption="Figure 2.2 - Intersection of County Road 2200 E and Homer Lake Road (NE corner of the intersection)"
    position="medium"
    alt=" ">}}
  {{</accordion-content>}}

  {{<accordion-content title="Pavement condition">}}
 
    Pavement failures such as cracking, potholes, depressions, rutting, shoving, upheavals, and ravelling can cause roadway safety problems such as skidding, vehicle vibrations, driving off-road, and improper maneuvers. It has been shown in many studies that poor pavement conditions increase the chance of vehicle crashes. Regular inspections and maintenance work are needed so that crashes caused by pavement deterioration and defects can be avoided. 
    <br></br>
    Figure 3 shows the pavement condition of the east approach. As seen in the figure, the east approach of the intersection has issues of fatigue cracking, transverse cracking, and raveling on the edge of the road. Efforts need to be made to improve the pavement condition of this approach.

  {{<image src="RuralIntx_WB_PaveCond.PNG" 
    caption="Figure 3 - Pavement condition"
    position="medium" 
    alt="Photo of degrading pavement along the shoulder of the east approach">}}
  {{</accordion-content>}}

{{</accordion>}}

## Operational Conditions
Traffic volume data was collected at the intersection of County Road 2200 E and Homer Lake Road from 7:00 AM to 7:00 PM on Thursday, February 25, 2021. Most vehicles traveling through the intersection were passenger vehicles. The percentage of heavy vehicles (trucks) counted was less than 2%. The 15-minute vehicle traffic counts at each approach are shown in Figure 4. For the intersection as a whole, the AM peak hour is 7:00 AM – 8:00 AM and the PM peak is 4:45 PM – 5:45 PM. The turning movement counts for each direction are shown in Figure 5.

<rpc-chart url="RuralIntxTrafficCounts.csv"
    chart-title="Figure 4 - Vehicle Traffic Volume at the Intersection of County Road 2200 E and Homer Lake Road"
    x-label="Time"
    y-label="Traffic Counts"
    type="line"></rpc-chart>

  {{<image src="turning_movements.png" 
    caption="Figure 5 - Turning Movement Counts"
    position="medium"
    alt="Diagram of turning movement counts, showing the vehicle traffic and direction of travel through the intersection for a 12 hour period, in the AM peak hour, and in the PM peak hour">}}


{{<accordion>}}

  {{<accordion-content title="Level-Of-Service">}}
    Selected intersection criteria such as Level-of-Service (LOS), approach delay, and intersection delay were analyzed to determine the existing operational conditions during the morning and afternoon peak hours on a typical weekday. LOS is a qualitative measure describing operational conditions—from “A” (best) to “F” (worst)—within a traffic stream or at an intersection. LOS is quantified for signalized and unsignalized intersections using vehicle control delay. LOS A represents free flow along the intersection with minimal delay, LOS B represents stable flow with slight delays, LOS C indicates stable flow with acceptable delays, LOS D represents an approaching unstable flow with tolerable delay (e.g. travelers must occasionally wait through more than one signal cycle before proceeding), LOS E indicates unstable flow with an approaching intolerable delay, and LOS F represents forced or jammed flow. Table 1 describes the LOS criteria for two-way and all-way stop controlled intersections.

  <rpc-table url="UnsigIntxLOSCriteria.csv" 
    table-title="Table 1 - LOS Criteria for Unsignalized Intersections" text-alignment="c,c"
    source="HCM 2010"> </rpc-table>

    Control delay is the component of delay that results from the type of traffic control at the intersection: the difference between the travel time that would have occurred in the absence of the intersection control and the travel time that results from the presence of the intersection control. Average control delay per vehicle is estimated for each lane group, then aggregated for each approach and for the intersection as a whole. Table 2 shows LOS, approach delays, and intersection delays for this intersection during morning and afternoon peak hours.

  <rpc-table url="RuralIntxLOS.csv" 
    table-title="Table 2 - LOS at the Intersection of County Road 2200 E and Homer Lake Road" text-alignment="c,c"> </rpc-table>
  {{</accordion-content>}}

  {{<accordion-content title="Speed Analysis">}}
    IDOT provides speed data for both roadways at this intersection. The speed data for County Road 2200 E (Northbound and Southbound) was collected in 2018 while the speed data for Homer Lake Road (Eastbound and Westbound) was collected in 2016. The posted speed limit on all four approaches is 55mph. Table 3 presents the overall average speed and 85th percentile speed on the four approaches. For every approach, both the average speed and 85th percentile speed are lower than the posted speed limit.
    <br></br>
    <rpc-table url="RuralIntxSpeedData.csv" table-title="Table 3 - Speed Data for Each Approach" text-alignment="c,c"> </rpc-table>
  {{</accordion-content>}}

{{</accordion>}}


## Pedestrian and Bicycle Conditions

In the 12-hour data collection period, no pedestrians and two bicyclists were observed using the intersection. The bicyclists traveled along the E/W direction between 4:00 PM and 5:00 PM at two different times. One bicyclist, observing a vehicle approaching the intersection, stopped at the stop sign. The other did not stop at the stop sign, traveling quickly across the intersection while no other vehicles approached. 

## Crash Analysis

Crash data from 2014 to 2018 was analyzed for the intersection of County Road 2200 E and Homer Lake Road. The crash data information was obtained from the Illinois Department of Transportation Division of Traffic Safety. There were 11 crashes at the study location over the 5-year study period. **Figure 6** shows the collision diagram for the intersection.  

{{<image src="collision_diagram.png" 
    caption="Figure 6 - Collision Diagram"
    position="medium"
    alt="Map showing the crashes, by type, for the intersection and its surrounding approaches">}} 

### Crash Trends and Time
**Figure 7.1-7.4** show the timing of crashes that occurred at the intersection during this five-year span.  These indicate that the highest number of crashes occurred in 2015. Friday and Saturday were the days of the week with the highest number of crashes. The hour of highest crash frequency was between 12PM and 1PM. 

<rpc-chart url="crash_count_year.csv"
    chart-title="Figure 7.1 - Number of Crashes by Year"
    x-label="Year"
    y-label="Crash Counts"
    type="bar"></rpc-chart>

<rpc-chart url="crash_count_month.csv"
    chart-title="Figure 7.2 - Number of Crashes by Month"
    x-label="Month"
    y-label="Crash Counts"
    type="bar"></rpc-chart>

<rpc-chart url="crash_count_day.csv"
    chart-title="Figure 7.3 - Number of Crashes by Day of the Week"
    x-label="Day"
    y-label="Crash Counts"
    type="bar"></rpc-chart>

<rpc-chart url="crash_count_hour.csv"
      chart-title="Figure 7.4 - Number of Crashes by Hour of the Day"
      x-label="Hour"
      y-label="Crash Counts"
      type="bar"></rpc-chart>

### Crash Severity

Crash severity levels are generally classified on the KABCO scale: Fatal ( K ), A-Injury ( A ), B-Injury ( B ), C-Injury ( C ), and No Injury ( O ). Definitions for these KABCO severity levels can be found in the Appendix. **Table 4** and **Figure 8** present the number of crashes by severity level at the study location.  There were no fatal or A-injury crashes at this intersection from 2014 to 2018.  B-Injury crashes accounted for 27% of the total crashes, C-Injury crashes for 18%, and No-Injury crashes for 55%.

<rpc-table url="crash_severity_year.csv"
    table-title="Table 4 - Number of Crashes by Severity Level and Year"
    text-alignment="c,c"></rpc-table>

<rpc-chart url="crash_severity_year_chart.csv"
    chart-title="Figure 8 - Number of Crashes by Severity Level and Year"
    x-label="Severity Level"
    y-label="Crash Counts"
    type="bar"
    stacked="true"></rpc-chart>

### Crash Types

**Table 5** and **Figure 9** show the count of crashes at the intersection from 2014 to 2018. **Table 6** and **Figure 10** show each crash type by the number of crashes for each severity level. Angle crashes represented the majority of crashes at this intersection, accounting for 64% of all crashes. At the intersection of County Road 2200 E and Homer Lake Road, all the B-Injury crashes and C-Injury crashes were caused by angle crashes. Most angle crashes occurred in 2015, when stop control signs were located only on the east and west approaches. Both animal crashes happened in the fall; further analysis by time of day shows that one was at night in September, and one was at dusk in October.  

<rpc-table url="crash_type_year.csv"
  table-title="Table 5 - Number of Crashes by Crash Type and Year "
  text-alignment="c,c"></rpc-table>

<rpc-chart url="crash_type_year_chart.csv"
      chart-title="Figure 9 - Number of Crashes by Crash Type and Year"
      x-label="Crash Type"
      y-label="Crash Counts"
      type="bar"
      stacked="true"></rpc-chart>

<rpc-table url="crash_severity_type.csv"
  table-title="Table 6 - Number of Crashes by Crash Type and Severity Level "
  text-alignment="c,c"></rpc-table>

<rpc-chart url="crash_severity_type_chart.csv"
      chart-title="Figure 10 - Number of Crashes by Crash Type and Severity Level"
      x-label="Severity Level"
      y-label="Crash Counts"
      type="bar"
      stacked="true"></rpc-chart>

Among all seven angle crashes, five occurred before June 2016, in the period when stop signs were located only on the E/W approaches (Homer Lake Road). Among these five crashes, three occurred on approaches with an obtuse angle, and they were caused by the driver on the E/W approach failing to judge the distance of incoming vehicles from the N/S approach correctly or not seeing the incoming vehicles. The other two crashes occurred on approaches with an acute angle. One crash was caused by the lack of stop signs on the N/S approach. The other crash had the same cause, in addition to the driver on the E/W approach slipping into the intersection because of ice.  
    
After stop signs were installed on the N/S approaches and the intersection corner was cleaned for field of vision improvement, only 2 angle crashes have occurred. Both crashes happened at dusk in the summer, and both were caused by drivers on the E/W approaches running the stop sign—one failed to observe the stop sign, and the other one could not see the stop sign because of sun glare. 

## Findings and Proposed Countermeasures

### Findings

The following findings are summarized based on the detailed evaluation of the intersection at County Road 2200 E and Homer Lake Road: 

- Angle crashes represent the majority of crashes at this intersection. 
- Safety issues from limited field of vision and the lack of stop signs on the N/S approaches seem to have been partially resolved in 2016. 
- Safety issues from drivers missing or disobeying stop signs on the E/W approaches still exist.

### Proposed Countermeasures

The key solutions for improving safety at the study intersection are improving drivers’ awareness of the stop signs on the E/W approaches and alerting drivers to make a complete stop at the intersection. To improve safety at the intersection in the long term, realigning the intersection to increase drivers’ visibility should also be considered.

Below are recommended strategies: 

{{<image src="RuralIntx-Recom.PNG" position="medium" alt="Table providing the following recommendations (1)	Adjust the size and placement of the stop signs on the east and west approaches to improve drivers’ awareness of the stop signs. (2)	Provide stop line markings on the east, west, and north approaches. (3)	Install “STOP” pavement markings on the east and west approaches.  (4)	Add flashing warning beacons to the stop signs on the east and west approaches. If budget allows, add flashing warning beacons to the stop signs on the north and south approaches. (5)	Install transverse rumble strips on the east and west approaches to encourage drivers to reduce speed earlier. Drivers would then have more time and chances to notice and respond to the stop signs. (6)	Realign the intersection to improve drivers’ field of vision.">}} 

