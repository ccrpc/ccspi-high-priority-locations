---
title: "Rural Intersection"
date: 2018-01-18T11:32:26-06:00
draft: false
menu: main
weight: 20
bannerHeading: Rural Intersection
bannerText: >
bannerAction: Provide Feedback
bannerUrl: /contact
---

In the rural area, only one intersection was identified as a high-priority location. The identified intersection is the intersection of County Road 2200 E and Homer Lake Road. 