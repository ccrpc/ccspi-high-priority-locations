---
title: "Prospect Avenue and Bloomington Road"
draft: false
weight: 10
bannerHeading: Prospect Avenue and Bloomington Road
bannerText: >
  The intersection of Prospect Avenue and Bloomington Road.
bannerAction: Provide Feedback
bannerUrl: /contact
---

## Introduction

This study analyzes the safety of the intersection of Prospect Avenue and Bloomington Road. This intersection provides immediate access to Interstate 74 in fewer than 500 feet. **Map 1** shows the location of this intersection. 

<iframe src="https://maps.ccrpc.org/ccspi-urban-intx-prospect-bloomington/" width="100%"  height="600" allowfullscreen="true"></iframe>

## Existing Conditions Analysis

The intersection of Prospect Avenue and Bloomington Road is a busy intersection with heavy truck traffic. The average annual daily traffic (AADT) of Prospect Avenue is approximately 25,000 vehicles, AADT of Bloomington Road is approximately 10,350 vehicles. There are commercial stores, private parking lots and driveways in the vicinity of this intersection. There are gas stations located at the northwest and southwest quadrants of this intersection. Home Depot store is located at the northeast quadrant. As this intersection is a skewed intersection, drivers have to turn 120-degree to make a left turn from the eastbound lane. The intersection was identified as a truck crash hot spot in the Champaign-Urbana Metropolitan Planning Area in the [Champaign-Urbana Region Freight Plan](https://ccrpc.org/wp-content/uploads/2019/07/Champaign-Urbana-Region-Freight-Plan-Oct-2019-Final.pdf). Many truck crashes were related to vehicles making eastbound left turn from Bloomington Road onto Prospect Avenue. **Figure 1** shows the geometry of this intersection and access point locations.

{{<image src="figure1.png"
    caption="Figure 1 - Access points near the intersection of Prospect Avenue and Bloomington Road">}}

Vehicle traffic count data for this intersection was collected from 7 AM to 7 PM on Tuesday, October 26th, 2021. Passenger vehicles, trucks, buses, and on-road bicyclists are included in the vehicle traffic count calculations. Light vehicles (96.7%) made up the most substantial portions of traffic. Trucks account for 2.8% of the total traffic at this intersection. The vehicle traffic volumes at each approach are shown in **Figure 2**. **Figure 3** shows turning movement counts for the whole 12-hour period, at the morning peak (7:30 AM -8:30 AM), at the midday peak hour (12:00 PM – 1:00 PM) and at the afternoon peak (4:30 PM – 5:30 PM). It can be observed that many vehicles traveling on Bloomington Road turned north onto Prospect Avenue. The Level-of-Service and Control Delay for each approach at this intersection can be found in **Table 1**. As it can be observed from the table, westbound left approach is operating at capacity during the afternoon peak hour.

<rpc-chart url="15 minute volume.csv"
      chart-title="Figure 2 - Vehicle Traffic Volume at Bradley Avenue and Mattis Avenue.png"
      x-label="Time"
      y-label="Traffic Counts"
      type="line"></rpc-chart> 

{{<image src="figure3.png"
    caption="Figure 3 - Turning Movement Counts">}}

<rpc-table url="los.csv"
  table-title="Table 1 - Approach Level-of-Service and Control Delay at Study Intersection"
  text-alignment="c,c"></rpc-table>

## Traffic Safety Analysis

### Crash Data Analysis 

Crash data from 2015 to 2019 was analyzed for this intersection. The crash data information was obtained from Illinois Department of Transportation Division of Traffic Safety. There were 147 crashes at the study location over the 5-year period. **Figure 4** shows the location and type for each of the crashes at this intersection. 

{{<image src="figure4.png"
    caption="Figure 4 - Collision Diagram">}}

#### Crash Trends and Time

**Figure 5** shows the timing of crashes that occurred at the intersection over this five-year period. September and November were the months with the highest number of crashes. Friday was the day of week with the highest number of crashes. The time windows of 13PM-14PM and 16PM-17PM had the highest crash frequency.

<rpc-chart url="crash_count_year.csv"
      chart-title="Figure 5.1 - Number of Crashes by Year"
      x-label="Year"
      y-label="Crash Counts"
      type="bar"></rpc-chart>

<rpc-chart url="crash_count_month.csv"
      chart-title="Figure 5.2 - Number of Crashes by Month"
      x-label="Month"
      y-label="Crash Counts"
      type="bar"></rpc-chart>

<rpc-chart url="crash_count_day.csv"
      chart-title="Figure 5.3 - Number of Crashes by Day of the Week"
      x-label="Day"
      y-label="Crash Counts"
      type="bar"></rpc-chart>

<rpc-chart url="crash_count_hour.csv"
      chart-title="Figure 5.4 - Number of Crashes by Hour of the Day"
      x-label="Hour"
      y-label="Crash Counts"
      type="bar"></rpc-chart>

#### Crash Severity

Crash severity levels are generally classified using the KABCO scale: Fatal ( K ), A-Injury ( A ), B-Injury ( B ), C-Injury ( C ), and No Injury ( O ). Definitions of KABCO severity levels can be found on the Appendix page. **Table 2** and **Figure 6** present the number of crashes by severity level at the study location. There were no fatal crashes occurred from 2015 to 2019 at this intersection. Most crashes did not result in injuries. However, there were three A-Injury crashes, eleven B-Injury crashes and fourteen C-Injury crashes. 

<rpc-table url="crash_severity_year.csv"
  table-title="Table 2 - Number of Crashes by Severity Level and Year"
  text-alignment="c,c"></rpc-table>

<rpc-chart url="crash_severity_year_chart.csv"
      chart-title="Figure 6 - Number of Crashes by Severity Level and Year"
      x-label="Severity Level"
      y-label="Crash Counts"
      type="bar"
      stacked="true"></rpc-chart>

#### Crash Types

**Table 3** and **Figure 7** show the count of crashes at the intersection from 2015 to 2019, sorted by year and crash type. **Table 4** and **Figure 8** sort each crash type by the number of crashes per KABCO severity level.  Turning crashes and rear-end crashes were the predominant crash types at this intersection, accounting for 45% and 37% of the total crashes, respectively. Most turning crashes led to no injuries, but one lead to A-Injuries, eight led to B-Injuries and four led to C-Injuries. Most rear-end crashes led to no injuries, but one led to A-Injury, one led to B-injury and nine led to C-Injuries. Sideswipe same direction crashes also take a large portion of the total crashes. The detailed analysis of turning crashes, rear-end crashes, and sideswipe same direction crashes can be found in the following chapters. 

<rpc-table url="crash_type_year.csv"
  table-title="Table 3 - Number of Crashes by Crash Type and Year "
  text-alignment="c,c"></rpc-table>

<rpc-chart url="crash_type_year_chart.csv"
      chart-title="Figure 7 - Number of Crashes by Crash Type and Year"
      x-label="Crash Type"
      y-label="Crash Counts"
      type="bar"
      stacked="true"></rpc-chart>

<rpc-table url="crash_severity_type.csv"
  table-title="Table 4 - Number of Crashes by Crash Type and Severity Level "
  text-alignment="c,c"></rpc-table>

<rpc-chart url="crash_severity_type_chart.csv"
      chart-title="Figure 8 - Number of Crashes by Crash Type and Severity Level"
      x-label="Severity Level"
      y-label="Crash Counts"
      type="bar"
      stacked="true"></rpc-chart>

##### Turning crashes

From 2015 to 2019, 66 turning crashes occurred at the study intersection. These crashes can be categorized into three scenarios and are analyzed in the following paragraphs, sorted by their crash counts. The collision diagram of the turning crashes is shown in **Figure 9**. Crashes of different scenarios are shown using different colors.

{{<image src="figure9.png"
    caption="Figure 9 - Turning crashes">}}

- (1)	Scenario 1: A vehicle entering/exiting a parking lot collided with a through vehicle (pink colored crashes)

Twenty-four crashes occurred on the west leg when one vehicle was entering or exiting a parking lot while the other one was traveling through. Twenty-one crashes occurred on the eastbound lanes of Bloomington Road and three crashes occurred on the westbound lanes. Many crashes were associated with vehicles in the adjacent lanes stopping or slowing down and/or waving the driver to go. 

- (2)	Scenario 2: A left-turn vehicle collided with a through vehicle (blue colored crashes)

Twenty-two crashes occurred when a left-turn vehicle collided with a through vehicle. Nine crashes occurred when one vehicle was making a southbound left turn from Prospect Avenue onto Bloomington Road while the other one was heading north on Prospect Avenue. Seven crashes occurred when one vehicle was making a northbound left turn from Prospect Avenue onto Bloomington Road while the other one was heading south on Prospect Avenue. The main crash causes include disobeying or misinterpreting traffic signals, failing to see the other vehicle due to the heavy traffic, and being allowed to proceed by another vehicle. 

- (3)	Scenario 3: Both vehicles were turning left (green colored crashes)

Twenty crashes occurred when both vehicles were turning left. Eighteen crashes occurred when both vehicles attempted to make an eastbound left turn from Bloomington Road onto Prospect Avenue, and many of them involved trucks. One crash occurred when a vehicle was making a southbound left turn from Prospect Avenue and the other one was making an eastbound left turn from Bloomington Road. One crash occurred when both vehicles were making a westbound left turn from Bloomington Road. 

##### Rear-end crashes

Fifty-four rear-end crashes occurred at the study intersection from 2015 to 2019. The crashes can be categorized into three scenarios, as described below. The collision diagram of the turning crashes is shown in **Figure 10**. Crashes of different scenarios are marked by different colors.

{{<image src="figure10.png"
    caption="Figure 10 - Turning crashes">}}

- (1)	Scenario 1: Crashes on a right-turn lane (red colored crashes)

Twenty-four crashes occurred on the right-turn lanes. Most occurred on the westbound right-turn lane and the southbound right-turn lane. Most crashes were related to the front vehicle stopping or slowing down to wait for the red light or traffic. 

- (2)	Scenario 2: Crashes occurred when vehicles were approaching the intersection (blue colored crashes)

Nineteen crashes occurred when the vehicles were approaching the intersection. Most crashes occurred when the front vehicle stopped in traffic or stopped to wait for the red light, the following vehicle followed too close and/or failed to reduce speed. On Prospect Avenue northbound lanes, seven crashes occurred on the through lanes and one occurred on the left-turn lane. On Prospect Avenue southbound lanes, two crashes occurred on the westside through lane. On Bloomington Road eastbound lanes, six crashes occurred on the through lane and shared through and left-turn lane, and two crashes occurred on the left-turn lane. One crash occurred on Bloomington Road westbound through lane. 

- (3)	Scenario 3: Crashes occurred when vehicles were leaving the intersection (green colored crashes)

Eleven crashes occurred when vehicles were leaving the intersection and some of these crashes involved more than two vehicles. Again, most crashes occurred when the leading vehicle stopped or slowed in traffic queue and the following vehicle followed too closely and/or failed to reduce speed. 

##### Sideswipe same direction crashes

From 2015 to 2019, sixteen sideswipe same direction crashes occurred at the study intersection. Although the crash causes vary, most crashes were related to lane-changing maneuvers. Causes that induce the lane-changing maneuvers include attempting to enter the gas station, preparing for the left-turn, planning to merge on Interstate-74 etc. The collision diagram of the turning crashes is shown in **Figure 11**.

{{<image src="figure11.png"
    caption="Figure 11 - Sideswipe same direction crashes">}}

## Findings and Proposed Countermeasures

### (1) Access management 

There are many private parking lots and driveways in the vicinity of this intersection. As shown in the crash analysis section, a high percentage of crashes occurred when a vehicle was entering or leaving a private parking lot, the driver could not see the other vehicle(s) due to heavy traffic, or the driver was allowed to proceed by a stopping vehicle, and then failed to yield to a vehicle on a through lane. It is recommended implementing access management to limit the number of access points in the vicinity of the intersection.

### (2) Improve intersection geometry design

There were many turning crashes occurred when two vehicles were turning left at the same time, most were associated with eastbound left turns. At this skewed intersection, drivers have to turn 120 degrees to get onto Prospect Avenue. Therefore, it is recommended to improve the design of the intersection. Currently, IDOT is working with the City of Champaign to conduct a preliminary engineering study on Prospect Avenue between Interstate-74 and Springfield Avenue which includes improvements to the intersection of Prospect Avenue and Bloomington Road.