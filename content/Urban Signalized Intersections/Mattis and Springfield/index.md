---
title: "Mattis Avenue and Springfield Avenue"
draft: false
weight: 30
bannerHeading: Mattis Avenue and Springfield Avenue
bannerText: >
  The intersection of Mattis Avenue and Springfield Avenue.
bannerAction: Provide Feedback
bannerUrl: /contact
---

## Introduction

This study analyzes safety conditions at the intersection of Mattis Avenue and Springfield Avenue, a signalized intersection in the City of Champaign. This intersection was ranked as one of the top high-priority urban signalized intersections in the Champaign County Safety Plan Implementation analysis. **Map 1** shows the location of this intersection. Mattis Avenue serves as a north-south principal arterial on the north side of this intersection and a minor arterial on the south side. Springfield Avenue serves as an east-west principal arterial.

<iframe src="https://maps.ccrpc.org/ccspi-urban-intx-mattis-springfield/" width="100%"  height="600" allowfullscreen="true"></iframe>

## Existing Conditions Analysis

The existing condition analysis of the intersection of Mattis Avenue and Springfield Avenue consists of an evaluation of the intersection's physical and geometrical conditions, as well as the traffic operational conditions of its roadway approaches.  

### Geometry and physical conditions

The study intersection is a signal-controlled intersection. **Figure 1.1-1.2** show the configuration of the intersection.

Mattis Avenue is a four-lane road with two lanes per direction. This roadway's annual average daily traffic (AADT) is between 19,900 and 20,000 vehicles per day [^1], according to Illinois Department of Transportation (IDOT) data from 2021. Each lane is approximately 12 feet wide along this roadway (based on aerial imagery). A raised median is present on both the north and south legs of the intersection. Parking is not permitted along Mattis Avenue. The speed limit is 40 miles per hour (mph) for Mattis Avenue, and a speed limit sign is posted on the north side of the intersection. There are streetlights along Mattis Avenue. 

Springfield Avenue is also a four-lane road with two lanes per direction. This roadway's annual average daily traffic (AADT) is between 13,200 and 14,900 vehicles per day [^1], according to 2021 IDOT data. Each lane is approximately 11 feet wide on the east side of Springfield Avenue, while each lane is approximately 12 feet wide on the west side (based on aerial imagery). Parking is not permitted along Springfield Avenue, and it has a 35 mph speed limit. A speed limit sign is posted on the east side of the intersection. Streetlights are also available along Springfield Avenue. 

Road markings are mostly present at this intersection and road surface conditions are poor to average. Standard pedestrian crossing markings are present on all four legs of the intersection. One exclusive left-turn lane, one through lane, and one through/right-turn lane are present on the west and south legs. One exclusive left-turn lane, one exclusive right-turn lane, and two through lanes are present on the east and north legs of the intersection. All of the left-turn lanes have slightly negative offsets. 

{{<image src="figure1.png"
    caption="Figure 1-1 - Mattis Avenue and Springfield Avenue Intersection Configuration">}}

{{<image src="figure1-2.png"
    caption="Figure 1-2 - Mattis Avenue and Springfield Avenue Intersection Configuration">}}

Pedestrian and bicyclist volumes are comparatively low at the Mattis Avenue and Springfield Avenue intersection. The surrounding land uses at the intersection of Mattis Avenue and Springfield Avenue are mostly commercial, with some residential uses. Sidewalks are present, with exceptions along the western edge of Mattis Avenue north of the intersection and the southern edge of Springfield Avenue on both of its approaches. 

### Vehicle Traffic Operational Conditions

#### Peak Hour vehicular Traffic

Twelve hours of vehicular, pedestrian, and bicycle movement data for this intersection were collected from 7 AM to 7 PM on Tuesday, September 28th, 2021. Passenger vehicles (also referred to as light vehicles) made up the most substantial portion of traffic, at 97.0%. **Figure 2** shows the vehicle traffic volume at each approach, and **Figure 3** shows the peak hour turning movement counts for each direction. The 12-hour traffic count data shows that the north and south legs (Mattis Avenue) have higher traffic volumes than the east and west legs (Springfield Avenue). The AM peak hour is 7:15 AM-8:15 AM, the midday peak hour is 12:00 PM – 1:00 PM, and the PM peak hour is 4:30 PM – 5:30 PM.

<rpc-chart url="15 minute volume.csv"
      chart-title="Figure 2 - Vehicle Traffic Volume at Bradley Avenue and Mattis Avenue.png"
      x-label="Time"
      y-label="Traffic Counts"
      type="line"></rpc-chart> 

{{<image src="figure3.png"
    caption="Figure 3 - Turning Movement Counts"
    alt="Graphic showing the number of movements made at this intersection in each direction">}}

#### Vehicle Level-Of-Service

Level of service (LOS) is a qualitative measure describing operational conditions from "A" (best) to "F" (worst) within a traffic stream or at an intersection. This measurement is quantified for signalized and unsignalized intersections in terms of vehicle control delay.

Control delay is a component of delay that results from the type of traffic control at the intersection or approach, measured by comparison with the uncontrolled condition. It is the difference between the travel time that would have occurred in the absence of the intersection control and the travel time that results from the intersection control's presence.

**Table 1** shows the LOS criteria for Signalized Intersections as per the Highway Capacity Manual 6th Edition

<rpc-table url="LOScriteria.csv"
  table-title="Table 1 - LOS Criteria for signalized intersections"
  text-alignment="c,c"></rpc-table>

The LOS and control delays during the morning (AM), midday, and afternoon (PM) peak hours for the study intersection are shown in **Table 2**. The overall intersection operational condition is an LOS of D for morning and afternoon peak hours. It should be noted that this intersection has protected/permissive phasing for left-turn movement.

<rpc-table url="los.csv"
    table-title="Table 2 - Approach level-of-service and control delay at Mattis Avenue and Springfield Avenue intersection"
    text-alignment="c,c"></rpc-table>

## Traffic Safety Analysis

### Crash Data Analysis 

Crash data from 2015 to 2019 was analyzed for the intersection of Mattis Avenue and Springfield Avenue. The crash data information was obtained from the IDOT Division of Traffic Safety (i). Eighty-six crashes occurred at the study location over these five years. **Figure 4** shows the collision diagram for this intersection. 

{{<image src="figure4.png"
    caption="Figure 4 - Collision Diagram"
    alt="Plan view diagram of intersection, showing the type and location for all crashes from 2015 to 2019">}}

#### Crash Trends and Time

**Figure 5** shows the time of crashes from 2015 to 2019 at the intersection of Mattis Avenue and Springfield Avenue. As seen in the figure, 24 crashes occurred in 2019, which was the highest annual total within the study period. There was construction at this site in 2019, and all of these crashes can be considered work-zone-related crashes. The lowest number of crashes occurred in 2018 with 11 crashes. April and August had a relatively high number of crashes compared to other months, with ten crashes each. Monday was the day of the week with the highest number of crashes, at seventeen crashes. The highest crash frequency was from 3 PM to 8 PM, while the single-hour periods with the highest crash rates were 11 AM to noon and 4 PM to 5 PM, each with eight crashes. 

<rpc-chart url="crash_count_year.csv"
      chart-title="Figure 5.1 - Number of Crashes by Year"
      x-label="Year"
      y-label="Crash Counts"
      type="bar"></rpc-chart>

<rpc-chart url="crash_count_month.csv"
      chart-title="Figure 5.2 - Number of Crashes by Month"
      x-label="Month"
      y-label="Crash Counts"
      type="bar"></rpc-chart>

<rpc-chart url="crash_count_day.csv"
      chart-title="Figure 5.3 - Number of Crashes by Day of the Week"
      x-label="Day"
      y-label="Crash Counts"
      type="bar"></rpc-chart>

<rpc-chart url="crash_count_hour.csv"
      chart-title="Figure 5.4 - Number of Crashes by Hour of the Day"
      x-label="Hour"
      y-label="Crash Counts"
      type="bar"></rpc-chart>

#### Crash Severity

Crash severity levels are generally classified in order of decreasing severity as Fatal (K), A-Injury (A), B-Injury (B), C-Injury (C), and No Injury (O). The definitions of these KABCO severity levels can be found in **Appendix A**. **Table 3** and **Figure 6** present the number of crashes by severity level at the study location. There were no fatal crashes from 2015 to 2019 at the intersection of Mattis Avenue and Springfield Avenue. Most crashes (74%) were No Injury crashes. Four A-Injury crashes, seven B-Injury crashes, and eleven C-Injury crashes occurred in the five-year study period. 

<rpc-table url="crash_severity_year.csv"
  table-title="Table 3 - Number of Crashes by Severity Level and Year"
  text-alignment="c,c"></rpc-table>

<rpc-chart url="crash_severity_year_chart.csv"
      chart-title="Figure 6 - Number of Crashes by Severity Level and Year"
      x-label="Severity Level"
      y-label="Crash Counts"
      type="bar"
      stacked="true"></rpc-chart>

#### Crash Types

**Table 4** and **Figure 7** show the types of crashes by year at the intersection of Mattis Avenue and Springfield Avenue. As shown in the figure, turning crashes were the predominant crash type at this intersection, accounting for 49% of total crashes. Rear-end crashes were also prevalent at the intersection, accounting for 26% of all crashes. A detailed analysis of the turning and rear end crashes can be found in chapters 3.1.3.1 and 3.1.3.2. There were three pedalcyclist related crashes, causing one A Injury crash and two B Injury crashes at this location. 

<rpc-table url="crash_type_year.csv"
  table-title="Table 4 - Number of Crashes by Crash Type and Year "
  text-alignment="c,c"></rpc-table>

<rpc-chart url="crash_type_year_chart.csv"
      chart-title="Figure 7 - Number of Crashes by Crash Type and Year"
      x-label="Crash Type"
      y-label="Crash Counts"
      type="bar"
      stacked="true"></rpc-chart>

**Table 5** and **Figure 8** quantify of each type of crash by severity level. Sideswipe crashes led to no injuries. The number of pedalcyclist-related crashes was low, but they were relatively severe compared to other types of crashes, with all resulting in injuries. Angle crashes also had a higher injury rate than other types of crashes. 

<rpc-table url="crash_severity_type.csv"
  table-title="Table 5 - Number of Crashes by Crash Type and Severity Level "
  text-alignment="c,c"></rpc-table>

<rpc-chart url="crash_severity_type_chart.csv"
      chart-title="Figure 8 - Number of Crashes by Crash Type and Severity Level"
      x-label="Severity Level"
      y-label="Crash Counts"
      type="bar"
      stacked="true"></rpc-chart>

##### Turning crashes

From 2015 to 2019, 42 turning crashes occurred at the study intersection, making turning crashes the predominant crash type. Turning crashes consisted of one A-Injury crash, two B-Injury crashes, six C-Injury crashes, and 33 No Injury turning crashes. In 93% of cases, drivers taking a left turn collided with oncoming vehicles traveling straight through the intersection. Failing to yield the right of way was the primary reason for turning crashes. Twenty-nine percent of turning crashes occurred during wet or snowy pavement conditions, and 38% occurred in dark conditions.     

##### Rear-End crashes

From 2015 to 2019, 22 rear-end crashes occurred at the study intersection. Most rear-end crashes (68%) occurred on Mattis Avenue. Rear-end crashes caused one A-Injury crash, four C-Injury crashes, and 17 No Injury crashes. Following too closely or failing to reduce speed to avoid crashes were the primary reasons for rear-end crashes. In most cases, the leading vehicle stopped or slowed down due to a red light or traffic congestion, and the following vehicle could not stop properly.

## Findings and Proposed Countermeasures

### Study Findings 

After analyzing the crash data and site investigation results, several risk factors were identified. The identified findings are listed below:

- Left turning lanes have slightly negative offsets.
- There is an absence of dedicated traffic signal heads for each lane.
- Physical medians (curbs, rather than just paint) are not extended throughout the entire intersection functional areas on the east and south leg.
- Turning crashes are the predominant type of crash.
- Thirty-four percent of crashes occurred at nighttime.
- There were three pedalcyclist crashes, although pedestrian and bicyclist volumes at the study intersection were low. The highest hourly pedestrian volume was 18 pedestrians per hour (between 3:15 PM and 4:15 PM).

### Proposed Countermeasures 

After analyzing the operational and safety issues, the following recommendations are provided:

#### (1) Offsets Improvement for Left Turn Lanes

Offset refers to the direction (positive or negative) and distance between the centerline of the left-turn lanes on one approach and the centerline of the left-turn lane on the opposing approach [^2]. The direction is positive if the left-turn lane on one approach is to the left of the opposing left-turn lane and negative if the other way around [^2]. **Figure 9** shows different offsets for left-turn lanes [^3].

{{<image src="figure9.png"
    caption="Figure 9 - Different offsets for left turn lanes">}}

The Mattis Avenue and Springfield Avenue intersection has slightly negative left-turn lane offsets for all four legs. A negative left-turn lane offset has an impact on the visibility of oncoming vehicles, which increases the possibility of turning crashes. Because of this, offset improvement for the left-turn lanes is recommended at the intersection of Mattis Avenue and Springfield Avenue. This countermeasure has a Crash Modification Factor (CMF) of 0.662 for all types of crashes [^4], which means it should reduce future crashes to 66.2% of the current frequency. It should be noted that this proposed strategy is most effective when the intersection has permissive or permissive/protective phasing for left-turn movement, which is the way that the signal phasing at this intersection currently operates.

#### (2) Traffic signal modernization

At present, no flashing yellow signal is available for permissive left-turning movement. Flashing yellow signals mean drivers must yield to pedestrians and oncoming traffic before making a left turn. Adding flashing yellow signals for left-turn movement would improve safety and signal visibility at the intersection of Mattis Avenue and Springfield Avenue. This countermeasure has a CMF of 0.857 for left-turn crashes [^5]. 

Approximately 34% of crashes at this intersection occurred at nighttime. Adding a signal head for each lane is also recommended to improve signal visibility. This has a CMF of 0.867 for nighttime crashes [^6].

#### (3) Access Control

As much as possible, driveways and median openings should be limited within intersections’ functional areas, according to CUUATS access management guidelines [^7]. Many driveways provide access to commercial parking lots in the vicinity of this intersection. Physical medians are present on all legs, but they are not extended up to the intersection functional areas on the east and south leg. These accesses are not in compliance with the proposed access recommendations regarding minimum intersection functional areas, based on roadway functional classification and roadway ADT. Consolidating some access openings and extending the medians on the east and south leg at this intersection are recommended.

#### (4) Crosswalk Marking Improvement

Currently, standard crosswalk markings are present at the Mattis Avenue and Springfield Avenue intersection. From traffic count data, the highest number of pedestrians crossing at this intersection was 18 from 3:15 PM to 4:15 PM. This number of pedestrian crossings is not high. However, some bicyclists crossed this intersection using the crosswalk. Three bicyclists were involved in crashes, and all of them occurred at the crosswalks. Considering this issue, high visibility crosswalk markings, such as ladder or continental crosswalk markings, are recommended at this intersection. 

#### (5) Pavement Conditions Improvement

Approximately 19% of crashes at this intersection occurred due to wet or icy roadway conditions. These types of roadway conditions caused 29% of turning crashes. For this reason, improving pavement friction conditions at the intersection of Mattis Avenue and Springfield Avenue is recommended. Pavement friction management includes providing adequate and durable pavement properties, as well as collecting data and performing analysis to ensure the effectiveness of this countermeasure [^8]. The main purpose of this countermeasure is to minimize pavement-friction-related crashes. High friction surface treatment (HFST) will increase friction and skid resistance and has a CMF of 0.8 for intersection crashes [^9]. However, this countermeasure has a low service life and needs to be maintained periodically.

## References

[^1]:	Illinois Department of Transportation (https://www.gettingaroundillinois.com/Traffic%20Counts/index.html)
[^2]:Federal Highway Administration Model Inventory of Roadway Elements (https://safety.fhwa.dot.gov/tools/data_tools/mirereport/147.cfm#:~:text=Definition%3A%20Amount%20of%20offset%20between,lane%20on%20the%20opposing%20approach)
[^3]: Federal Highway Administration (https://www.fhwa.dot.gov/publications/research/safety/09035/index.cfm)
[^4]: CMF Clearinghouse (http://www.cmfclearinghouse.org/detail.cfm?facid=6095)
[^5]:	CMF Clearinghouse (http://www.cmfclearinghouse.org/detail.cfm?facid=7730)
[^6]:	CMF Clearinghouse (http://www.cmfclearinghouse.org/detail.cfm?facid=4113)
[^7]:	Access Management Guidelines for the Urbanized Area (https://ccrpc.org/wp-content/uploads/2015/03/access-management-2013-04-17-final.pdf)
[^8]:	Federal Highway Administration (https://safety.fhwa.dot.gov/roadway_dept/pavement_friction/friction_management/#:~:text=Pavement%20friction%20management%20includes%20providing,the%20effectiveness%20of%20the%20program)
[^9]:	Federal Highway Administration (https://safety.fhwa.dot.gov/provencountermeasures/pavement-friction.cfm)
