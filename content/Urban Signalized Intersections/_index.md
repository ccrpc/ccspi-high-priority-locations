---
title: "Urban Signalized Intersections"
date: 2022-06-07T11:32:26-06:00
draft: false
menu: main
weight: 50
bannerHeading: Urban Signalized Intersections
bannerText: >
bannerAction: Provide Feedback
bannerUrl: /contact
---

In the urban area, five signalized intersections were selected as high-priority locations. Considering the fact that the intersection of Cunningham Avenue and University Avenue and the intersection of Lincoln Avenue and University Avenue were recently improved, three locations were analyzed. The locations are shown below:

<rpc-table url="urb_sig.csv" table-title="Urban Signalized Intersections"> </rpc-table>