---
title: "Mattis Avenue and Bradley Avenue"
draft: false
weight: 20
bannerHeading: Mattis Avenue and Bradley Avenue
bannerText: >
  The intersection of Mattis Avenue and Bradley Avenue.
bannerAction: Provide Feedback
bannerUrl: /contact
---

## Introduction

This study analyzes the safety of the intersection of Mattis Avenue and Bradley Avenue. This is a signal-controlled intersection located in the City of Champaign. **Map 1** shows the location of this intersection. 

<iframe src="https://maps.ccrpc.org/ccspi-urban-intx-mattis-bradley/" width="100%"  height="600" allowfullscreen="true"></iframe>

## Existing Conditions Analysis

Existing conditions analysis consists of examining the geometry and physical conditions of the intersection, as well as evaluating traffic operational conditions. 

### Geometry and physical conditions

The intersection of Mattis Avenue and Bradley Avenue is a busy intersection with heavy truck traffic.  The average annual daily traffic (AADT) of Mattis Avenue is approximately 11000 vehicles, AADT of Bradley Avenue is approximately 9000 vehicles. There are driveways accessing businesses’ parking lots in the vicinity of this intersection. Gas stations are located at the northwest and northeast corners of this intersection, and other commercial stores are located at the northwest, northeast and southwest corners. An industrial area is located to the southeast of this intersection, with facilities for Kraft Foods and DSC Logistics. **Figure 1** shows the access points near this intersection. 

{{<image src="figure1.png"
    caption="Figure 1 - Access points near the intersection of Mattis Avenue and Bradley Avenue"
    alt="Map labeling the access points near the intersection of Mattis Avenue and Bradley Avenue">}}

### Vehicle Traffic Operational Conditions

Vehicle traffic count data for this intersection was collected from 7 AM to 7 PM on Thursday, September 28th, 2021. Passenger vehicles (also referred to as light vehicles), trucks, buses, and on-road bicyclists are included in the vehicle traffic volume calculations. Light vehicles (95.9%) made up the most substantial portions of traffic. Trucks accounted for 3.7% of the total traffic at this intersection, which is higher than the other urban intersections in this safety study. The vehicle traffic volumes at each approach are shown in **Figure 2**. This shows that Mattis Avenue (NB & SB) had a higher traffic volume than Bradley Avenue (EB & WB). **Figure 3** shows turning movement counts for the whole 12-hour period, at the morning peak hour (7:15 AM -8:15 AM), at the midday peak hour (11:45 PM – 12:45 PM), and at the afternoon peak hour (2:15 PM – 3:15 PM). The level of service (LOS) and control delay for each approach at this intersection can be found in **Table 1**. As it can be observed from the table, the eastbound and westbound through and right approaches are operating at capacity during the afternoon peak hour. 

Mattis Avenue (from Bloomington Road to Windsor Road) and Bradley Avenue (from Staley Road to Lincoln Avenue) are heavily traveled by trucks and were identified as truck crash hot spots in the Champaign-Urbana Metropolitan Planning Area by the [Champaign-Urbana Region Freight Plan](http://localhost:1313/ccspi-high-priority-locations/rural-segments/segment1/#http://localhost:1313/ccspi-high-priority-locations/rural-intersections/intersection/). Trucks approaching or exiting the industrial area near the Mattis Avenue and Bradley Avenue intersection have easy access to I-74, I-57, and I-72 via Mattis Avenue, Bradley Avenue, Bloomington Road, and Prospect Avenue. **Figure 4** shows the truck crash hot spots at Mattis Avenue and Bradley Avenue. 

<rpc-chart url="15 minute volume.csv"
      chart-title="Figure 2 - Vehicle Traffic Volume at Mattis Avenue and Bradley Avenue"
      x-label="Time"
      y-label="Traffic Counts"
      type="line"></rpc-chart> 

{{<image src="figure3-Turning Movement Counts.png"
    caption="Figure 3 - Turning Movement Counts"
    alt="Graphic showing the number of movements made at this intersection in each direction">}}

<rpc-table url="los.csv"
  table-title="Table 1 - Approach Level-of-Service and Control Delay at Study Intersection"
  text-alignment="c,c"></rpc-table>

{{<image src="figure4-truck crash hot sport.png"
    caption="Figure 4 Truck crash hot spots at Mattis Avenue and Bradley Avenue">}}

## Traffic Safety Analysis

### Crash Data Analysis 

Crash data from 2015 to 2019 was analyzed for the intersection of Mattis Avenue and Bradley Avenue. The crash data information was obtained from the Illinois Department of Transportation Division of Traffic Safety. There were 117 crashes at the study location over this five year period. **Figure 5** shows the location and type of collision of each of the crashes at this intersection. 

{{<image src="figure5-Collision Diagram.PNG"
    caption="Figure 5 - Collision Diagram"
    alt="Plan view diagram of intersection, showing the type and location for all crashes from 2015 to 2019">}}

#### Crash Trends and Time

**Figure 6** shows the timing of crashes that occurred at the intersection over this five-year period, and indicates that December and January were the months with the highest number of crashes. The time windows from 5-6PM had the highest crash frequency.

<rpc-chart url="crash_count_year.csv"
      chart-title="Figure 6.1 - Number of Crashes by Year"
      x-label="Year"
      y-label="Crash Counts"
      type="bar"></rpc-chart>

<rpc-chart url="crash_count_month.csv"
      chart-title="Figure 6.2 - Number of Crashes by Month"
      x-label="Month"
      y-label="Crash Counts"
      type="bar"></rpc-chart>

<rpc-chart url="crash_count_day.csv"
      chart-title="Figure 6.3 - Number of Crashes by Day of the Week"
      x-label="Day"
      y-label="Crash Counts"
      type="bar"></rpc-chart>

<rpc-chart url="crash_count_hour.csv"
      chart-title="Figure 6.4 - Number of Crashes by Hour of the Day"
      x-label="Hour"
      y-label="Crash Counts"
      type="bar"></rpc-chart>

#### Crash Severity

Crash severity levels are generally classified using the KABCO scale: Fatal (K), A-Injury (A), B-Injury (B), C-Injury (C), and No Injury (O). Definitions of KABCO severity levels can be found on the Appendix page. **Table 2** and **Figure 7** present the number of crashes by severity level at the study location. There were no fatal crashes from 2015 to 2019 at the intersection of Mattis Avenue and Bradley Avenue. Most crashes did not result in injuries. However, there were six A-Injury crashes, ten B-Injury crashes and nineteen C-Injury crashes. 

<rpc-table url="crash_severity_year.csv"
  table-title="Table 2 - Number of Crashes by Severity Level and Year"
  text-alignment="c,c"></rpc-table>

<rpc-chart url="crash_severity_year_chart.csv"
      chart-title="Figure 7 - Number of Crashes by Severity Level and Year"
      x-label="Severity Level"
      y-label="Crash Counts"
      type="bar"
      stacked="true"></rpc-chart>

#### Crash Types

**Table 3** and **Figure 8** show the count of crashes at the intersection from 2015 to 2019, sorted by year and crash type. **Table 4** and **Figure 9** sort each crash type by the number of crashes for each severity level. Rear-end and turning crashes were the predominant crash types at the intersection, accounting for 51% and 31% of all crashes, respectively. Most rear-end crashes led to no injuries, but two led to A-Injuries, four led to B-Injuries, and six led to C-Injuries. Most turning crashes led to no injuries, but three led to A-Injuries, three led to B-injuries, and seven led to C-Injuries. A detailed analysis of rear-end crashes and turning crashes can be found below. Four pedestrian crashes and two pedalcyclist crashes occurred at this intersection, leading to one A-Injury, two B-Injuries, and three C-Injuries. 

<rpc-table url="crash_type_year.csv"
  table-title="Table 3 - Number of Crashes by Crash Type and Year "
  text-alignment="c,c"></rpc-table>

<rpc-chart url="crash_type_year_chart.csv"
      chart-title="Figure 8 - Number of Crashes by Crash Type and Year"
      x-label=" Crash Type"
      y-label="Crash Counts"
      type="bar"
      stacked="true"></rpc-chart>

<rpc-table url="crash_severity_type.csv"
  table-title="Table 4 - Number of Crashes by Crash Type and Severity Level "
  text-alignment="c,c"></rpc-table>

<rpc-chart url="crash_severity_type_chart.csv"
      chart-title="Figure 9 - Number of Crashes by Crash Type and Severity Level"
      x-label="Severity Level"
      y-label="Crash Counts"
      type="bar"
      stacked="true"></rpc-chart>

##### Rear-end crashes

From 2015 to 2019, 60 rear-end crashes occurred at the Mattis Avenue and Bradley Avenue intersection. These rear-end crashes can be classified into two categories and four scenarios, as described below.  Most crashes occurred when the front vehicle was stopping or slowing down for a red light or traffic, and the following vehicle followed too closely or failed to reduce speed. Some rear-end crashes also occurred when a driver was changing lanes. Many rear-end crashes involved more than two vehicles. The collision diagram of rear end crashes is shown in Figure 10. The different crash scenarios are indicated by different colors.

{{<image src="figure10.png"
    caption="Figure 10 - Rear-end crashes">}}

- (A)	Category A: Crashes when vehicles were entering the intersection:
Fifty-two rear-end crashes occurred when vehicles were entering the intersection, accounting for 87% of the rear-end crashes at the study intersection. These crashes can be further categorized into three scenarios:
  - Scenario 1: Crashes in a right-turn lane (pink colored crashes)

    Twenty-eight crashes occurred in a right-turn lane, accounting for 32% of the rear-end crashes. Eleven crashes occurred in the southbound right-turn lane, eight crashes occurred in the northbound right-turn lane, five crashes occurred in the eastbound right-turn lane, and four crashes occurred in the westbound right-turn lane. In most crashes, the following vehicle failed to reduce speed and collided with the front vehicle which was stopping or slowing down to wait for the red light or traffic. Some crashes were also associated with icy roadway conditions.

  - Scenario 2: Crashes in a through lane (orange colored crashes)

    Nineteen crashes occurred in a through lane, accounting for 32% of rear-end crashes. Rear-end crashes were more frequent on Bradley Avenue than on Mattis Avenue; six crashes occurred on the eastbound through lane, five crashes occurred on the westbound through lane, five crashes occurred on the northbound through lane, and four crashes occurred on the southbound through lane. Some crashes involved multiple vehicles. Most crashes occurred when the traffic signals were changing and can be attributed to the driver failing to reduce speed or following too closely. However, the triggers for crashes vary, including the move-and-stop action of the leading vehicles, being distracted or misled by traffic in nearby lanes, misinterpreting the movements of the leading vehicle, and a slippery roadway. 

  - Scenario 3: Crashes in a left-turn lane (green colored crashes)

    Five crashes occurred in a left-turn lane. This occurred more frequently on Bradley Avenue than on Mattis Avenue, with four crashes on Bradley Avenue and one crash on Mattis Avenue. 

- (B)	Category B: Crashes when vehicles were leaving the intersection (blue colored crashes)

  - Scenario 4: Seven crashes occurred when the vehicles were leaving the intersection, accounting for 13% of rear-end crashes. Some of these crashes occurred when the front vehicle stopped in traffic or stopped to wait for a vehicle ahead to make a turn. 

##### Turning crashes
From 2015 to 2019, 36 turning crashes occurred at the study intersection. These crashes can be categorized into four scenarios and are analyzed in the following paragraphs, sorted by their crash counts. The main causes for turning crashes at this intersection can be summarized as failing to yield, disregarding traffic signals, being allowed to proceed by another vehicle, and not being able to see other vehicles due to heavy traffic. Some crashes were associated with more than one factor. The collision diagram of turning crashes is shown in Figure 11. Crashes of different scenarios are indicated by different colors.

{{<image src="figure11.png"
    caption="Figure 11 - Turning crashes">}}

- (1)	Scenario 1: A left-turn vehicle colliding with a through vehicle (pink colored crashes)

  Nineteen crashes (53% of all turning crashes) occurred when a left-turn vehicle collided with a through vehicle. In eight of these, an eastbound left-turn vehicle collided with a westbound through vehicle; in  seven, a westbound left-turn collided with an eastbound through vehicle; in three, a southbound left-turn vehicle collided with a northbound through vehicle; and in one, a northbound left-turn vehicle collided with a southbound through vehicle.

- (2)	Scenario 2: A vehicle entering or exiting a parking lot (orange colored crashes)

  Thirteen crashes (36% of all turning crashes) occurred when one vehicle was entering or exiting a parking lot, while the other vehicle was traveling through. In some of these, the vehicles in adjacent lanes stopped to wait for the parking lot driver and/or waved for the driver to go. 

- (3)	Scenario 3: A vehicle making a right turn (green colored crashes)

  Three crashes occurred when a vehicle was making a right turn. Two crashes occurred when one vehicle was turning north from the westbound right turn lane and one vehicle was turning north from the eastbound left-turn lane; one crash occurred when one vehicle was turning right to east and the other vehicle was heading straight east. All these crashes led to property damage only. 

- (4)	Scenario 4: Both vehicles executing a left-turn (blue colored crashes)

  One crash occurred when both vehicles were turning left. 

## Findings and Proposed Countermeasures

### (1)	Access management 

There are many private parking lots and driveways near this intersection. As shown in the crash analysis section, a high percentage of crashes occurred when a vehicle was entering or exiting a private parking lot, the driver could not see the other vehicle(s) due to heavy traffic, or the driver was allowed to proceed by a stopping vehicle, and then failed to yield to a vehicle on a through lane. According to [Access Management Guidelines for the Urbanized Area](https://ccrpc.org/wp-content/uploads/2015/03/access-management-2013-04-17-final.pdf), the upstream and downstream functional distance of this intersection is less than the recommended distance. Therefore, it is recommended to reduce and consolidate access points near this intersection.  

### (2)	Traffic signal modernization

Traffic signal modernization at this intersection is recommended to improve intersection safety for all roadway users. Traffic signal modernization would include the following elements: adding flashing arrows, modernizing vehicle and pedestrian detection, improving signal timing, retroreflective backplates, supplemental signal heads align with each travel to improve approach sight distance, etc. 

### (3) Roundabout

Studies have shown that roundabouts are a safer alternative to traffic signals and stop signs, and roundabouts also provide many environmental and economic benefits. Converting the current signalized intersection to a roundabout is recommended, but some factors need to be considered: 

- i.	Adaptation and education

  Currently, there are no multi-lane roundabouts in the Champaign-Urbana Metropolitan Planning Area (MPA). There are only two mini roundabouts in residential areas, one in Champaign and one in Urbana. There are no full roundabouts built in the community at this point. A multi-lane roundabout can be considered when there is adequate education and outreach regarding roundabouts in the community, and the public is already familiar with single-lane roundabouts. 

- ii.	The safety of visually impaired pedestrians

  While roundabouts can provide many benefits, they may decrease the safety of visually impaired pedestrians. Visually impaired pedestrians typically rely on audio cues and predictable traffic signal cycle patterns to detect the movement of traffic and determine if it is safe to cross the street. At roundabouts, there is no such pattern to detect, which makes roundabouts difficult to navigate for visually impaired pedestrians. 
  
- iii.	Truck safety

  According to the Champaign-Urbana Region Freight Plan, Mattis Avenue (from Bloomington Road to Windsor Road) and Bradley Avenue (from Staley Road to Lincoln Avenue) are heavily traveled by trucks. The intersection of Mattis Avenue and Bradley Avenue has a high truck volume and is a truck crash hot spot in the Champaign-Urbana MPA. The safety of trucks should be considered when designing the roundabout at this location. 
