---
title: "Appendix"
date: 2019-06-14T11:32:26-06:00
draft: false
menu: main
weight: 70
bannerHeading: Appendix
bannerText: >
bannerAction: Provide Feedback
bannerUrl: /contact
---
## Appendix A

<rpc-table url="KABCO.csv" table-title="Crash Severity Definitions"> </rpc-table>

## Appendix B

{{<image src="AppendixB.PNG" position="full" aspect="12:15">}}

## Appendix C

{{<image src="AppendixC.PNG" position="full" aspect="12:15">}}