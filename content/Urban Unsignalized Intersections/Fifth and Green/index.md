---
title: "Fifth Street and Green Street"
draft: false
weight: 30
bannerHeading: Fifth Street and Green Street.
bannerText: >
  The intersection of Fifth Street and Green Street.
bannerAction: Provide Feedback
bannerUrl: /contact
---

## Introduction

This study analyzes the safety of the intersection of South Fifth Street and East Green Street (referred to as Fifth Street and Green Street for the remainder of this document) in Champaign. The intersection is unsignalized and located in urban Champaign County. **Map 1** shows the location of the intersection, which is near the University of Illinois, in the area commonly known as Campustown.  Green Street is a minor traffic arterial for the county, as well as a significant commercial corridor, while Fifth Street is a local road and connects primarily institutional uses south of Green Street with multifamily residential uses north of Green Street.

<iframe src="https://maps.ccrpc.org/ccspi-urban-intx-fifth-green/" width="100%"  height="600" allowfullscreen="true"></iframe>

## Existing Conditions Analysis

Existing conditions analysis consists of examining the geometry and physical conditions of the intersection, as well as evaluating traffic operational conditions. 

### Geometry and physical conditions

The intersection of Fifth Street and Green Street is a two-way stop-controlled intersection with stop signs placed on Fifth Street. As seen in **Figure 1**, this intersection lies between two signalized intersections in the East-West direction (Green Street) and between a series of all-way stop-controlled intersections in the North-South direction (Fifth Street).

The minor road, Fifth Street, is an undivided two-lane roadway. Parking is allowed along the southbound lane of Fifth Street (see **Figure 2**). During the period of crash data analysis (2015 to 2019), Green Street was a three-lane undivided roadway with one through lane in each direction and a center two-way left-turn lane. Following an extensive roadway redesign project along the Green Street corridor, green bike lanes with Helmeted Bicyclist Symbols, arrow pavement markings, and green pavement paints were installed along both through lanes. The roadway configurations in 2019 and 2021 can be seen in **Figures 3.1 – 3.4**. 

{{<image src="control_map.png"
    caption="Figure 1 - Control types of the surrounding intersections"
    alt="Map labeling the control types of Fifth and Green and the surrounding intersections on Fifth Street and Green Street">}}

{{<image src="figure 2.png"
    caption="Figure 2 - On-street parking on the southbound lane of S Fifth Street (Google Maps)"
    alt="Photo of vehicles parked on the street">}}

{{<image src="figure 3-1.png"
    position="full"
    alt="Photo of intersection of Fifth Street and Green Street">}}    

{{<image src="figure 3-2.png"
    position="full"
    caption="Figures 3.1-3.2 - Intersection Configurations in 2019 (Google Maps)"
    alt="Photo of intersection of Fifth Street and Green Street">}}

{{<image src="figure 3-3.jpg"
    alt="Photo of bike lane crossing an intersection">}}

{{<image src="figure 3-4.jpg"
    position="full"
    caption="Figures 3.3-3.4 - New bike lanes in 2021"
    alt="Photo of bike lane running mid-block">}}

### Vehicle Traffic Operational Conditions

Vehicle traffic count data for this intersection was collected from 7 AM to 7 PM on Thursday, September 23th, 2021. Passenger vehicles (also referred to as light vehicles), trucks, buses, and on-road bicyclists are included in the vehicle traffic volume calculations. Light vehicles (88.2%) and on-road bicyclists (9.2%) made up the most substantial portions of traffic. The vehicle traffic volumes at each approach are shown in **Figure 4**, and the peak hour turning movement counts for each direction are shown in **Figure 5**. The 12-hour traffic count data shows that the east and west legs (Green Street) have a higher traffic volume than the north and south legs (Fifth Street). The AM peak is 10 AM-11 AM, the midday peak is 12 PM – 1 PM, and the PM peak is 4:30 PM – 5:30 PM. The level of service and control delay during the morning, midday, and afternoon peak hours are shown in **Table 1**. All of the approaches operate under acceptable conditions, using LOS D as the minimum standard in urban areas. 

<rpc-chart url="15 minute volume.csv"
      chart-title="Figure 4 - Vehicle Traffic Volume at Fifth Street and Green Street"
      x-label="Time"
      y-label="Traffic Counts"
      type="line"></rpc-chart> 

{{<image src="turning_movements.png"
    caption="Figure 5 - Turning Movement Counts"
    alt="Graphic showing the number of movements made at this intersection in each direction">}}

<rpc-table url="los.csv"
  table-title="Table 1 - Approach Level-of-Service and Control Delay at Study Intersection"
  text-alignment="c,c"></rpc-table>


### Pedestrian and Bicyclists Crossing Activities

This intersection has a high volume of pedestrian and bicyclist crossings. The 15-minute volume count data for these crossings can be found in **Figure 6**, and the aggregated 12-hour total volumes for each direction can be found in **Figure 7**. More pedestrians and bicycles cross the intersection in the east-west direction (crossing Fifth Street) than the north-south direction (crossing Green Street). The detailed 15-minute volume counts in each direction for Fifth Street can be found in **Figures 8.1 and 8.2**.

<rpc-chart url="15 minute ped.csv"
      chart-title="Figure 6 - Pedestrian and Bicycle Crossings of Fifth Street and Green Street"
      x-label="Time"
      y-label="Traffic Counts"
      type="line"></rpc-chart> 

{{<image src="bike ped crossings.png"
    caption="Figure 7 - Pedestrian and Bicycle Crossings of Fifth Street (North-South) and Green Street (East-West)"
    alt="Chart showing bike and pedestrian crossings in each direction on each leg of intersection over the course of the day">}}

<rpc-chart url="north ped bar.csv"
  chart-title="Figure 8.1 - Pedestrian and Bicycle Crossings of the North Leg of Fifth and Green"
  x-label="Time"
  y-label="Traffic Counts"
  type="bar"></rpc-chart>

<rpc-chart url="south ped bar.csv"
  chart-title="Figure 8.2 - Pedestrian and Bicycle Crossings of the South Leg of Fifth and Green"
  x-label="Time"
  y-label="Traffic Counts"
  type="bar"></rpc-chart>

## Traffic Safety Analysis

### Crash Data Analysis 

Crash data from 2015 to 2019 was analyzed for the intersection of Fifth Street and Green Street. Seventeen crashes occurred at the study location over these five years. **Figure 9** shows the location and type for each of the collisions at this intersection.

{{<image src="crash diagram.png"
    caption="Figure 9 - Collision Diagram"
    alt="Plan view diagram of intersection, showing the type and location for all crashes from 2015 to 2019">}}

#### Crash Trends and Time

**Figure 10** show the timing of crashes that occurred at the intersection during these five years. These indicate that February-March and August-September were the months with the highest number of crashes. 11 AM-1 PM, 5-7 PM, and 9-10 PM were the time windows with the highest crash frequency. 

<rpc-chart url="crash_count_year.csv"
      chart-title="Figure 10.1 - Number of Crashes by Year"
      x-label="Year"
      y-label="Crash Counts"
      type="bar"></rpc-chart>

<rpc-chart url="crash_count_month.csv"
      chart-title="Figure 10.2 - Number of Crashes by Month"
      x-label="Month"
      y-label="Crash Counts"
      type="bar"></rpc-chart>

<rpc-chart url="crash_count_day.csv"
      chart-title="Figure 10.3 - Number of Crashes by Day of the Week"
      x-label="Day"
      y-label="Crash Counts"
      type="bar"></rpc-chart>

<rpc-chart url="crash_count_hour.csv"
      chart-title="Figure 10.4 - Number of Crashes by Hour of the Day"
      x-label="Hour"
      y-label="Crash Counts"
      type="bar"></rpc-chart>

#### Crash Severity

Crash severity levels are generally classified using the KABCO scale: Fatal ( K ), A-Injury ( A ), B-Injury ( B ), C-Injury ( C ), and No Injury ( O ). Definitions of KABCO severity levels can be found on the Appendix page. **Table 2** and **Figure 11** present the number of crashes by severity level at the intersection of Fifth Street and Green Street. There were no fatal crashes at the study intersection from 2015 to 2019. One A-Injury crash and three C-Injury crashes occurred; however, most crashes were No-Injury crashes.

<rpc-table url="crash_severity_year.csv"
  table-title="Table 2 - Number of Crashes by Severity Level and Year"
  text-alignment="c,c"></rpc-table>

<rpc-chart url="crash_severity_year_chart.csv"
      chart-title="Figure 11 - Number of Crashes by Severity Level and Year"
      x-label="Severity Level"
      y-label="Crash Counts"
      type="bar"
      stacked="true"></rpc-chart>

#### Crash Types

**Table 3** and **Figure 12** show the count of crashes at the intersection of Fifth Street and Green Street from 2015 to 2019, sorted by year and crash type. **Table 4** and **Figure 13** sort each crash type by the number of crashes for each KABCO severity level.  Angle crashes were the predominant crash type at the intersection, accounting for 82% of all crashes. Most angle crashes led to no injuries, but one lead to an A-Injury, and three led to C-Injuries. A detailed analysis of the angle crashes can be found below in Section 3.1.3.1. 

The other crash types at this intersection were parked motor vehicle crashes and a single turning crash. The turning crash occurred when one vehicle tried to make a left turn from the through lane. The two parked motor vehicle crashes occurred in the southbound lane of the north leg. In one case, the crash was the result of the southbound vehicle attempting to avoid a large vehicle traveling north and subsequently colliding with a parked vehicle. The other occurred when a parked vehicle pulled away from its parking spot and collided with another parked vehicle. 


<rpc-table url="crash_type_year.csv"
  table-title="Table 3 - Number of Crashes by Crash Type and Year "
  text-alignment="c,c"></rpc-table>

<rpc-chart url="crash_type_year_chart.csv"
      chart-title="Figure 12 - Number of Crashes by Crash Type and Year"
      x-label="Crash Type"
      y-label="Crash Counts"
      type="bar"
      stacked="true"></rpc-chart>

<rpc-table url="crash_severity_type.csv"
  table-title="Table 4 - Number of Crashes by Crash Type and Severity Level "
  text-alignment="c,c"></rpc-table>

<rpc-chart url="crash_severity_type_chart.csv"
      chart-title="Figure 13 - Number of Crashes by Crash Type and Severity Level"
      x-label="Severity Level"
      y-label="Crash Counts"
      type="bar"
      stacked="true"></rpc-chart>

##### Angle crashes

From 2015 to 2019, 14 angle crashes occurred at the study intersection. Nine crashes occurred in the northeast corner (a vehicle heading north colliding with a vehicle heading west). Four crashes occurred in the southeast corner (a vehicle heading north colliding with a vehicle heading east). One crash occurred in the northwest corner (a vehicle heading south colliding with a vehicle heading west). 

Of the nine crashes in the northeast corner, two involved the driver heading north being motioned to proceed by a different westbound driver. In four of the northeast crashes, the driver heading north did not see or misjudged the speed of the westbound vehicle. The remaining three were caused by northbound vehicles failing to yield right of way (since Fifth Street is stop-controlled and Green Street is uncontrolled). 

The four crashes in the southeast corner each had distinct causes. One occurred when the vehicle on Fifth Street slid on ice. In another, the driver on Fifth Street believed the intersection to be an all-way stop-controlled intersection. The third crash occurred when the northbound vehicle was speeding. The last crash can be attributed to the driver on Fifth Street failing to yield right of way. 

In the crash that occurred in the northwest corner, the driver heading south stopped at the intersection but failed to see the vehicle coming from the east. 

### Field Investigation

During the in-person field investigation, staff observed road user behaviors that pose safety concerns at the study intersection and the corresponding roadways. First, staff observed pedestrians crossing the street in the middle of the block, rather than using crosswalks. This behavior increases the chance of pedestrian crashes, requires vehicles to slow down or stop frequently before entering the intersection, and increases the risks of rear-end crashes. Second, while engaging in passenger drop-off or food pick-up, some vehicles stopped and parked in the bike lanes surrounding this intersection. This may impede bike traffic and pose safety concerns to road users. Third, due to the high volume of pedestrians, some drivers on Fifth Street were required to wait for an extended period before entering the intersection. This situation may cause drivers to lose patience and enter the intersection before it is safe to do so. 

The other potential risk at the study intersection is the allowance of on-street parking near the intersection along the southbound lane of the north leg. Drivers who enter the intersection from the north may need to deviate from their lane (and into the northbound lane) to avoid the vehicles that are parked on their right side. This risk is further increased when the parked vehicle is large or there is another vehicle entering the road at the same time. 

## Findings and Proposed Countermeasures

### (1) Remind drivers to stop and watch carefully

Most crashes at this intersection were caused by drivers failing to yield right of way or misjudging the time and distance of oncoming roadway users. To remind drivers to stop completely at the stop sign and watch carefully before entering the intersection, installing a “CROSS TRAFFIC DOES NOT STOP” plaque under the STOP sign for both legs of Fifth Street is recommended. 

In addition, the study intersection is located amongst a series of all-way stop-controlled (AWSC) intersections along Fifth Street. The three intersections south of Green Street are all AWSC. This could lead drivers approaching from the south to misjudge the control type of the study intersection—believing this intersection to also be AWSC if they are not already familiar with it. To further remind drivers that this intersection is a two-way stop-controlled intersection, adding a ‘2-WAY’ plaque can also be considered. 

### (2) Regulate pedestrian activities

Although the nearest crosswalk is at most 200 ft away, some pedestrians still choose to cross the street in the middle of the block instead of using the crosswalk. This brings danger not only to the pedestrians but also to other road users. To regulate pedestrian activities, extending pedestrian barricades along the sidewalk at appropriate locations should be considered.
