---
title: "Urban Unsignalized Intersections"
date: 2018-01-18T11:32:26-06:00
draft: false
menu: main
weight: 40
bannerHeading: Urban Unsignalized Intersections
bannerText: >
bannerAction: Provide Feedback
bannerUrl: /contact
---

In the urban area, five unsignalized intersections were selected as high-priority locations. The locations are shown below:

<rpc-table url="urb_unsig.csv" table-title="Urban Unsignalized Intersections"> </rpc-table>