---
title: "McKinley Avenue and Bradley Avenue"
draft: false
weight: 40
bannerHeading: McKinley Avenue and Bradley Avenue.
bannerText: >
  The intersection of McKinley Avenue and Bradley Avenue.
bannerAction: Provide Feedback
bannerUrl: /contact
---

## Introduction

This study analyzes safety conditions at the intersection of McKinley Avenue and Bradley Avenue, an unsignalized intersection in the City of Champaign. This intersection was selected as one of the top high-priority urban unsignalized intersections in the analysis for the Champaign County Safety Plan implementation. **Map 1** shows the location of the intersection. McKinley Avenue is a north-south major collector, while Bradley Avenue serves as an east-west minor arterial.

<iframe src="https://maps.ccrpc.org/ccspi-urban-intx-mckinley-bradley/" width="100%"  height="600" allowfullscreen="true"></iframe>

## Existing Conditions Analysis

The existing condition analysis for the intersection of McKinley Avenue and Bradley Avenue consists of an evaluation of the intersection's physical and geometrical conditions and the traffic operational conditions of its roadway approaches. 

### Physical Condition

The study intersection is an all-way stop-controlled intersection. McKinley Avenue is a two-lane road with one lane per direction. This roadway's annual average daily traffic (AADT) is between 2,150 and 3,550 vehicles per day[^1], according to Illinois Department of Transportation (IDOT) data from 2021. Each lane is approximately 19 feet wide along this roadway, with curb parking along the south side of the intersection and no median on either the north or south legs. The speed limit for McKinley Avenue is 30 miles per hour (mph). Bradley Avenue is a four-lane road with two lanes per direction. To the east of the intersection, Bradley Avenue has no median and each lane is approximately 10 feet wide. To the west of the intersection, there is a four-foot-wide marked median, and each lane is approximately 11 feet wide. To the east and west, Bradley Avenue has no street parking available and a 35-mph speed limit. The AADT of this roadway is between 13,200 and 13,500 vehicles per day[^1], according to 2021 IDOT data. Road markings are generally fully intact around this intersection. However, there are a few places where markings are partially missing (shown in **Figure 1**) and road surface condition is poor to average. The east/west stop signs on Bradley Avenue feature LED lights. In addition, there are Stop Ahead signs on the Bradley Avenue approaches to the intersection in both directions. One bus shelter is located on the southwest corner of the intersection, along Bradley Avenue (shown in **Figure 2**). Streetlights are present on each leg of the intersection, and a 2019 project improved the lighting at this intersection. Standard pedestrian crossing markings are present on each leg of the intersection. **Figure 3** shows the geometry of the study intersection. 

{{<image src="Figure1.png"
    caption="Figure 1 - Partial road marking missing on the north side of the intersection">}}

{{<image src="Figure2.png"
    caption="Figure 2 - Bus shelter on the southwest corner of the intersection on Bradley Avenue">}}

{{<image src="Figure3.png"
    caption="Figure 3 - Geometric condition of McKinley Avenue and Bradley Avenue intersection">}}

Pedestrian and bicyclist volumes are relatively low at the study intersection. The surrounding land uses at the intersection of McKinley Avenue and Bradley Avenue are mostly residential and industrial. Sidewalks are present, with the exception of the southwest corner of the intersection, along the western edge of McKinley Avenue.

### Traffic Operational Conditions

#### Peak Hour vehicular Traffic

On Thursday, September 16th, 2021, twelve hours of vehicular, pedestrian, and bicycle movement data for this intersection were collected from 7 AM to 7 PM. Passenger vehicles (also referred to as light vehicles) made up the most substantial portion of vehicle traffic, at 94.5%. **Figure 4** shows the vehicle traffic volume at each approach, and **Figure 5** shows the peak hour turning movement counts for each direction. The 12-hour traffic count data shows that the north and south legs (McKinley Avenue) have lower traffic volumes than the east and west legs (Bradley Avenue). The morning peak hour is 7:30 AM-8:30 AM, the noon peak hour is 12:00 PM – 1:00 PM, and the afternoon peak hour is 2:45 PM – 3:45 PM. Shift changes at the nearby Kraft Foods facility may contribute to the timing of the afternoon peak hour.

<rpc-chart url="15 minute volume.csv"
      chart-title="Figure 4 - Vehicle traffic volume at McKinley Avenue and Bradley Avenue intersection"
      x-label="Time"
      y-label="Traffic Counts"
      type="line"></rpc-chart> 

{{<image src="Figure5.png"
    caption="Figure 5 - Turning movement counts at McKinley Avenue and Bradley Avenue intersection">}}

#### Vehicle Level-Of-Service

Level of service (LOS) is a qualitative measure describing operational conditions from "A" (best) to "F" (worst) within a traffic stream or at an intersection. This measurement is quantified for signalized and unsignalized intersections in terms of vehicle control delay.

Control delay is a component of delay that results from the type of traffic control at the intersection or approach, measured by comparison with the uncontrolled condition. It is the difference between the travel time that would have occurred in the absence of the intersection control and the travel time that results from the intersection control's presence.

**Table 1** shows the LOS criteria for All-Way-Stop-Controlled intersections or unsignalized intersections as per the Highway Capacity Manual 6th Edition

<rpc-table url="LOS criteria for unsignalized intersections.csv"
  table-title="Table 1 - LOS criteria for unsignalized intersections"
  text-alignment="c,c"></rpc-table>

The LOS and control delays during the morning (AM), noon, and afternoon (PM) peak hours are shown in **Table 2**. The overall intersection operational condition is an LOS of D for morning and afternoon peak hour traffic. However, the eastbound approach shows significant delays during the morning and afternoon peak hours.

<rpc-table url="LOS at McKinley and Bradley.csv"
  table-title="Table 2 - Approach level of service and control delay"
  text-alignment="c,c"></rpc-table>

## Traffic Safety Analysis

### Crash Data Analysis 

Crash data from 2015 to 2019 was analyzed for the intersection of McKinley Avenue and Bradley Avenue. The crash data information was obtained from the IDOT Division of Traffic Safety. There were 24 crashes at the study location over this five-year span. **Figure 6** shows the collision diagram for the intersection. 

{{<image src="Figure6.png"
    caption="Figure 6 - Collision diagram at McKinley Avenue and Bradley Avenue intersection">}}

#### Crash Trend and Time

**Figure 7.1 - 7.4** show the time of crashes from 2015 to 2019 at the intersection of McKinley Avenue and Bradley Avenue. This shows that seven crashes occurred in 2016, which was the highest annual total within the study period. April had a relatively higher number of crashes, with four crashes in total. Tuesday and Thursday were the days of the week that had the highest number of crashes, with seven crashes each day. The time period of highest crash frequency was from 2 PM to 7 PM. Approximately 60% of the crashes at the study intersection occurred during this time period.

<rpc-chart url="crash_count_year.csv"
      chart-title="Figure 7.1 - Number of Crashes by Year"
      x-label="Year"
      y-label="Crash Counts"
      type="bar"></rpc-chart>

<rpc-chart url="crash_count_month.csv"
      chart-title="Figure 7.2 - Number of Crashes by Month"
      x-label="Month"
      y-label="Crash Counts"
      type="bar"></rpc-chart>

<rpc-chart url="crash_count_day.csv"
      chart-title="Figure 7.3 - Number of Crashes by Day of the Week"
      x-label="Day"
      y-label="Crash Counts"
      type="bar"></rpc-chart>

<rpc-chart url="crash_count_hour.csv"
      chart-title="Figure 7.4 - Number of Crashes by Hour of the Day"
      x-label="Hour"
      y-label="Crash Counts"
      type="bar"></rpc-chart>

#### Crash Severity

Crash severity levels are generally classified—in order of decreasing severity—as Fatal ( K ), A-Injury ( A ), B-Injury ( B ), C-Injury ( C ), and No Injury ( O ). The definitions of these KABCO severity levels can be found in **Appendix A**. **Table 3** and **Figure 8** present the number of crashes by severity level at the study location. There were no fatal crashes from 2015 to 2019 at the intersection of McKinley Avenue and Bradley Avenue. Most crashes (71%) were No-Injury crashes. One A-Injury crash, three B-Injury crashes, and three C-Injury crashes occurred in the five-year study period. 

<rpc-table url="crash_severity_year.csv"
    table-title="Table 3 - Number of crashes by severity and year"
    text-alignment="c,c"></rpc-table>

<rpc-chart url="crash_severity_year_chart.csv"
      chart-title="Figure 8 - Number of Crashes by Severity Level and Year"
      x-label="Severity Level"
      y-label="Crash Counts"
      type="bar"
      stacked="true"></rpc-chart>

#### Crash Types 

**Table 4** and **Figure 9** show the types of crashes by year at the intersection of McKinley Avenue and Bradley Avenue. As shown in the figure, angle crashes were the predominant crash type at the intersection, accounting for 42% of all crashes. Rear-end crashes were also prevalent at the study intersection, accounting for 33% of the crashes. A detailed analysis of the angle and rear-end crashes can be found in the following Sections. There were also three turning crashes, two pedalcyclist-related crashes, and one sideswipe crash at this location. 

<rpc-table url="crash_type_year.csv"
  table-title="Table 4 - Number of crashes by crash type and year"
  text-alignment="c,c"></rpc-table>

<rpc-chart url="crash_type_year_chart.csv"
      chart-title="Figure 9 - Number of Crashes by Crash Type and Year"
      x-label="Crash Type"
      y-label="Crash Counts"
      type="bar"
      stacked="true"></rpc-chart>

  **Table 5** and **Figure 10** quantify each type of crash by severity level. One angle crash led to a C Injury, while all other angle crashes resulted in no injuries. Pedalcyclist crashes resulted in the only A Injury at this intersection, as well as a B Injury. Based on this, pedalcyclist crashes were relatively severe compared to other crashes. Two rear-end crashes led to a B Injury, and two led to C Injury crashes. All turning and sideswipe crashes led to no injuries. 

<rpc-table url="crash_severity_type.csv"
  table-title="Table 5 - Number of Crashes by Severity Level and Crash Type"
  text-alignment="c,c"></rpc-table>

<rpc-chart url="crash_severity_type_chart.csv"
      chart-title="Figure 10 - Number of crashes by crash type and severity level"
      x-label="Severity Level"
      y-label="Crash Counts"
      type="bar"
      stacked="true"></rpc-chart>

{{<accordion>}}
  {{<accordion-content title="Angle crashes">}}
    From 2015 to 2019, ten angle crashes occurred at the McKinley Avenue and Bradley Avenue intersection, making angle crashes the predominant crash type. In these angle crashes, eastbound and westbound vehicles traveling on Bradley Avenue were typically responsible, with eastbound being the more common direction of responsible vehicles. All angle crashes can be attributed to failing to yield right of way or disregarding a stop sign. Crash data showed that five crashes occurred when the driver failed to yield the right of way, four crashes occurred when the vehicle disregarded the stop sign, and the other crash occurred because of driving skill.
  {{</accordion-content>}}

  {{<accordion-content title="Rear-end crashes">}}
    From 2015 to 2019, eight rear-end crashes occurred at the study intersection. All rear-end crashes occurred on Bradley Avenue, with five occurring on the west leg and three on the east leg. Six of these crashes can be attributed to vehicles failing to reduce speed to avoid crashes. The other two crashes were attributed to the driver's driving skills and following too closely. 
  {{</accordion-content>}}

{{</accordion>}}

## Traffic Signal Warrant Study

A warrant for any traffic control device (sign, signal, or pavement marking) is the minimum criteria that must be met before such a device can be installed. If one or more Manual of Uniform Traffic Control Devices (MUTCD) traffic signal warrants are met, a traffic signal can be considered for installation. Meeting a warrant does not mean a traffic control device must be installed. Engineering judgment should be used to determine whether installing a traffic signal would improve the intersection's overall safety and operational efficiency.

A traffic signal warrant analysis was completed for the McKinley Avenue and Bradley Avenue Intersection. The signal warrant study was performed by analyzing the relevant factors described in the nine (9) traffic signal warrants specified in the MUTCD-2009 Edition[^2]. 

The traffic signal warrants include:

1. Warrant 1, Eight-Hour Vehicular Volume
2. Warrant 2, Four-Hour Vehicular Volume 
3. Warrant 3, Peak Hour 
4. Warrant 4, Pedestrian Volume
5. Warrant 5, School Crossing 
6. Warrant 6, Coordinated Signal System 
7. Warrant 7, Crash Experience 
8. Warrant 8, Roadway Network 
9. Warrant 9, Intersection Near a Grade Crossing

In this analysis, Bradley Avenue was considered the major street, and McKinley Avenue was considered a minor street. The posted speed limit on Bradley Avenue was 35 mph, and 12-hour traffic data was used to complete the analysis. Staff used Highway Capacity Software (HCS) for traffic signal warrant analysis for 2021 data. The HCS software was developed by the McTrans Center[^3]. **Appendix B** contains detailed calculations for checking the applicability of the traffic signal warrants. The signal warrant analysis showed that three warrants—Warrant 1: Eight-Hour Vehicular Volume, Warrant 2: Four-Hour Vehicular Volume, and Warrant 7: Crash Experience—were satisfied for the intersection of McKinley Avenue and Bradley Avenue.

## Findings and Proposed Countermeasures

### Study Findings 

Several risk factors were identified after analyzing the crash data and site investigation results. The identified findings are listed below:

- There is no sidewalk along McKinley Avenue on the southwest side of this intersection.
- LED stop signs were placed on Bradley Avenue sometime in 2020, and they are still present at this intersection. The impact of their presence is not reflected in the crash data analyzed between 2015 and 2019.
- Pedestrian and bicyclist volumes at the study intersection were low. The highest hourly pedestrian volume was 28 (between 3 PM and 4 PM), and the highest hourly bicyclist volume was six (between 5 PM and 6 PM).
- Eastbound traffic faces significant delays for both morning and afternoon peak hour traffic. 
- In many crashes, eastbound vehicles were at fault, and the average speed was high on the west side of the intersection.
- Angle crashes are the predominant type of crash.
- There were two pedalcyclist crashes at the study intersection, and both resulted in injuries. 
- MUTCD traffic signal Warrant 1: Eight-Hour Vehicular Volume, traffic signal Warrant 2: Four-Hour Vehicular Volume, and traffic signal Warrant 7: Crash Experience were satisfied for this intersection

### Proposed Countermeasures

After analyzing the operational and safety issues at the intersection of McKinley Avenue and Bradley Avenue, the following short-term and long-term recommendations are provided. 

#### (1) Short-Term Recommendations

##### Roadway Markings and Signs

As identified in the existing condition analysis section, some road markings are partially missing, and some other markings are faded. Considering this, improvement of road markings around this intersection is recommended. 

LED stop signs are only present on Bradley Avenue, so installing LED stop signs on McKinley Avenue is recommended to increase the visibility of stop signs to drivers (if the current intersection control type is maintained). 

From crash reports, it was found that at least two crashes (including one A Injury crash) occurred due to drivers disregarding stop signs on McKinley Avenue. "Stop Sign Ahead" signs can be placed on both the north and south legs of the intersection, and "STOP" pavement markings can be installed at every approach to make drivers aware of the need to stop at the intersection. This countermeasure has a crash modification factor (CMF) of 0.59 for angle crashes[^4]. 

Additionally, providing a speed limit sign in each approaching direction is recommended. Dynamic Speed Display Signs (DSDS) can be periodically placed on Bradley Avenue near the study intersection. Studies show that DSDS is effective in reducing vehicular speed[^5]. Law enforcement should also increase their speed enforcement activities on Bradley Avenue, as the effectiveness of DSDS depends on the perceived level of enforcement among the vehicle drivers.

##### Sidewalk

Currently, no sidewalk is present along McKinley Avenue on the southwest side of this intersection (see **Figure 11**). There is a bus shelter located on Bradley Avenue on the southwest corner of this intersection. Providing a sidewalk on the west side of the south leg of the intersection would facilitate access to this bus stop. The City of Champaign has a 13-foot right-of-way (ROW) along the south leg of McKinley Avenue, which provides enough ROW to build a sidewalk on the west side of the street. The City of Champaign should consider extending this proposed sidewalk south to West Tremont Street, connecting the proposed sidewalk with an existing sidewalk.

{{<image src="Figure11.png"
    caption="Figure 11 - No sidewalk on the southwest side of the intersection">}}

#### (2) Long-Term Recommendations

##### Intersection Control

Many crashes occurred due to disregard of stop signs. This is a typical safety issue at unsignalized intersections. In addition, eastbound traffic sees significant delays in both morning and afternoon peak hours. Because of this, the possibility of implementing a roundabout or traffic signal at this intersection should be evaluated for future implementation. Installing a traffic signal has a crash modification factor of 0.86 for all crashes[^6] and 0.33 for angle crashes[^7]. However, installing a traffic signal would increase the possibility of rear-end crashes. The conversion of a stop-controlled intersection into a multilane roundabout has a CMF of 0.62 for all crashes[^8]. 

To understand future mobility considerations, staff calculated the intersection's future Level of Service (LOS) and traffic delays, using the future traffic volumes forecasted for 2045 for different intersection control types (unsignalized, roundabout, and traffic signal). These analyses were done using the Synchro 10 software suite. The intersection's future turning movement volumes were projected by utilizing CUUATS Travel Demand Model's (TDM) forecasted growth rates for the study corridor. Using the TDM 2015 traffic volume as the baseline, the TDM Long Range Transportation Plan (LRTP) 2045 preferred scenario was used to obtain the projected future traffic volumes for 2045. Based on the 2015 and 2045 traffic volumes, the compound annual growth rate (CAGR) was estimated for each approach, and turning movement counts for each approach were calculated using the CAGR for 2045. **Table 6** shows the Year 2045 comparison of different control types for this intersection. An exclusive left-turn lane for Bradley Avenue was also considered for the signalized intersection analysis, but the left turn lane warrant wasn’t met according to the BDE manual[^9].

<rpc-table url="2045_LOS.csv"
  table-title="Table 6 - Year 2045 comparison with different intersection control types"
  text-alignment="c,c"></rpc-table>

Table 5 shows that both a roundabout and a traffic signal would operate well in 2045, and an all-way stop-controlled intersection will fail, operating at LOS F (the worst LOS category). Considering that Bradley Avenue has four lanes, implementing a multilane roundabout would represent a significant challenge, due to the need to acquire the required right of way to build a roundabout at this intersection. As shown above, a traffic signal warrant analysis satisfied three warrants. Installing a traffic signal would require less right of way than a roundabout. However, according to CUUATS access management guidelines[^10], 1,540 feet of spacing is required between two traffic signals. Currently, there is an existing traffic signal located at Prospect Avenue and Bradley Avenue, approximately 1,300 feet east of the intersection of McKinley Avenue and Bradley Avenue. Because of this, installing a traffic signal would not comply with the existing CUUATS access management guidelines.

##### Traffic Calming

The review of crash reports showed that several crashes occurred due to speeding. In many crashes, eastbound vehicles were at fault. Six out of the eight rear-end crashes occurred due to failure to reduce speed to avoid crashes, which suggests speeding around this intersection, primarily along Bradley Avenue. Currently, Bradley Avenue west of McKinley Avenue has eleven-foot-wide lanes, which can be reduced to ten-foot-wide lanes. Reducing lane width would lower vehicular speed, while also providing consistent roadway width along this whole section of Bradley Avenue. The remaining roadway width can be utilized to increase the width of the median from four feet to eight feet, which would improve safety by allowing pedestrians to cross one direction of traffic at a time (as shown in **Figure 12**). The average annual daily traffic (AADT) of Bradley Avenue between Prospect Avenue and Mattis Avenue is between 13,200 and 13,500. According to the Federal Highway Administration (FHWA), a roadway with ADT between 10,000-15,000 is a good candidate for a road diet in many instances[^11]. The city of Champaign may conduct intersection analyses and consider signal retiming at other intersection locations in conjunction with a possible road diet implementation. This will help calm traffic along Bradley Avenue and reduce the right-of-way required to change the intersection controls at this intersection. It is important to note that a lane reduction would not be possible without changing the intersection control to a traffic signal or a roundabout, due to the resulting delays and queueing of reduced lanes under the current stop control. Lane reconfiguration may also help to facilitate bike lanes along Bradley Avenue. 

{{<image src="Figure12.png"
    caption="Figure 12 - Proposed roadway geometry">}}

## References

[^1]:	Illinois Department of Transportation (https://www.gettingaroundillinois.com/Traffic%20Counts/index.html)
[^2]:	Manual of Uniform Traffic Control Devices (MUTCD), United States Department of Transportation - Federal Highway Administration (https://mutcd.fhwa.dot.gov/htm/2009/part4/part4c.htm)
[^3]:	HCS software developed by McTrans Center (https://mctrans.ce.ufl.edu/hcs/)
[^4]:	CMF Clearinghouse (https://www.cmfclearinghouse.org/detail.cfm?facid=6602)
[^5]:	Texas Transportation Institute – Effectiveness of Dynamic Speed Display Signs (DSDS) in Permanent Application, Project Summary Report O-4475-S, Texas Transportation Institute, TTI, College Station, TX 2004.
[^6]:	CMF Clearinghouse (https://www.cmfclearinghouse.org/detail.cfm?facid=316)
[^7]:	CMF Clearinghouse (https://www.cmfclearinghouse.org/detail.cfm?facid=320)
[^8]:	CMF Clearinghouse (https://www.cmfclearinghouse.org/detail.cfm?facid=10082)
[^9]:	Bureau of Design and Environment Manual by Illinois Department of Transportation
(https://idot.illinois.gov/Assets/uploads/files/Doing-Business/Manuals-Guides-&-Handbooks/Highways/Design-and-Environment/Design%20and%20Environment%20Manual,%20Bureau%20of.pdf)
[^10]:	Access Management Guidelines for the Urbanized Area (https://ccrpc.org/wp-content/uploads/2015/03/access-management-2013-04-17-final.pdf)
[^11]:	Federal Highway Administration (https://safety.fhwa.dot.gov/road_diets/resources/pdf/fhwasa17021.pdf)
