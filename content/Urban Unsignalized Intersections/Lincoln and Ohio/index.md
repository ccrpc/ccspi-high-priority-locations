---
title: "Lincoln Avenue and Ohio Street"
draft: false
weight: 20
bannerHeading: Lincoln Avenue and Ohio Street
bannerText: >
  The intersection of Lincoln Avenue and Ohio Street.
bannerAction: Provide Feedback
bannerUrl: /contact
---

## Introduction

This study analyzes the safety of the intersection of Lincoln Avenue and Ohio Street. **Map 1** shows the location of the study intersection. Several regional destinations are located near this intersection, such as University of Illinois athletic facilities, residence halls, and academic facilities, as well as a historic residential neighborhood.

<iframe src="https://maps.ccrpc.org/ccspi-urban-intx-lincoln-ohio/" width="100%"  height="600" allowfullscreen="true"></iframe>

## Existing Conditions Analysis

Existing conditions analysis consists of examining the geometry and physical conditions of the intersection, as well as evaluating traffic operational conditions. 

### Geometry and physical conditions

The intersection of Lincoln Avenue and Ohio Street is a one-way stop-controlled T-intersection, with the stop sign placed on Ohio Street. Ohio Street is a two-lane local road with one lane on each direction. There are no stop line or center line markings on Ohio Street. Parallel curb parking is permitted on the south side of Ohio Street. Champaign-Urbana Mass Transit District (CUMTD) bus stops are located on both sides of Lincoln Avenue, approximately 150 feet north of the study intersection. A 7-foot loading zone is provided on the east side of Lincoln Avenue. A crosswalk is located on the south leg of the study intersection. **Figure 1.1-1.2** show the configuration of the intersection. 

{{<image src="figure1-1.png"
    caption="Figure 1-1 - Lincoln Avenue and Ohio Street Intersection Configuration">}}

{{<image src="figure1-2.png"
    caption="Figure 1-2 - Lincoln Avenue and Ohio Street Intersection Configuration">}}

This intersection is one of four consecutive stop-controlled T-intersections on Lincoln Avenue between Ohio Street and Michigan Avenue. These intersections have similar configurations, as shown in **Figure 2**. The intersection of Nevada Street and Lincoln Avenue, which is approximately 830 feet to the north, is a signal-controlled intersection. The intersection of Pennsylvania Avenue and Lincoln Avenue, which is approximately 1,100 feet to the south, is also a signal-controlled intersection. From Nevada Street to Michigan Avenue, Lincoln Avenue is a three-lane minor arterial with a center turning lane. 

{{<image src="figure2.png"
    caption="Figure 2 - Roadway configuration of Lincoln Ave from Nevada Street to Michigan Avenue (Not to scale)">}}

### Traffic Operation Conditions

Vehicular traffic count data for this intersection was collected from 7AM to 7PM on September 16th, 2021. Most vehicles traveling at this intersection were passenger vehicles. The 15-minute vehicle traffic counts at each approach are shown in **Figure 3**, and the turning movement counts for each direction are shown in **Figure 4**. 

<rpc-chart url="15 minute vehicle volume.csv"
      chart-title="Figure 3 - Vehicle Traffic Volume at Bradley Avenue and Mattis Avenue.png"
      x-label="Time"
      y-label="Traffic Counts"
      type="line"></rpc-chart> 

{{<image src="figure4.png"
    caption="Figure 4 - Turning Movement Counts"
    alt="Graphic showing the number of movements made at this intersection in each direction">}}

### Pedestrian and bicyclist activities 

This intersection has a high volume of pedestrian and bicyclist crossings. The 15-minute volume count data for these crossings can be found in **Figure 5**, and the aggregated 12-hour total volumes for each direction can be found in **Figure 6**, where the counterclockwise direction is marked with diagonal stripes. Although there is no crosswalk on the north leg of the intersection, some pedestrians and bicyclists still crossed Lincoln Avenue using the north leg of the intersection. For the south leg, the total volumes in the clockwise direction (east to west) and the counterclockwise (west to east) direction were balanced. For the east leg, more pedestrians traveled from counterclockwise (south to north) than clockwise (north to south). The detailed 15-minute volume counts for each direction of the south leg can be found in **Figure 7**. On the south leg, more pedestrians and bicyclists traveled clockwise (east to west) than counterclockwise (west to east) in the morning, while more pedestrians and bicyclists traveled counterclockwise than clockwise in the afternoon.  

<rpc-chart url="15 minute ped volume.csv"
      chart-title="Figure 5 - Pedestrian Volume at Bradley Avenue and Mattis Avenue.png"
      x-label="Time"
      y-label="Traffic Counts"
      type="line"></rpc-chart> 

{{<image src="figure6.png"
    caption="Figure 6 - 12-Hour Pedestrian and Bicyclist Crossings">}}

<rpc-chart url="15 minute ped volume - Southleg.csv"
      chart-title="Figure 7 - Pedestrian and Bicyclist Crossings of the South Leg.png"
      x-label="Time"
      y-label="Traffic Counts"
      type="bar"></rpc-chart> 
## Traffic Safety Analysis

### Crash Data Analysis 
Crash data from 2015 to 2019 was analyzed for this study intersection. Twenty-eight crashes occurred at the study location over these five years. **Figure 8** shows the location and type for each crash.

{{<image src="figure8.png"
    caption="Figure 8 - Collision Diagram">}}

#### Crash Trends and Time

**Figure 9** shows the timing of crashes that occurred at the intersection during these five years.  These graphs show that April, September, and October were the months with the highest number of crashes. Tuesday was the day of the week with the highest number of crashes. 5 PM-6 PM was the time window with the highest crash frequency.

<rpc-chart url="crash_count_year.csv"
      chart-title="Figure 9.1 - Number of Crashes by Year"
      x-label="Year"
      y-label="Crash Counts"
      type="bar"></rpc-chart>

<rpc-chart url="crash_count_month.csv"
      chart-title="Figure 9.2 - Number of Crashes by Month"
      x-label="Month"
      y-label="Crash Counts"
      type="bar"></rpc-chart>

<rpc-chart url="crash_count_day.csv"
      chart-title="Figure 9.3 - Number of Crashes by Day of the Week"
      x-label="Day"
      y-label="Crash Counts"
      type="bar"></rpc-chart>

<rpc-chart url="crash_count_hour.csv"
      chart-title="Figure 9.4 - Number of Crashes by Hour of the Day"
      x-label="Hour"
      y-label="Crash Counts"
      type="bar"></rpc-chart>

#### Crash Severity

Crash severity levels are generally classified using the KABCO scale: Fatal (K), A-Injury (A), B-Injury (B), C-Injury (C), and No Injury (O). Definitions of KABCO severity levels can be found on the Appendix page. **Table 1** and **Figure 10** present the number of crashes by severity level at the study location. There were no fatal crashes from 2015 to 2019. Most crashes were No-Injury crashes. Two A-Injury crashes, three B-Injury crashes and four C-Injury crashes occurred over this time period.  

<rpc-table url="crash_severity_year.csv"
  table-title="Table 1 - Number of Crashes by Severity Level and Year"
  text-alignment="c,c"></rpc-table>

<rpc-chart url="crash_severity_year_chart.csv"
      chart-title="Figure 10 - Number of Crashes by Severity Level and Year"
      x-label="Severity Level"
      y-label="Crash Counts"
      type="bar"
      stacked="true"></rpc-chart>

#### Crash Types

**Table 2** and **Figure 11** show the count of crashes at the intersection of Lincoln Avenue and Ohio Street from 2015 to 2019, sorted by year and crash type. **Table 3** and **Figure 12** sort each crash type by severity level. Rear-end crashes were the predominant crash type at this intersection, accounting for 75% of all crashes. Some pedestrian and pedalcyclist crashes also occurred. A detailed analysis of the rear-end crashes and pedestrian or pedalcyclist crashes can be found below.  There was also one angle crash, which was caused by improper backing.  

<rpc-table url="crash_type_year.csv"
  table-title="Table 2 - Number of Crashes by Crash Type and Year "
  text-alignment="c,c"></rpc-table>

<rpc-chart url="crash_type_year_chart.csv"
      chart-title="Figure 11 - Number of Crashes by Crash Type and Year"
      x-label="Crash Type"
      y-label="Crash Counts"
      type="bar"
      stacked="true"></rpc-chart>

<rpc-table url="crash_severity_type.csv"
  table-title="Table 3 - Number of Crashes by Crash Type and Severity Level "
  text-alignment="c,c"></rpc-table>

<rpc-chart url="crash_severity_type_chart.csv"
      chart-title="Figure 12 - Number of Crashes by Crash Type and Severity Level"
      x-label="Severity Level"
      y-label="Crash Counts"
      type="bar"
      stacked="true"></rpc-chart>

#### Rear end crashes
From 2015 to 2019, 21 rear-end crashes occurred at the study intersection. All of these crashes can be attributed to vehicles following too closely or failing to reduce speed. Thirteen crashes occurred when the leading/front vehicle stopped to wait for pedestrians or bicyclists to cross the road (12 of these occurred at the south leg, one occurred at the north leg of the intersection). Seven crashes occurred when the leading/front vehicle stopped or slowed in the traffic flow (four of these at the north leg, three at the south leg). Six of these traffic flow crashes occurred during or near the PM peak hour. One rear-end crash occurred when the following vehicle veered into the turning lane. 

#### Pedestrian and pedalcyclist crashes
From 2015 to 2019, six crashes associated with pedestrians or pedalcyclist occurred at the study intersection. One occurred on the north leg when the vehicle was going in the southbound direction, two happened on the south leg while the vehicles were moving in the northbound direction, and three occurred on the south leg when the vehicles were driving on the southbound direction. All of these crashes can be attributed to vehicles failing to yield to a pedestrian or bicyclist. 

### Field Investigation
During the field investigation, staff observed some pedestrians and bicyclists crossing Lincoln Avenue in the middle of the block, rather than using the crosswalk at Ohio Street. This behavior increases the chance of pedestrian and pedalcyclist crashes, and requires vehicles to slow down or stop frequently which increases the risks of rear-end crashes. Additionally, due to the high volume of vehicular traffic, some pedestrians and bicyclists waited for an extended period to have a secured gap to cross Lincoln Avenue safely. This situation may cause pedestrians and bicyclists to lose patience and enter the intersection before it is safe to do so.

## Findings and Proposed Countermeasures

Crashes at the other three stop-controlled intersections on Lincoln Avenue between Iowa Street and Michigan Avenue were also reviewed in this study, as the entire section of Lincoln Avenue near campus has been problematic for an extended period. Rear-end crashes were prevalent at all the intersections along the Lincoln Avenue corridor between Ohio Street and Michigan Avenue. However, the intersection of Lincoln Avenue and Ohio Street had a higher number of pedestrian and pedalcyclist crashes than the other intersections from 2015-2019. 

To improve the safety of all road users, it is important to remind drivers to drive at an appropriate speed, to improve the visibility of pedestrians and bicyclists to drivers, and to regulate the activities of pedestrians and bicyclists to protect them from vehicles. 

**The proposed countermeasures include:**

1.	Installing pedestrian refuge medians and curb bump-outs on Lincoln Avenue. This will improve the visibility of pedestrians and bicyclists, reduce the time that they are in the street, and encourage slower vehicle speeds. 
2.	Installing barriers or fences along the sidewalk to encourage the use of crosswalks. 
3.	Conducting a detailed Lincoln Avenue corridor study. The City of Urbana and CUUATS submitted a grant application to IDOT in March 2022, requesting funding to conduct a detailed corridor study of Lincoln Avenue between Green Street and Florida Avenue. The goal of the corridor study is to increase safety and mobility in this segment of Lincoln Avenue. 
