---
title: "Prospect Avenue and Devonshire Drive"
draft: false
weight: 50
bannerHeading: Prospect Avenue and Devonshire Drive.
bannerText: >
  The intersection of Prospect Avenue and Devonshire Drive.
bannerAction: Provide Feedback
bannerUrl: /contact
---

## Introduction

This study analyzes the safety conditions at the intersection of Prospect Avenue and Devonshire Drive, an unsignalized intersection in Champaign. The intersection was selected as one of the top high-priority urban unsignalized intersections in the analysis for the Champaign County Safety Plan Implementation. **Map 1** shows the location of this intersection. Prospect Avenue is a minor north-south arterial, while Devonshire Drive serves as a major east-west collector road.

<iframe src="https://maps.ccrpc.org/ccspi-urban-intx-prospect-devonshire/" width="100%"  height="600" allowfullscreen="true"></iframe>

## Existing Conditions Analysis

The existing condition analysis for the intersection of Prospect Avenue and Devonshire Drive includes an evaluation of the intersection's physical and geometrical conditions, as well as the traffic operational conditions of its roadway approaches. 

### Geometry and physical conditions

The study intersection is an all-way stop-controlled intersection. **Figure 1.1-1.2** show the configuration of the Prospect Avenue and Devonshire Drive intersection.
Prospect Avenue is a four-lane road with two lanes in each direction. This roadway's annual average daily traffic (AADT) is between 8,350 and 9,800 vehicles per day (i), according to Illinois Department of Transportation (IDOT) data from 2021. Each lane is approximately 12 feet wide along this roadway. There are lane markings on Prospect Avenue, and no parking is permitted. There is an approximately four-foot-wide marked median present along Prospect Avenue. The speed limit for Prospect Avenue is 35 miles per hour (mph). Street lights are present along this roadway. 

{{<image src="figure1-1.png"
    caption="Figure 1-1 - Prospect Avenue and Devonshire Drive Intersection Configuration">}}
{{<image src="figure1-2.png"
    caption="Figure 1-2 - Prospect Avenue and Devonshire Drive Intersection Configuration">}}

Devonshire Drive has one lane for each direction, but it also has an exclusive right-turn lane on the east leg for westbound traffic. Devonshire Drive has a speed limit of 30 mph on the west side of the intersection and 35 mph on the east side of the intersection. No centerline or median is present on the west side of this roadway. The AADT of this roadway is between 2,300 and 4,700 vehicles per day (i), according to 2021 IDOT data. 

There are four Champaign-Urbana Mass Transit District (CUMTD) bus stops within 150 feet of this intersection. One marked pedestrian crossing is located on the east side of this intersection, and there are no other marked pedestrian crossings on the other three legs. The stop lines at this intersection are faded and can not be appropriately seen from a distance. Stop ahead signs are present on Prospect Avenue on both approaches. No street lights are present along Devonshire 


Pedestrian and bicyclist volumes are relatively low at this intersection. The surrounding land uses are primarily residential. However, Good Shepherd Lutheran Church is located on the southwest corner of the study intersection.

### Vehicle Traffic Operational Conditions

#### Peak Hour Vehicular Traffic

On Thursday, September 23rd, 2021, twelve hours of vehicular, pedestrian, and bicycle movement data for this intersection were collected from 7 AM to 7 PM. Passenger vehicles (also referred to as light vehicles) made up a substantial portion of traffic, at 97.5%. **Figure 2** shows the vehicle traffic volume at each approach, and **Figure 3** shows the peak hour turning movement counts for each direction. The 12-hour traffic count data shows that the north and south legs (Prospect Avenue) have higher traffic volumes than the east and west legs (Devonshire Drive). The westbound leg of Devonshire Drive sees significantly more traffic than the eastbound leg. The AM peak hour is 7:30 AM-8:30 AM, the midday peak hour is 11:45 AM – 12:45 PM, and the PM peak hour is 4:45 PM – 5:45 PM. Data also provided evidence of speeding in the southbound traffic along Prospect Avenue.

<rpc-chart url="15 minute volume.csv"
      chart-title="Figure 2 - Vehicle Traffic Volume at Fifth Street and Green Street"
      x-label="Time"
      y-label="Traffic Counts"
      type="line"></rpc-chart> 

{{<image src="figure3.png"
    caption="Figure 3 - Turning Movement Counts">}}

#### Vehicle Level-Of-Service

Level-of-service (LOS) is a qualitative measure describing operational conditions from "A" (best) to "F" (worst) within a traffic stream or at an intersection. This measurement is quantified for signalized and unsignalized intersections in terms of vehicle control delay.

Control delay is a component of delay that results from the type of traffic control at the intersection or approach, measured by comparison with the uncontrolled condition. It is the difference between the travel time that would have occurred in the absence of the intersection control and the travel time that results from the intersection control's presence.


**Table 1** shows the LOS criteria for all-way stop-controlled intersections or unsignalized intersections as per the Highway Capacity Manual 6th Edition

<rpc-table url="LOScriteria.csv"
  table-title="Table 1 - LOS Criteria for unsignalized intersections"
  text-alignment="c,c"></rpc-table>

The LOS and control delays for the study intersection during the morning (AM), midday, and afternoon (PM) peak hours are shown in **Table 2**. The overall intersection operational condition is an LOS of D for morning and afternoon peak-hour traffic. However, the northbound right and southbound left approaches show significant delays and an LOS of E during the morning peak-hour traffic, while for afternoon peak-hour traffic, the southbound left and through approaches show an LOS of E.

<rpc-table url="los.csv"
  table-title="Table 2 - Approach Level-of-Service and Control Delay at Study Intersection"
  text-alignment="c,c"></rpc-table>

## Traffic Safety Analysis

### Crash Data Analysis 

Crash data from 2015 to 2019—obtained from the IDOT Division of Traffic Safety [^1] —was analyzed for the intersection of Prospect Avenue and Devonshire Drive. There were 17 crashes at this study location over these five years. **Figure 4** shows the collision diagram of the intersection. 

{{<image src="crash diagram.png"
    caption="Figure 4 - Collision Diagram"
    alt="Plan view diagram of intersection, showing the type and location for all crashes from 2015 to 2019">}}

#### Crash Trends and Time

**Figures 5.1-5.4** show the timing of crashes from 2015 to 2019 at the intersection of Prospect Avenue and Devonshire Drive, sorted by year, month, day, and hour. The lowest number of crashes occurred in 2016, with only two crashes. July had a relatively high number of crashes, with a total of four crashes. Friday was the day of the week that had the highest number of crashes, with five crashes. The time period of the highest crash frequency was from 8 AM to 9 AM, with a total of three crashes.

<rpc-chart url="crash_count_year.csv"
      chart-title="Figure 5.1 - Number of Crashes by Year"
      x-label="Year"
      y-label="Crash Counts"
      type="bar"></rpc-chart>

<rpc-chart url="crash_count_month.csv"
      chart-title="Figure 5.2 - Number of Crashes by Month"
      x-label="Month"
      y-label="Crash Counts"
      type="bar"></rpc-chart>

<rpc-chart url="crash_count_day.csv"
      chart-title="Figure 5.3 - Number of Crashes by Day of the Week"
      x-label="Day"
      y-label="Crash Counts"
      type="bar"></rpc-chart>

<rpc-chart url="crash_count_hour.csv"
      chart-title="Figure 5.4 - Number of Crashes by Hour of the Day"
      x-label="Hour"
      y-label="Crash Counts"
      type="bar"></rpc-chart>

#### Crash Severity

Crash severity levels are generally classified in order of decreasing severity as Fatal (K), A-Injury (A), B-Injury (B), C-Injury (C), and No Injury (O). The severity of a crash is determined by the most severe injury of a person in that crash. The definitions of these KABCO severity levels can be found in **Appendix A**. **Table 3** and **Figure 6** present the number of crashes by severity at the study location. There were no fatal crashes from 2015 to 2019 at the intersection of Prospect Avenue and Devonshire Drive. Most crashes were No Injury crashes. One A-Injury crash, two B-Injury crashes, and two C-Injury crashes occurred in the five-year study period.

<rpc-table url="crash_severity_year.csv"
  table-title="Table 3 - Number of Crashes by Severity Level and Year"
  text-alignment="c,c"></rpc-table>

<rpc-chart url="crash_severity_year_chart.csv"
      chart-title="Figure 6 - Number of Crashes by Severity Level and Year"
      x-label="Severity Level"
      y-label="Crash Counts"
      type="bar"
      stacked="true"></rpc-chart>

#### Crash Types

**Table 3** and **Figure 7** show types of crashes by year at the intersection of Prospect Avenue and Devonshire Drive. Angle crashes were the predominant type at this intersection, accounting for 70.5% of all crashes. Rear-end crashes were also prevalent at this intersection, accounting for 23.5% of all crashes. A detailed analysis of the angle and rear-end crashes can be found in the following sections. Also, one pedalcyclist crash occurred at this intersection. 

<rpc-table url="crash_type_year.csv"
  table-title="Table 3 - Number of Crashes by Crash Type and Year "
  text-alignment="c,c"></rpc-table>

<rpc-chart url="crash_type_year_chart.csv"
      chart-title="Figure 7 - Number of Crashes by Crash Type and Year"
      x-label="Crash Type"
      y-label="Crash Counts"
      type="bar"
      stacked="true"></rpc-chart>

**Table 4** and **Figure 8** quantify each type of crash by severity level. Most angle and all rear-end crashes led to no injury. The pedalcyclist crash led to one A-Injury. 

<rpc-table url="crash_severity_type.csv"
  table-title="Table 4 - Number of Crashes by Crash Type and Severity Level "
  text-alignment="c,c"></rpc-table>

<rpc-chart url="crash_severity_type_chart.csv"
      chart-title="Figure 8 - Number of Crashes by Crash Type and Severity Level"
      x-label="Severity Level"
      y-label="Crash Counts"
      type="bar"
      stacked="true"></rpc-chart>

##### Angle crashes

From 2015 to 2019, 12 angle crashes occurred at the intersection of Prospect Avenue and Devonshire Drive. Angle crashes were the predominant type of crashes and primarily involved southbound and westbound vehicles. In ten crashes, the driver at fault was driving on Prospect Avenue. All of these crashes can be attributed to disregarding stop signs or failing to yield the right of way. Crash data showed that seven crashes occurred when a vehicle disregarded a stop sign, four crashes occurred when a driver failed to yield the right of way, and the remaining crash occurred as the driver was unable to determine to take appropriate action. 

##### Rear-End crashes

From 2015 to 2019, four rear-end crashes occurred at the study intersection. All of the rear-end crashes occurred on Prospect Avenue. Three occurred at the north leg; one occurred at the south leg. Three of these crashes can be attributed to vehicles failing to reduce speed to avoid crashing, which also indicates a speeding issue on Prospect Avenue, especially for the southbound traffic.

## Traffic Signal Warrant Study

A warrant for any traffic control device (sign, signal, or pavement marking) is the minimum criteria that must be met before such a device can be installed. If one or more Manual of Uniform Traffic Control Devices (MUTCD) traffic signal warrants are met, a traffic signal can be considered for installation. Meeting a warrant does not mean a traffic control device must be installed; engineering judgment should be used to determine whether installing a traffic signal would improve the intersection's overall safety and operational efficiency.

A traffic signal warrant analysis was completed for the Prospect Avenue and Devonshire Drive intersection. The signal warrant study was performed by analyzing the relevant factors described in the nine traffic signal warrants specified in the MUTCD-2009 Edition 
[^2]. 

The traffic signal warrants include:

- Warrant 1, Eight-Hour Vehicular Volume
- Warrant 2, Four-Hour Vehicular Volume 
- Warrant 3, Peak Hour 
- Warrant 4, Pedestrian Volume
- Warrant 5, School Crossing 
- Warrant 6, Coordinated Signal System 
- Warrant 7, Crash Experience 
- Warrant 8, Roadway Network 
- Warrant 9, Intersection Near a Grade Crossing

In the analysis, Prospect Avenue was considered the major street, and Devonshire Drive was considered the minor street. The posted speed limit on Prospect Avenue was 35 mph, and 12-hour traffic data was used to complete the analysis. Highway Capacity Software (HCS)[^3] was used for traffic signal warrant analysis for 2021 data. The signal warrant analysis showed that Warrant 1: Eight-Hour Vehicular Volume and Warrant 2: Four-Hour Vehicular Volume were satisfied for the study intersection. **Appendix C** contains detailed calculations for checking the applicability of the traffic signal warrants. 

## Findings and Proposed Countermeasures

### Study Findings 

After analyzing the crash data and site investigation at the intersection of Prospect Avenue and Devonshire Drive, several risk factors were identified. The findings are listed below:

- There is no centerline on the west side of the intersection on Devonshire Drive.
- There is no streetlights on Devonshire Drive.
- Only one marked crosswalk is present on the east leg of the intersection.
- Angle crashes are the predominant type of crash.
- Speeding is an issue along South Prospect Avenue. 
- Northbound right and southbound left approaches show significant delays, with LOS E for the morning peak hour traffic, while for afternoon peak hour traffic, southbound left and through approaches have LOS E.
- Two MUTCD traffic signal warrants—Warrant 1: Eight-Hour Vehicular Volume and Warrant 2: Four-Hour Vehicular Volume—were satisfied for this intersection.

### Proposed Countermeasures

After analyzing the operational and safety issues at the intersection of Prospect Avenue and Devonshire Drive, the following short-term and long-term recommendations are proposed:

#### (1) Short-Term Recommendations

##### Roadway Markings and Signs

As identified in the existing condition analysis section, stop markings have faded around this intersection, and there are no centerline markings on the west leg of Devonshire Drive. According to the Manual on Uniform Traffic Control Devices (MUTCD) guidelines, centerline markings should be placed on paved arterials and collectors that have a traveled way 20 feet wide or more and an ADT of 4,000 vehicles per day or greater [^4]. Devonshire Drive serves as a major collector, has a traveled section larger than 20 feet, and has an ADT higher than 4,000 vehicles per day on the east side of the intersection. Devonshire Drive has a marked center line on the east side of the intersection. It is recommended to install a centerline on the west side of the intersection to make the road marking more consistent.  
      
There are no marked pedestrian crossings on three legs of this intersection (with the east leg as the exception). Marked pedestrian crossings along all legs are recommended to make this intersection safer for pedestrians. 
      
Installation of LED stop signs is recommended at this intersection to increase the visibility of stop signs to drivers. This countermeasure has a crash modification factor (CMF) of 0.59 for angle crashes [^5], which means we should expect 59% of the current crash frequency after the implementation of this proposed countermeasure. 
      
Installation of "Stop Sign Ahead" signs is also proposed on the east and west legs of the intersection, and "STOP" pavement markings should be installed on every approach to show where drivers are required to stop when signaled to do so. 

Additionally, Dynamic Speed Display Signs (DSDS) can be periodically placed on the Prospect Avenue approaches near the intersection. Studies show that DSDS is effective in reducing vehicular speed [^6]. Law enforcement should also increase their speed enforcement activities on Prospect Avenue, as the effectiveness of DSDS depends on the perceived level of enforcement among drivers.

##### Street Lighting

Currently, there are no street lights present on Devonshire Drive. Four out of seventeen crashes (24%) occurred at night, and three of them involved vehicles driving on Devonshire Drive. Installing street lights along Devonshire Drive in the vicinity of the intersection is therefore recommended. This would improve drivers’ safety at night. This countermeasure has a CMF of 0.68 for all crashes in urban and suburban areas [^7].

#### (2) Long Term Recommendations

##### Intersection Control
      
Seven crashes at the study intersection occurred due to drivers disregarding stop signs. This is a typical safety issue at unsignalized intersections. In addition, some approaches are operating under congested conditions during morning and afternoon peak hours. The possibility of implementing a roundabout or signalizing the intersection should therefore be evaluated as long-term recommendations. 

For this purpose, staff calculated future intersection level of service (LOS) and traffic delays for different intersection control types (unsignalized, roundabout, and signalized intersection) based on the future traffic volumes forecasted for 2045. These analyses were done using the Synchro 10 software suite. The future intersection's turning movement volumes were projected by utilizing CUUATS Travel Demand Model's (TDM) forecasted growth rates for the area. Using the TDM 2015 traffic volume as the baseline, the TDM Long Range Transportation Plan (LRTP) 2045 preferred scenario was used to obtain the projected future traffic volumes for 2045. Based on the 2015 and 2045 traffic volumes, the compound annual growth rate (CAGR) was estimated for each approach, and turning movement counts for each approach were calculated using the CAGR for 2045. Implementing an exclusive left-turn lane for Prospect Avenue was also considered as part of the signalized intersection analysis, and the left-turn lane warrant met the requirement for southbound traffic according to the IDOT BDE manual [^8]. **Table 5** shows the comparison of the three different control types for this intersection.

<rpc-table url="control_types.csv"
            table-title="Table 5 - Comparison with three different intersection's control types"
            text-alignment="c,c"></rpc-table>

**Table 5** indicates that either a roundabout or a traffic signal would work properly in 2045, and all-way stop control will fail with LOS F. Considering that Prospect Avenue currently has four lanes, it is advisable to consider a multilane roundabout at this location, which will represent a significant challenge for right-of-way acquisition, considering the space limitations at this intersection. However, if a road diet can be considered along Prospect Avenue (as discussed in the next section), it is worth checking the possibility of a single-lane roundabout at the Prospect Avenue and Devonshire Drive intersection. As shown above, the traffic signal warrant analysis satisfied two warrants for 2021; therefore, implementing a traffic signal at Prospect Avenue and Devonshire Drive intersection, with exclusive left turns on the Prospect Avenue approaches, is recommended in the long term. However, installing a traffic signal would increase the possibility of rear-end crashes, a risk that will be discussed in the next section.

##### Traffic Calming
        
Crash reports from the study intersection indicated that some crashes occurred due to speeding. Three of the four rear-end crashes occurred due to failure to reduce speed to avoid crashes, which suggests some drivers are speeding near this intersection, mainly along Prospect Avenue. The AADT along Prospect Avenue between Windsor Road and Springfield Avenue varies between 9,200 and 15,200 vehicles per day. According to the Federal Highway Administration (FHWA), a roadway with an ADT between 10,000 and 15,000 is a good candidate for a road diet in many instances [^9]. To evaluate the possibility of implementing a road diet to reduce speeding along this roadway segment, the City of Champaign may conduct intersection analyses and consider signal retiming at other intersection locations. This would also help to facilitate the installation of bicycle lanes along Prospect Avenue and provide the opportunity to evaluate a single-lane roundabout implementation at this intersection. This countermeasure has a crash modification factor (CMF) of 0.53 for all crashes [^10].

## References

[^1]:	Illinois Department of Transportation (https://www.gettingaroundillinois.com/Traffic%20Counts/index.html)
[^2]:	Manual of Uniform Traffic Control Devices (MUTCD), United States Department of Transportation - Federal Highway Administration (https://mutcd.fhwa.dot.gov/htm/2009/part4/part4c.htm)
[^3]:	HCS software developed by McTrans Center (https://mctrans.ce.ufl.edu/hcs/)
[^4]:	Manual on Uniform Traffic Control Devices (https://mutcd.fhwa.dot.gov/htm/2009/part3/part3b.htm)
[^5]:	CMF Clearinghouse (https://www.cmfclearinghouse.org/detail.cfm?facid=6602)
[^6]:	Texas Transportation Institute – Effectiveness of Dynamic Speed Display Signs (DSDS) in Permanent Application, Project Summary Report O-4475-S, Texas Transportation Institute, TTI, College Station, TX 2004.
[^7]:	CMF Clearinghouse (https://www.cmfclearinghouse.org/detail.cfm?facid=11026#commentanchor)
[^8]:	Bureau of Design and Environment Manual by Illinois Department of Transportation
(https://idot.illinois.gov/Assets/uploads/files/Doing-Business/Manuals-Guides-&-Handbooks/Highways/Design-and-Environment/Design%20and%20Environment%20Manual,%20Bureau%20of.pdf)
[^9]:	Federal Highway Administration (https://safety.fhwa.dot.gov/road_diets/resources/pdf/fhwasa17021.pdf)
[^10]: CMF Clearinghouse (https://www.cmfclearinghouse.org/detail.cfm?facid=2841)



