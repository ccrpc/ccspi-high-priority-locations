---
title: "Prospect Avenue and John Street"
draft: false
weight: 10
bannerHeading: Prospect Avenue and John Street.
bannerText: >
  The intersection of Prospect Avenue and John Street.
bannerAction: Provide Feedback
bannerUrl: /contact
---

## Introduction

This study was performed to analyze the safety of the intersection of Prospect Avenue and John Street.  The intersection of Prospect Avenue and John Street is an unsignalized intersection located in the City of Champaign. **Map 1** shows the location of the intersection. Prospect Avenue is a minor arterial road, and John Street is a local road in the city of Champaign. The land uses on the area surrounding this intersection are primarily residential, with a neighborhood bakery on a residential block to the east, and small commercial areas several blocks away to the north and south. An elementary school is also located one block to the southeast of the intersection.

<iframe src="https://maps.ccrpc.org/ccspi-urban-intx-prospect-john/" width="100%"  height="600" allowfullscreen="true"></iframe>

## Existing Conditions Analysis

The existing conditions analysis for this intersection consists of examining the geometry and physical conditions of the intersection, as well as evaluating traffic operational conditions. 

### Geometry and physical conditions

Prospect Avenue and John Street is a two-way stop-controlled intersection with stop signs placed on John Street. The major road, Prospect Avenue, is a minor arterial with four lanes, two per direction. The annual average daily traffic (AADT) of Prospect Avenue is approximately 14,400 vehicles. The speed limits of the north leg and south leg of the study intersection are 20 miles per hour (mph). 

Considering there is an elementary school located a block southeast of the study intersection and traffic volumes are high and drivers were speeding along Prospect Avenue, there are multiple signs placed along Prospect Avenue to remind drivers to reduce speed. In the southbound direction, a “20 MPH SCHOOL ZONE AHEAD” (S4-5A)” sign is placed at the intersection of Green Street and Prospect Avenue (one block north of the study intersection) along Prospect Avenue to alert southbound vehicles that they are entering a school zone and should reduce speed. In addition, a School Zone Sign (S1-1) accompanied by a “CELL PHONE USE PROHIBITED” sign is placed between Green Street and John Street. Lastly, a School Speed Limit Assembly of 20 mph (S4-3P, R2-1, and S4-2P) is placed at the northwest corner of the study intersection to remind drivers approaching the intersection from the north to lower their speed. In the northbound direction, there is School Crossing Assembly (S1-1 and W16-7P) located at the northeast corner of the intersection of West Daniel Street and Prospect Avenue (one block south of John Street). 

One significant geographic feature of the area is Prospect Avenue’s downward slope toward the study intersection from both the north and south directions, which may cause drivers approaching the intersection on Prospect Avenue to gain some speed. **Figures 1.1** and **Figure 1.2** show these downward slopes.

{{<image src="figure1.PNG">}}

The minor road at this intersection—John Street—is a two-lane local road with one lane in each direction. The speed limit for John Street is 30 mph, and the AADT is approximately 2,050 vehicles. Both the east leg and the west leg of the intersection feature sharrows and signage indicating drivers that they should share the road with cyclists. There are crosswalks across John Street on both legs. To the west of the intersection, there is no speed limit sign installed along John Street within 3 blocks. 

### Vehicle Traffic Operational Conditions

Traffic count data for the Prospect Avenue and John Street intersection was collected from 7AM to 7PM on September 15, 2021. Passenger vehicles (also referred to as light vehicles), trucks, buses, and on-road bicyclists are included in the vehicle traffic analysis.  

The vast majority of vehicles traveling through the study intersection were light vehicles, accounting for 97.6% of the total volume. As seen in the 15-minute traffic volume per approach graph in **Figure 2**, Prospect Avenue (northbound and southbound approaches) had a much higher traffic volume than John Street (eastbound and westbound approaches). **Figure 3** shows turning movement counts over the entire 12-hour period, as well as the morning peak hour (7:30 AM -8:30 AM), the midday peak hour (12 PM – 1 PM), and the afternoon peak hour (2:45 PM – 3:45 PM). On Prospect Avenue, significantly more vehicles traveled straight through the intersection than turned. On John Street, many more vehicles traveled through or turned right than turned left. The level of service (LOS) and control delay for each approach at the study intersection can be found in **Table 1**, which shows that all of the approaches are under acceptable conditions (in urban areas, an LOS of D or better). For more information on these measurements, see the appendix.


<rpc-chart url="15 minute volume.csv"
      chart-title="Figure 2 - Vehicle Traffic Volume at Fifth Street and Green Street"
      x-label="Time"
      y-label="Traffic Counts"
      type="line"></rpc-chart> 

{{<image src="figure3.png"
    caption="Figure 3 - Turning Movement Counts">}}

<rpc-table url="los.csv"
  table-title="Table 1 - Approach Level-of-Service and Control Delay at Study Intersection"
  text-alignment="c,c"></rpc-table>

## Traffic Safety Analysis

### Crash Data Analysis 

Crash data from 2015 to 2019 were analyzed for the intersection of Prospect Avenue and John Street. These data were obtained from the Illinois Department of Transportation Division of Traffic Safety. Thirty-four crashes occurred at this intersection over the 5-year analysis period. **Figure 4** shows the collision diagram for Prospect Avenue and John Street, with the location and type of all crashes over this period. 

{{<image src="figure4.png"
    caption="Figure 4 - Collision Diagram"
    alt="Plan view diagram of intersection, showing the type and location for all crashes from 2015 to 2019">}}

#### Crash Trends and Time

**Figures 5.1-5.4** demonstrate the timing of the crashes at Prospect Avenue and John Street in the study period. As seen from these figures, May was the month with the highest number of crashes in these years, Tuesday and Friday were the days of the week with the highest number of crashes, and the time period of highest crash frequency was between 5 PM and 6 PM. 

<rpc-chart url="crash_count_year.csv"
      chart-title="Figure 5.1 - Number of Crashes by Year"
      x-label="Year"
      y-label="Crash Counts"
      type="bar"></rpc-chart>

<rpc-chart url="crash_count_month.csv"
      chart-title="Figure 5.2 - Number of Crashes by Month"
      x-label="Month"
      y-label="Crash Counts"
      type="bar"></rpc-chart>

<rpc-chart url="crash_count_day.csv"
      chart-title="Figure 5.3 - Number of Crashes by Day of the Week"
      x-label="Day"
      y-label="Crash Counts"
      type="bar"></rpc-chart>

<rpc-chart url="crash_count_hour.csv"
      chart-title="Figure 5.4 - Number of Crashes by Hour of the Day"
      x-label="Hour"
      y-label="Crash Counts"
      type="bar"></rpc-chart>

#### Crash Severity

Crash severity levels are generally classified, in order of declining severity, as: Fatal ( K ), A-Injury ( A ), B-Injury ( B ), C-Injury ( C ), and No Injury ( O ). Definitions of these severity levels can be found in the Appendix. **Table 2** and **Figure 6** present the number of crashes by severity level at the study location. There were no fatal crashes from 2015 to 2019 at the intersection of Prospect Avenue and John Street. Most crashes resulted in no injuries. There was one A-Injury crash, six B-Injury crashes and three C-Injury crashes. 

<rpc-table url="crash_severity_year.csv"
  table-title="Table 2 - Number of Crashes by Severity Level and Year"
  text-alignment="c,c"></rpc-table>

<rpc-chart url="crash_severity_year_chart.csv"
      chart-title="Figure 6 - Number of Crashes by Severity Level and Year"
      x-label="Severity Level"
      y-label="Crash Counts"
      type="bar"
      stacked="true"></rpc-chart>

#### Crash Types

**Table 3** and **Figure 7** shows the types of crashes by year at the Prospect Avenue and John Street intersection. As can be seen in the figure, the majority of crashes at the intersection were angle crashes, accounting for 62% of all crashes. Rear-end crashes and turning crashes were also prevalent at the study intersection, accounting for 26% and 12% of crashes in the study period, respectively. A detailed analysis of each type of crash can be found in the following sections.

**Table 4** and **Figure 8**  summarize the number of each type of crash by severity level. The majority of angle crashes led to no injuries; however, there were one led to A-Injury, four led to B-Injuries, and three led to C-Injuries in this period. Most rear-end crashes also led to no injuries, but there was one B-Injury rear end crash. One turning crash led to a B-Injury, while the remaining four resulted in no injury. 

<rpc-table url="crash_type_year.csv"
  table-title="Table 3 - Number of Crashes by Crash Type and Year "
  text-alignment="c,c"></rpc-table>

<rpc-chart url="crash_type_year_chart.csv"
      chart-title="Figure 7 - Number of Crashes by Crash Type and Year"
      x-label="Crash Type"
      y-label="Crash Counts"
      type="bar"
      stacked="true"></rpc-chart>

<rpc-table url="crash_severity_type.csv"
  table-title="Table 4 - Number of Crashes by Crash Type and Severity Level "
  text-alignment="c,c"></rpc-table>

<rpc-chart url="crash_severity_type_chart.csv"
      chart-title="Figure 8 - Number of Crashes by Crash Type and Severity Level"
      x-label="Severity Level"
      y-label="Crash Counts"
      type="bar"
      stacked="true"></rpc-chart>

##### Angle crashes

From 2015 to 2019, 21 angle crashes occurred at the intersection of Prospect Avenue and John Street. While some crashes involved multiple causes, the main angle crash causes can be summarized as follows: 

- (1)	Driver ran a stop sign. 
      Five crashes were caused by drivers running a stop sign. This situation occurred on both legs of John Street. 

- (2)	Failed to yield the right-of-way. 
      Most angle crashes were caused by drivers on John Street failing to yield right of way. This occurred relatively equally for both directions of travel on John Street. The crashes caused by failing to yield right-of-way can be further categorized into two primary scenarios:

  - a.	Driver misjudged gap. Drivers stopped at the stop sign but failed to estimate the distance of oncoming vehicles or the time needed to safely navigate the intersection. Drivers on John Street misjudging gaps was the prevailing reason for failure to yield the right of way. 
  
  - b.	Driver stopped at the stop sign but were motioned through the intersection by a driver in another vehicle. In three angle crashes, the vehicle heading west on John Street stopped at the stop sign, but they were then waved to proceed by a driver on Prospect Avenue that was stopped in traffic. All three of these crashes occurred during peak hours. One occurred during the AM peak hour when a vehicle traveling west on John Street collided with a vehicle traveling south on Prospect Avenue. The other two crashes occurred during the PM peak hour when the vehicles traveling west on John Street collided with vehicles traveling north on Prospect Avenue.

##### Rear-End crashes

From 2015 to 2019, nine rear-end crashes occurred at intersection of Prospect Avenue and John Street. While some crashes were caused by multiple factors or involved more than two vehicles, the main rear-end crash causes can be summarized as follows:   

- (1)	Followed too closely and failed to reduce speed. 

    Most rear-end crashes were primarily caused by vehicles following too closely and failing to reduce speed. One crash occurred while the driver was making a lane change. The others occurred when the front vehicle stopped suddenly in the flow of traffic and the following vehicle failed to reduce their speed to avoid a crash. 

- (2)	Distracted driving. 

    In some cases, the following driver was looking at their phone or distracted by other objects in the vehicle when their vehicle collided with the front vehicle. 

##### Turning crashes

From 2015 to 2019, four turning crashes occurred at the study intersection. While all these crashes can be attributed to failing to yield right-of-way, they can be further categorized into the following scenarios: 

- (1)	Failed to estimate the distance of oncoming vehicles/time needed to safely navigate the intersection. 

    In some crashes, the driver had waited in line for an extended period before proceeding, with the resulting lack of patience causing them to enter the intersection before it was safe to do so.

- (2)	Driver’s view was obstructed by other vehicles in traffic.  

- (3)	The roadway was clear when the vehicle made the left turn. 

    Similar to the first cause, the driver on John Street observed the roadway to be clear when they entered the intersection, but an oncoming vehicle quickly appeared. Speeding on Prospect Avenue is a possible cause of this kind of crashes. 

## Findings and Proposed Countermeasures

The intersection adjacent to Prospect Avenue and John Street to the north (the intersection of Prospect Avenue and Green Street) is a signal-controlled intersection and is only 400 feet away from the study intersection. Because of this, enhancing the study intersection to a signal-controlled intersection is not appropriate.  Furthermore, because the three consecutive intersections to the south are all two-way stop-controlled intersections, it is not appropriate to convert the study intersection into an all-way stop-controlled intersection. Therefore, controlling vehicle speed on Prospect Avenue and enhancing stop warnings on John Street are the key solutions to the safety problems at this intersection. 

### (1) Monitor speed on Prospect Avenue

At the study intersection, several crashes occurred when the driver on John Street had already stopped and checked that the road was clear before entering the intersection. In some crash reports, the driver on John Street also stated their belief that the driver on Prospect Avenue was speeding. Installing dynamic speed measurement signs to remind drivers not to drive over the speed limit on Prospect Avenue is recommended. This is useful not only for the safety of the study intersection, but also for the safety of the school zone along Prospect Avenue. 

### (2) Enhance stop warnings on John Street

To remind drivers on John Street to stop completely, look carefully, and proceed only when it is safe, adding a stop sign companion “CROSS TRAFFIC DOES NOT STOP” and installing “STOP” pavement markings are recommended. 

