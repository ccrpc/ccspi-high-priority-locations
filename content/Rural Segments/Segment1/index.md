---
title: "Homer Lake Road"
draft: false
weight: 20
bannerHeading: Homer Lake Road.
bannerText: >
  Homer Lake Road (County Road 1950 E – South Homer Lake Road)
bannerAction: Provide Feedback
bannerUrl: /contact
---
## Introduction

### Background
This study was performed to analyze transportation safety along **Homer Lake Road (from County Road 1950 E to South Homer Lake Road)**. The following map shows the location of the study segment. 

<iframe src="https://maps.ccrpc.org/ccspi-rural-seg-homerlake/" width="100%"  height="600" allowfullscreen="true"></iframe>

There are 11 intersections along this segment. **Table 1** shows the control type of each intersection.

<rpc-table url="IntxControlType.csv" table-title="Table 1 - Intersection Control Type"> </rpc-table>

### Traffic Crash Overview
From 2014 to 2018, there were 24 reported crashes along Homer Lake Road (County Road 1950 E – South Homer Lake Road). 10 crashes occurred along the sub-segments between intersections, and 14 crashes occurred at intersections. Among the 14 intersection crashes, 11 occurred at [the intersection of County Road 2200 E and Homer Lake Road](http://localhost:1313/ccspi-high-priority-locations/rural-intersections/intersection/), the other 3 occurred at different intersections. Because crashes at the intersection of County Road 2200 E and Homer Lake Road have been analyzed in the Rural Intersections section (see link above), they are not included here. 

Aside from the crashes at the intersection of County Road 2200 E and Homer Lake Road, no special crash pattern was discovered—the other 13 crashes occurred at a variety of locations along the segment and were caused by a variety of factors. Due to the long distance and varying land uses, geometries, and physical conditions along this stretch of Homer Lake Road, it was determined that the best approach to studying the safety of the corridor was to analyze it **section-by-section**.  Therefore, for this study, Homer Lake Road (County Road 1950 E – South Homer Lake Road) was divided into **five sections**.

**Table 2**  lists the scope of the five sections and the severity level of the crashes that occurred within each section.  Definitions for the severity levels can be found in the Appendix. 

{{<accordion>}}
  {{<accordion-content title="Table 2 - Crash Location and Severity">}}
    <rpc-table url="crash_location_severity.csv" table-title="Table 2 - Crash Location and Severity"> </rpc-table>
  {{</accordion-content>}}
{{</accordion>}}

The results of the transportation safety analysis and the recommended countermeasures for each section are seen below.

## Section 1

**County Road 1950 E - County Road 2075 E**

### Section overview
**Figure 1** shows the two sharp curves within Section 1—the northern curve at County Road 1525 N, and the southern curve at County Road 1500 N. Both curves are approximately 90 degrees. As a rural highway, the speed limit of this section is 55 mph. An overturned motorcycle crash occurred at the curve at County Road 1500 N in 2017. The primary cause of this crash was failure to reduce speed at the sharp turn. For both curves, there is a Reverse Turn Right sign (W1-3) approximately 300 ft before the curve and a One-Direction Large Arrow (W1-6) sign at the tangent section of the curve. 

{{<image src="Problem Segment Diagram.png"
    caption="Figure 1 - Section 1 Overview"
    position="medium"
    alt="Map showing the 2 consecutive sharp curves at this intersection, the signage along the curves, and street view imagery along the curves">}}

### Risk Factor Analysis

The first major risk in this section is the **absence of speed limit and advisory speed signs**. While there are two sharp curves, there are no speed limit or advisory speed signs near the curves (or even within this section). For drivers who are unfamiliar with the roadway geometry and don’t already know how sharp the curves are, there is a chance of entering these curves at a high speed. Driving quickly on such sharp curves introduces dangers such as loss of vehicle control and insufficient response time when encountering a vehicle coming from the other direction. For drivers who are familiar with the roadway condition and believe that they know the road ahead very well, there is a possibility of maintaining the same speed or slowing down an insufficiently safe amount when entering into the curve. 

The second risk is a seasonal risk posed by crops. Tall and dense **crops on adjacent land can severely restrict drivers’ views** at the curves. For an uncontrolled intersection with a speed limit of 55 mph, the required leg length of a clear sight triangle is 285 ft [^1]. However, during the growing season, the actual sight distance at theses curves is less than the required length. With the sight triangle area blocked, drivers cannot see vehicles coming from the other direction and may not have sufficient reaction time if a conflict occurs. This risk is increased by the absence of a centerline and the possibilities of aggressive driving and vehicle overtaking. 

The third risk is also introduced by the adjacent crops. **The One-Direction Large Arrow sign at the curve at County Road 1500 N may not be easily observed in all seasons.** During late summer and autumn, when the corn stalks are as high as the sign, there is a risk of drivers failing to notice the sign due to the similar yellow coloring between the sign and the corn stalks, especially during the daytime when the sun is strong. 
 

### Proposed Countermeasures
The treatments that are appropriate for the study section can be grouped into four categories—speed control signs, curve warnings, physical roadway changes, and vegetation management. 

**The recommendations can be summarized as follows:**

-	Use a larger sign or add flashing beacons to the Reverse Turn sign (W1- 3) at both curves.
-	Supplement the existing One-Direction Large Arrow (W1-6) signs at both curves by:
    - Providing arrow signs for both directions.
    - Adding retroreflective delineators.
-	Install speed control signs.
-	Move back crops at the curve of County Road 1500 N.

#### Speed Control Signs
Analysis of the suitability and effectiveness of each treatment is seen below. For more details regarding the design, application, and cost of these treatments, please refer to the FHWA's "Low-Cost Treatments for Horizontal Curve Safety 2016" [^2].

Although the placement of the Reverse Turn signs implicitly tells drivers that the advisory speed is less than 30 mph [^3], individual drivers may not reach this interpretation in the short time before they reach the curve. Installing speed control signs at the location of the Reverse Turn signs is recommended to more effectively warn drivers to reduce their speed before entering the curves. 

There are two types of speed control signs: static and dynamic. In addition, there are two display content types on the speed control plaque, digital and verbal.  A digital display numerically shows the limit / advisory speed or the vehicle's actual speed, while a verbal display shows a message such as ‘SLOW DOWN’ or ‘THANK YOU’, providing straightforward speed adjustment instruction. The combination of digital content and verbal messages could further strengthen the warning effect of these signs.

Compared with dynamic signs, static signs are low-cost, easy to install, and easy to maintain. However, some studies point out that drivers can be less responsive to static signs over time. Dynamic speed control signs, which give drivers individual feedback, can help fix the problem of habituation.

Considering the installation cost and maintenance fee, ***a static advisory speed sign with flashing beacons is recommended.*** If budget allows, dynamic signs could be considered. 

#### Curve Warnings
    
To alert drivers to the curves in the road, both curves have a Reverse Turn (W1- 3) sign and a One-Direction Large Arrow (W1-6) sign installed. However, improvements are needed to further strengthen their warning effect and to eliminate factors that could impede their function.  

The Reverse Turn sign for the curve at County Road 1525 N is located near a row of trees which have dense leaves from spring to autumn. The sign for the curve at County Road 1500 N is near a corn field. To improve drivers’ perception of the signs and help them respond to the signs earlier, installing larger signs or adding flashing beacons could be considered.

For both curves, the One-Direction Large Arrow sign is available only in one direction. At the curve at County Road 1525 N, there is no sign for westbound traffic (travelling toward Urbana). At the curve at County Road 1500 N, there is no sign for eastbound traffic (travelling toward Homer Lake). As mentioned earlier, the view of the sign at County Road 1500 N is also seasonally impeded by tall yellow corn stalks, especially around noon when the sun is bright. Two improvements should be considered for both curves. First, ***installing One-Direction Large Arrow (W1-6) signs for both directions***. Second, ***placing two to three retroreflective delineators before the One-Direction Large Arrow signs*** to provide additional visual emphasis on the roadway alignment change and allow drivers to react to the change in advance.

The recommended warning signs for Section 1 are shown in the **Figure 2**.

{{<image src="sign_recommendation.png"
    position="medium"
    alt="Map showing the 2 consecutive sharp curves at this intersection, along with the proposed the signage along the curves"
    caption="Figure 2 - Recommended warning signs, along with current signage to preserve (low-opacity icons)">}} 

#### Roadway Physical Changes

***Providing more space for traveling vehicles*** would be an effective way to improve transportation safety in this section. For both curves in the section, the road lanes before and after the curve are approximately 8.5 feet wide. Studies have shown that narrow lanes induce drivers to drive near the center of the road, increasing the likelihood of head-on collisions. The harvest season also leads to particular months with high volumes of heavy vehicles and large trucks on this segment, with their wider dimensions and larger turn radii. If budget allows, improvements such as widening the travel lane and widening the roadway shoulder are worth consideration.

#### Vegetation Management 

Sufficient sight distance should be provided, allowing drivers the time to adjust their speed when conflicting vehicles approach from the other direction. At the curve of County Road 1500 N, the requirements of a clear sight triangle cannot be met during the summer and fall growing season. ***Trimming and removing*** are the two primary strategies for dealing with vegetation obstructions. Trying to restrict the height of corn stalks is senseless and not feasible; therefore,  removing the crops within the sight triangle is suggested. FHWA resources [^4] provide instructions for defining a sight triangle and controlling vegetation at intersections.

## Section 2
**County Road 2075 E – County Road 2200 E**

### Risk Factor Analysis

From 2014 to 2018, four crashes occurred within Section 2—three were at sub-segments between intersections, and one was at the intersection of County Road 2150 E and Homer Lake Rd. The crash types and severity levels are listed in **Table 3**.

<rpc-table url="Crashes within Section 2.csv" table-title="Table 3 - Crashes within Section 2"> </rpc-table>

The Animal crash was caused by a deer. The Other Object crash was caused by a dead pine tree blown down by wind. The Fixed Object crash of B-Injury level was caused by impaired driving and distracted driving. The driver was under the influence of alcohol and was responding to a text message while driving, which caused him to leave the roadway; the vehicle then ran into the Salt Folk River. The Fixed Object crash at the intersection of County Road 2150 E and Homer Lake Rd was caused by weather and roadway surface conditions—the vehicle slid on ice and struck a power pole.

### Proposed Countermeasures

**Roadside vegetation management program**

While roadside vegetation provides aesthetic and environmental benefits, it can also pose dangers to transportation safety. In harsh weather conditions such as heavy snow and heavy rain, tree limbs can be knocked down to the road and obstruct traffic or hit cars directly.  Trees are also the most common struck object in serious Fixed Object crashes [^5].  In addition, vegetation such as shrubs on the roadside may block drivers’ sight distance or obstruct traffic control signs. ***A balance between preserving natural resources and ensuring transportation safety should be maintained.*** A roadside vegetation management program is suggested, and coordination between the highway department, the forestry department and the public is recommended. The following tasks should be considered: 
-	Routinely inspect roadside vegetation conditions and keep the inspection records in a database.
-	Remove dead tree limbs before wind, rain, or snow bring them down into traffic.
-	Remove obstructions that would impact the visibility of traffic control signs
-	Remove obstructions that would block drivers’ sight distance. 
-	Encourage public engagement. The public can provide the most timely information on transportation issues. Strategies such as public surveys and periodical contact with community representatives should be considered. 


## Section 3
**County Road 2200 E – County Road 2300 E**

### Risk Factor Analysis

From 2014 to 2018, there were 12 crashes within  Section 3; 11 occurred at the intersection of County Road 2200 E and Homer Lake Road, and 1 occurred within sub-segment 5. More information on the County Road 2200 E and Homer Lake Road intersection, including analysis of the crashes, risk factors and proposed countermeasures, can be found [here](#http://localhost:1313/ccspi-high-priority-locations/rural-intersections/intersection/). The crash that occurred within sub-segment 5 was a pedestrian crash of C-Injury-level severity. While walking along the roadway for exercise, a pedestrian was struck on the arm by the rear view mirror of a passing vehicle.

### Proposed Countermeasures
Because pedestrian activity is not frequent in rural areas, and there is an existing 4-ft unpaved roadway shoulder in this segment, it would not be cost-effective to build a sidewalk here. Regular traffic counts to monitor movement of pedestrians and bicyclists along the roadway are therefore recommended. If walking and biking activity is observed as frequent, then widening the unpaved shoulder to provide a safer walking and biking environment can be considered.

## Section 4
**County Road 2300 E – County Road 2500 E**

### Risk Factor Analysis
From 2014-2018, four crashes occurred within Section 4. The crash type and severity level are listed in **Table 4**.

<rpc-table url="Crashes within Section 4.csv" table-title="Table 4 - Crashes within Section 4"> </rpc-table>
The Other Non-Collision crash was caused by a vehicle—the trailer detached from the hitch and the vehicle ran into a ditch. The Fixed Object crash was caused by impaired driving—the driver was driving under the influence of alcohol and the vehicle struck a utility pole. The driver was subsequently arrested for driving under the influence of alcohol. 

The fatal crash occurred at a railroad crossing. **Figure 3** shows the location of the railroad crossing. The vehicle hit a train and the 18-year-old female driver died at the scene of the accident. The railroad crossing where the crash occurred is shown in the photo below. At the time of the crash, there were no gates at the railroad crossing, and only lights were used to indicate approaching trains. According to the crash report, the vehicle was traveling westbound on Homer Lake Road and struck the front engine of the train. The train was traveling north at approximately 60 mph. The train conductor and the train engineer stated that the lights were functioning properly, and they never saw a vehicle approaching from either direction. The main cause of the crash is not noted in the crash report. According to Federal Railroad Administration Highway/Rail Grade Crossing Incidents Dashboard, this railway crossing had seen two previous fatal crashes—one in 1977 and another in 1997. In both previous crashes, as in this crash, the vehicle hit the train, rather than the train hitting the vehicle [^6]. 
  

{{<image src="RR_in_2021.png"
    position="medium"
    alt="Photo of a rural road with a railroad crossing, including gates"
    caption="Figure 3 - Railroad crossing, photo taken in 2021">}}

### Proposed Countermeasures

One likely contributor to this tragedy was the the absence of gates when the crash occurred. Gates have since been installed, which significantly reduce the possibility of drivers proceeding when a train is approaching. 

To prevent such tragedy in the future, it is essential to improve driver’s awareness of the crossing signals at a greater distance, so they can receive this information early and have more time to respond to it. The emerging technology of Intelligent Transportation Systems may be a promising solution in the future. Traveler Information systems and Vehicle-to-Infrastructure technologies can inform drivers of the status of railroad crossings ahead and guide them to respond appropriately in advance. 

## Section 5
**County Road 2500 E – S Homer Lake Road**

### Risk Factor Analysis

From 2014 to 2018, three crashes occurred within Section 5. The crash types and severity levels are listed in **Table 5**.

<rpc-table url="Crashes within Section 5.csv" table-title="Table 5 - Crashes within Section 5"> </rpc-table>

The no-injury Fixed Object crash at the intersection of County Road 2500 E and Homer Lake Road was caused by impaired driving. The driver was driving under the influence of alcohol, and the vehicle left the roadway and entered the ditch. The fatal Fixed Object crash was also caused by driving under the influence of alcohol. The driver lost control of the vehicle's speed and struck a tree while trying to avoid a mailbox. When the crash occurred, the driver was not wearing a seatbelt. The driver struck the front dash of the vehicle and received fatal injuries.

### Proposed Countermeasures

Suggested countermeasures for alcohol-impaired driving can be found in the following section. 

## Findings and Proposed Countermeasures

The critical issues identified in the analysis and the proposed countermeasures are listed below. 

### (1)	Alcohol-Impaired Driving

From 2014 to 2018, four crashes along Homer Lake Road (County Road 1950 E – South Homer Lake Road) were caused by driving under the influence of alcohol . All of these were fixed object crashes. The severity levels of these crashes are listed in **Table 6**.

<rpc-table url="Impaired driving crashes of Homer Lake Road.csv" table-title="Table 6 - Alcohol-Impaired Driving Crashes of Homer Lake Road (County Road 1950 E – South Homer Lake Road)"> </rpc-table>

Impaired driving is also recognized as an emphasis area in the Rural Champaign County Area Safety Plan. According to guidance provided by Centers for Disease Control and Prevention and strategies provided in Rural Champaign County Area Safety Plan, efforts can be made in two major areas: ***Education and Enforcement***. 

Two approaches to education are recommended. The first approach is conducting public outreach via media, such as television, radio, billboards, and newsprint. Different media should be applied to different age groups. The second approach is launching school-based, employer-based and community-based education programs. Program content should include the consequences of driving under the influence of alcohol and the available transportation alternatives.

Three enforcement actions are recommended. The first is checking the compliance of alcohol servers and retailers and controlling the hours, locations, and promotion of alcohol sales. The second is implementing high-visibility saturation patrolling at times and locations where alcohol-related crashes are prevalent. The third is encouraging the use of ignition interlocks. Although at this time it is not realistic to require all that vehicles be equipped with an alcohol ignition interlock, the mandatory use of alcohol ignition interlock for all DUI offenders should be considered. 

### (2)	Roadside Crop and Vegetation Management

#### Crop Management
  
Champaign County has large areas of cornfields, which are vital to the local economy. While cornfields bring huge economic benefits, they also provides some dangers and concerns for roadway safety. During the growing season, roadways may be narrowed by the broad leaves and thick stalks of the corn, which increase the risk of vehicles encroaching into the oncoming traffic lane. At intersections or corners, drivers' views may be blocked by the stalks, preventing drivers from seeing each other until just before a crash. A wall of corn stalks behind traffic control signs may also make it harder to discern the signs. Thus, a roadway risk management program is necessary to keep a balance between roadway safety and economic benefit during the growing season. Tasks that should be considered in the program include, but are not limited to:

1.	Reviewing all of the uncontrolled intersections and corners in the rural area where there is a potential risk of drivers’ sight view being blocked by crops during the growing season. 
2.	Checking the traffic control signs near corn fields and adjusting signs if they are at risk of being obstructed by corn stalks.

#### Roadside Vegetation Management
    
  As mentioned earlier, while roadside vegetation is good for landscape and climate, it also poses risks to transportation safety. Champaign County receives heavy snow frequently in winter, during which snow and ice accumulate on tree limbs. This can be dangerous if the tree limbs cannot sustain the heavy weight of the snow and fall onto the roadway. In addition, roadside vegetation in improper positions may obstruct drivers’ views. Therefore, a roadside vegetation management program is essential. The tasks which should be considered in the management program include but are not limited to:

1.	Inspecting roadside vegetation conditions regularly and keeping the inspection records in a database.
2.	Removing dead tree limbs before the wind, rain, and snow bring them into traffic.
3.	Removing obstructions that would impact the visibility of traffic control signs.
4.	Removing obstructions that would block drivers’ sight distance. 
5.	Collecting information from the public and encouraging the public to report potential risks from vegetation.

## References
[^1]: Hancock, M. W., & Wright, B. (2013). A policy on geometric design of highways and streets. American Association of State Highway and Transportation Officials: Washington, DC, USA.

[^2]: Albin, R. B., Brinkly, V., Cheung, J., Julian, F., Satterfield, C., Stein, W. J., ... & Hanscom, F. R. (2016). Low-Cost Treatments for Horizontal Curve Safety 2016 (No. FHWA-SA-15-084). United States. Federal Highway Administration. Office of Safety.

[^3]: Manual on Uniform Traffic Control Devices 

[^4]: Eck, R. W. (2007). Vegetation Control for Safety: A Guide for Local Highway and Street Maintenance Personnel (No. FHWA-SA-07-018).

[^5]: Safety and Trees: The Delicate Balance. (2011b, June 20). Federal Highway Administration. (https://safety.fhwa.dot.gov/roadway_dept/countermeasures/safe_recovery/clear_zones/fhwasa0612/)

[^6]: Ogden, B. D., & Cooper, C. (2019). Highway-Rail Crossing Handbook (No. FHWA-SA-18-040). United States. Federal Highway Administration.



