---
title: "County Highway 15 from Sidney to Homer "
draft: false
weight: 40
bannerHeading: County Highway 15
bannerText: >
   County Rd 2125 E  to County Rd 2600 E
bannerAction: Provide Feedback
bannerUrl: /contact
---

## Introduction

This study was performed to analyze transportation safety along **County Highway 15 from Sidney to Homer** (from County Road 2125 E to County Road 2600 E). The study segment is located between Sidney and Homer on the southeastern side of Champaign County and is approximately 6 miles long. The map below shows the location of the study segment.  

<iframe src="https://maps.ccrpc.org/ccspi-rural-seg-sidney-to-homer/" width="100%" height="600" allowfullscreen="true"></iframe>

In general, the land use along the study roadway is a mix of agricultural, industrial, and residential uses. The roadway shoulders are approximately 3.6 ft wide and unpaved. Utility poles run along the roadway through this segment. Side slopes and ditches are located along some sections of the roadway. This segment has an average daily traffic (ADT) of approximately 1,100 vehicles, including a high volume of heavy vehicles and large trucks.

## Transportation Safety Analysis 

A preliminary analysis was conducted before identifying the risk factors and proposing countermeasures for the entire segment. From 2014 to 2018, 23 reported crashes occured within this segment. The predominant crash types for this segment were Animal crash and Fixed Object crash, accounting for 35% and 17% of all crashes. **Table 1** presents the number of crashes by collision type and year.  

<rpc-table url="Crash_Type_Year.csv" table-title="Table 1 - Summary of crashes by crash type and year"> </rpc-table>

**Table 2** presents the number of crashes by severity level. Most crashes (83%) were non-injury crashes. Definitions for the severity levels can be found in the Appendix.

<rpc-table url="Crash Type Severity.csv" table-title="Table 2 - Summary of crashes by crash type and severity"> </rpc-table>

For this study, the entire segment was divided into 5 sections, as listed in **Table 3**. 

{{<accordion>}}
  {{<accordion-content title="Table 3 - Summary of crashes by crash location and severity">}}
    <rpc-table url="Crash Location Severity.csv" table-title="Table 3 - Summary of crashes by crash location and severity"> </rpc-table>
  {{</accordion-content>}}
{{</accordion>}}

## Section 1:<br /> County Road 2125 E – County Road 2200 E

The land uses of Section 1 are a mix of agricultural and residential uses. A railroad track runs almost parallel with the eastern half of this section. Two property accesses (i.e., driveways) cross the railroad track from County Highway 15; one of them has a crossing sign, and the other has a crossing signal. The roadway has an approximately 3.5 ft wide unpaved shoulder, which is covered in grass in portions. Utility poles run along the roadway.

From 2014 to 2018, four crashes occurred within this section; one was a C-Injury crash and three were No-Injury crashes. The crash types and severity levels are listed in **Table 4**. The Other Non-Collision crash was caused by a driver failing to yield and missing a T intersection. In the Animal crash, the vehicle struck a dog owned by nearby residents. The Other Object crash was caused by a driver failing to reduce speed under adverse roadway conditions. The vehicle hit snow or ice on the shoulder, and the driver lost control of the vehicle, which then struck a guard rail. The Fixed Object crash can be attributed to the physical condition of the driver.

<rpc-table url="Section 1.csv" table-title="Table 4 - Crashes in Section 1"> </rpc-table>

## Section 2: <br /> County Road 2200 E – County Road 2300 E

In Section 2, the western half of the section continues to run parallel to the previously-mentioned railroad. There is one property access to the railroad in this section, shown in **Figure 1**.  A mild curve is located at the center of this section. 

{{<image src="section_2.png"
  alt="Google Streetview photo property access crossing railroad tracks"
  caption="Figure 1 - Property access crossing railroad tracks"
  position="full">}}

From 2014 to 2018, three crashes occurred within Section 2 (as seen in **Table 5** below). The Fixed Object crash occurred when the roadway was covered by snow and slush. The driver lost control of the vehicle and entered a ditch nearby. The train crash was caused by improper backing—the vehicle backed off of the road surface and into the railroad gravel when the driver was using the railroad right-of-way to turn around. The vehicle was not on the tracks, but it was close enough that the train stuck the vehicle. The Overturned crash was caused by impaired driving; the driver was under the influence of both alcohol and cannabis.

<rpc-table url="Section 2.csv" table-title="Table 5 - Crashes in Section 2"> </rpc-table>

## Section 3: <br /> County Road 2300 E – County Road 2400 E

The land uses of Section 3 are a mix of agricultural, industrial, and residential uses. The western edge of this section is home to several industrial facilities. The center of this section, west of County Road 2375 E, features a mild curve. From 2014 to 2018, seven crashes occurred within this section (as shown in **Table 6** below) — five were caused by animals, one was caused by improper passing, and one was caused by distracted driving. In the distracted driving crash, the driver read cellphone messages while driving and struck a cross intersection sign. Among the Animal crashes, three occurred near the intersection of County Road 2375 E and County Road 15, which is close to a stream and a large area of vegetation. 

<rpc-table url="Section 3.csv"  table-title="Table 6 - Crashes in Section 3"> </rpc-table>

## Section 4: <br /> County Road 2400 E – County Road 2500 E

From 2014 to 2018, five crashes occurred within Section 4 (as shown in **Table 7** below) — one occurred at the intersection of County Road 2400 N and County Highway 15, while the remaining four were along the road segment away from any intersections. The intersection crash was a left turn collision with a vehicle passing on the left; this was caused by improper vehicle overtaking. One of the Fixed Object crashes was caused by the driver exceeding the safe speed during adverse weather conditions. The driver braked in a heavy rain downpour and lost control of the vehicle. The vehicle then entered a ditch close to the intersection and struck a utility pole. The other Fixed Object crash occurred when the driver took evasive action to avoid an oncoming vehicle crossing the center line. The evading vehicle exited the roadway and rolled over several times into a plowed field.  The Overturned crash occurred early in the morning and was caused by alcohol-impaired driving. The vehicle left the roadway, struck a guy wire, and rolled several times. The Sideswipe crash was caused by improper overtaking. 

<rpc-table url="Section 4.csv"  table-title="Table 7 - Crashes in Section 4"> </rpc-table>

## Section 5: <br /> County Road 2500 E – County Road 2600 E

From 2014 to 2018, four No-Injury crashes occurred within Section 5 (as shown in **Table 8** below). The Sideswipe crash was caused by distracted driving. The driver was looking at the center console and encroached onto the oncoming lane. The Rear End crash was caused by the malfunction of the following vehicle’s brake. The leading vehicle was slowing down to make a right turn, and the following vehicle could not stop in time and maneuvered left to avoid a collision. However, in doing so, the passenger-side mirror of the following vehicle struck the driver-side rear corner of the leading vehicle.  The two Animal crashes were caused by deer; these crashes occurred near the point where Highway 15 crosses the Veazel Branch stream. 

<rpc-table url="Section 5.csv"  table-title="Table 8 - Crashes in Section 6"> </rpc-table>

## Findings and Proposed Countermeasures

The risk factors for this segment were identified based on the crash data analysis and site investigation results. The identified safety issues and the proposed countermeasures are listed as follows:

### (1) Animal crashes

The predominant type of crash on County Highway 15 from Sidney to Homer is animal crashes, accounting for 41% of all crashes. There were six animal crashes caused by deer and one caused by a pet dog. Placing animal crossing signs along the segment is recommended, especially along Section 3 and Section 5, which include streams and vegetated areas. 

### (2) Unpaved roadway shoulder width 

In Section 4, there were three roadway departure crashes in which vehicles crossed the edge line and left the roadway. The roadway shoulders of Section 4 as well as of some other sections of the study segment are approximately 3.5 feet wide, with some parts covered by grass. Therefore, widening and upgrading the existing unpaved roadway shoulders and installing shoulder rumble strips are recommended. 

### (3) Side slopes and ditches

Some portions of this study segment have side slopes and ditches, which contribute to the risk of vehicle overturn. Regrading the side slopes and ditches should be considered. 



