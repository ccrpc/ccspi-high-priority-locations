---
title: "County Highway 22 from Penfield to Royal "
draft: false
weight: 30
bannerHeading: County Highway 22
bannerText: >
  (1) Walnut St - County Rd 2700 E (2) County Rd 2400 N- County Rd 2000 N.
bannerAction: Provide Feedback
bannerUrl: /contact
---

## Introduction

This study was performed to analyze transportation safety along **County Highway 22 from Penfield to Royal**. The map below shows the location of this study segment. 

<iframe src="https://maps.ccrpc.org/ccspi-rural-seg-penfield-to-royal/" width="100%" height="600" allowfullscreen="true"></iframe>

In general, the surrounding land uses of this roadway are agricultural and residential. The roadway has narrow and unpaved shoulders, along with side slopes. Ditches are located along some sections of the roadway. Buck Creek crosses the study segment along its northern portion, about 2.5 miles south of Penfield.

## Transportation Safety Analysis 

A preliminary analysis of the segment’s history and existing conditions was conducted, followed by identification of risk factors and proposed countermeasures for the entire segment. From 2014 to 2018, there were 14 reported crashes on this segment of road. **Table 1** presents these crashes by collision type and year. 10 run-off-road crashes occured (71% of all crashes), including eight (57%) Fixed Object crashes and two (14%) Overturned crashes. 

<rpc-table url="Crash_Type_Year.csv" table-title="Table 1 - Summary of crashes by crash type and year"> </rpc-table>

**Table 2** presents the number of crashes by type and severity level. Definitions for the severity levels can be found in the Appendix. There was one fatal crash on the segment in this time period; it was caused by driving under the influence of alcohol. Three A-Injury crashes and one B-Injury crash also occured in this period. 

<rpc-table url="Crash Type Severity.csv" table-title="Table 2 - Summary of crashes by crash type and severity"> </rpc-table>

Finally, **Table 3** presents the location and severity level of all crashes. For this study, the entire segment was divided into 5 sections. As a rural roadway, County Highway 22 from Penfield to Royal has a low traffic volume: average daily traffic (ADT) is between 250 and 300 vehicles. Thus, traffic operation analysis was not included in this study.

{{<accordion>}}
  {{<accordion-content title="Table 3 - Summary of crashes by crash location and severity">}}
    <rpc-table url="Crash Location Severity.csv" table-title="Table 3 - Summary of crashes by crash location and severity"> </rpc-table>
  {{</accordion-content>}}
{{</accordion>}}

## Section 1:<br /> Walnut Street – County Road 2700 N ##

As mentioned earlier, cornfields and residences surround Section 1, and the roadway has narrow and unpaved shoulders along with side slopes. Ditches are found at some locations along the section. Buck Creek crosses this section between County Road 2900 N and County Road 2800 N. The photos below show the roadway geometry and physical condition of Section 1. 

{{<accordion>}}
  {{<accordion-content title="Section 1 Figures">}}
    {{<image src="Section_1_1.jpg" alt="Photo of sloping ditch next to road" caption="Figure 1 - Side slope in section 1 (Photo taken in July 2021)" position="full">}}
    {{<image src="Section_1_2.jpg" alt="Photo of narrow and overgrown shoulder along road" caption="Figure 2 - Narrow and unpaved shoulder in section 1 (Photo taken in July 2021)" position="full">}}
  {{</accordion-content>}}
{{</accordion>}}

From 2014 to 2018, five crashes occurred within this section. The crash types, severity levels, and the primary causes are listed in **Table 4**. The predominant crash type was Fixed-Object crash, accounting for 80% of the total crashes within this section. Three crashes were caused by animals (deer), and the remaining two crashes were caused by human factors. These human-caused crashes occurred at midnight; drowsy driving caused one, and impaired driving caused the other. The impaired driving crash led to the fatality of the driver. 

<rpc-table url="Section 1.csv" table-title="Table 4 - Crashes in Section 1"> </rpc-table>



## Section 2: <br /> County Road 2700 N – County Road 2500 N

The photos below show the roadway geometry and physical conditions of Section 2. Buck Creek crosses this section between County Road 2700 N and County Road 2600 N.

{{<accordion>}}
  {{<accordion-content title="Section 2 Figures">}}
    {{<image src="Section_2_1.jpg" alt="Photo road going over a ditch without a barrier" caption="Figure 3 - Ditch without protection in Section 2 (Photo taken in July 2021)" position="full">}}
    {{<image src="Section_2_2.jpg" alt="Photo of narrow shoulder along road" caption="Figure 4 - Narrow and unpaved shoulder in Section 2 (Photo taken in July 2021)" position="full">}}
  {{</accordion-content>}}
{{</accordion>}}

From 2014 to 2018, two crashes occurred within this section. The crash types, severity levels, and the primary causes are listed in **Table 5**. The first crash was an Overturned vehicle caused by a damaged tire. The second crash was a Fixed Object crash that occurred in winter when the road was covered by ice and snow. The vehicle traveled through the adjacent ditch and rolled over—possibly twice—before coming to a rest. 

<rpc-table url="Section 2.csv" table-title="Table 5 - Crashes in Section 2"> </rpc-table>

## Section 3: <br /> County Road 2500 N – County Road 2400 N

Section 3 includes a railroad crossing. From 2014 to 2018, two crashes occurred within this section, one occurred at the intersection of County Road 2500 N and County Road 2700 E, and the other occurred near the railroad crossing south of County Road 2500 N. The crash types, severity levels, and primary causes are listed in **Table 6**. 

{{<accordion>}}
  {{<accordion-content title="Section 3 Figures">}}
    {{<image src="Section_3_1.jpg" alt="Google Streetview photo of rural road intersection" caption="Figure 5 - The intersection of County Road 2500 N and County Road 2700 E" position="full">}}
    {{<image src="Section_3_2.jpg" alt="Photo of road crossing railroad tracks" caption="Figure 6 - Railroad crossing in Section 3 (Photo taken in July 2021)" position="full">}}
  {{</accordion-content>}}
{{</accordion>}}

The intersection of County Road 2500 N and County Road 2700 E is a Two-Way Stop-Controlled intersection. When the driver was traveling southbound on 2700 E and approaching the intersection, he observed a semi-truck pulling into the intersection. To avoid the semi-truck, the driver left the roadway and drove into a ditch. The vehicle continued to travel south in the ditch and struck an entry culvert and a tree. The crash near the railroad crossing occurred while maintenance was being performed on the railroad crossing; there were two nearby barricades with ROAD CLOSED signs attached. The driver didn’t see the warnings and hit the barricades. 

<rpc-table url="Section 3.csv" table-title="Table 6 - Crashes in Section 3"> </rpc-table>

## Section 4: <br /> County Road 2400 N – County Road 2150 N

From 2014 to 2018, three motor vehicle crashes occurred within Section 4, one of which was a motorcycle crash. The crash types, severity levels, and primary causes are listed in **Table 7**. 

<rpc-table url="Section 4.csv" table-title="Table 7 - Crashes in Section 4"> </rpc-table>

In all three crashes, the vehicle veered off the travel lane and into the soft shoulder.  When the driver tried to pull back onto the road, the sloping shoulder or the drop-off at the edge of the road caused the driver to lose control of the vehicle. 

{{<accordion>}}
  {{<accordion-content title="Section 4 Figures">}}
    {{<image src="Section_4_1.jpg" alt="Photo of narrow and unpaved road shoulder with sloping ground next to the road" caption="Figure 7 - Narrow and unpaved shoulder with side slope in section 4 (Photo taken in July 2021)" position="full">}}
    {{<image src="Section_4_2.jpg" alt="Photo of rural road without any painted lane markings" caption="Figure 8 - Roadway without lane marking in section 4 (Photo taken in July 2021)" position="full">}}
  {{</accordion-content>}}
{{</accordion>}}

## Section 5: <br /> County Road 2150 N – County Road 2000 N

From 2014 to 2018, two crashes occurred within Section 5. The crash types, severity levels, and primary causes are listed in **Table 8**. The animal crash was caused by a deer. There is a cemetery on the southwest side of the intersection of County Road 2700 E and County Road 2150 N, as well as a large area of vegetation on the southeast side of the intersection. The animal crash was located near the area of vegetation. The second crash was a fixed object crash caused by impaired driving. The driver received an A-level injury.

<rpc-table url="Section 5.csv" table-title="Table 8 - Crashes in Section 4"> </rpc-table>

{{<accordion>}}
  {{<accordion-content title="Section 5 Figure">}}
    {{<image src="Section_5_1.jpg" alt="Photo of narrow and unpaved road shoulder with sloping ground next to the road" caption="Figure 9 - Narrow and unpaved shoulder with side slope in section 5 (Photo taken in July 2021)" position="full">}}
  {{</accordion-content>}}
{{</accordion>}}

## Findings and Proposed Countermeasures

The risk factors below were identified based on the crash data analysis and site investigation results. The identified safety issues and the proposed countermeasures are listed as follows.

### (1)	Narrow and unpaved roadway shoulder, along with side slopes and ditches

From 2014 to 2018, 10 Run-Off-Road crashes occurred along County Highway 22 from Penfield to Royal (71% of all crashes in this corridor), including eight (57%) Fixed Object crashes and two (14%) Overturned crashes. Many of the Fixed Object crashes also reported vehicle overturn. Driving at a high speed on a roadway with narrow and unpaved shoulders can lead to a high risk of run-off-road crashes. The side slopes and ditches along the roadway further increase the risk of vehicle overturn. 

Studies have shown that when a roadway shoulder is soft or covered with grass or loose gravel, the possibility of vehicle skidding or turnover increases [^1] [^2]. Along County Highway 22 from Penfield to Royal, there are several places where the road shoulder is unstable and narrow. Increasing shoulder stability and providing wider shoulders are recommended. If budget allows, replacing the unpaved shoulder with a paved shoulder and installing shoulder rumble strips are also recommended. The paved shoulder with rumble strips can help drivers stay on the roadway and mitigate vehicle skidding issues, reducing the risk of vehicles running off the road.

Roadside improvements that can help reduce crash risks and mitigate crash consequences are also recommended. For the study segment, improvements such as flattening side slopes and installing guardrails near ditches—where run-off road crashes might lead to severe injuries—are suggested. An example of the appropriate context for ditch guardrail installation can be found in Section 2.

### (2)	Impaired driving

The issue of impaired driving is worthy of attention. There were two crashes caused by impaired driving within this segment. One led to a fatal crash and the other one led to an A-injury crash. The countermeasures for impaired driving mainly focus on education and enforcement. Please refer to the [Rural Champaign County Area Safety Plan](https://ccrpc.org/wp-content/uploads/2020/08/Revised-Final-Rural-Safety-Plan_2020-08-20.pdf) for more details regarding impaired driving countermeasures.

### (3)	Animal crashes 

The issue of animal crashes is also noteworthy along this road segment. Installation of Deer Crossing signs along the segment is recommended. 

# 4 References

[^1]: Viner, J. G. (1995). Rollovers on sideslopes and ditches. Accident Analysis & Prevention, 27(4), 483-491.

[^2]: Deleys, N. J., & Brinkman, C. P. (1987). Rollover potential of vehicles on embankments, sideslopes, and other roadside features. SAE transactions, 907-916.

