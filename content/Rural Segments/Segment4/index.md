---
title: "North Maplewood Drive "
draft: false
weight: 50
bannerHeading: North Maplewood Drive
bannerText: >
   North Maplewood Drive
bannerAction: Provide Feedback
bannerUrl: /contact
---

## Introduction

This study was performed to analyze transportation safety along **North Maplewood Drive (from Veterans Parkway to County Road 3300 N)**. This segment is approximately 3.5 miles long and runs north-south, with no curves along the segment.  The map below shows the location of the study segment.  

<iframe src="https://maps.ccrpc.org/ccspi-rural-seg-maplewood/" width="100%" height="600" allowfullscreen="true"></iframe>

The surrounding land uses around this roadway are primarily agricultural, commercial, and residential. There are 20 intersections along this segment. **Table 1** shows the control type of each intersection. As this roadway serves traffic in Rantoul, it has a moderately high traffic volume in the southern portion of the segment, with an annual average daily traffic (AADT) of 6,100 vehicles/day along its busiest portion, according to 2016 data [^1]. In the northern portion, AADT was measured as low as 650 vehicles/day [^1]. Since we consider this segment a rural segment (outside of the Metropolitan Planning Area, seen on the map) and traffic volumes are not sufficiently high to impact traffic operation, traffic operation analysis was not included in this study.

{{<accordion>}}
  {{<accordion-content title="Table 1 - Intersection Geometry and Control Type">}}
    <rpc-table url="int_control.csv" table-title="Table 1 - Intersection Geometry and Control Type"> </rpc-table>
  {{</accordion-content>}}
{{</accordion>}}

## Transportation Safety Analysis 

A detailed safety analysis was conducted before identifying risk factors and proposing countermeasures for the entire segment. From 2014 to 2018, there were 56 reported crashes within this segment. Fifty-one of these were intersection crashes, and five were segment crashes away from intersections. The predominant crash type for this segment was Turning crashes, which account for 29% of all crashes. **Table 2** presents the number of crashes in this segment by crash type and year.  


<rpc-table url="Crash_Type_Year.csv" table-title="Table 2 - Summary of crashes by crash type and year"> </rpc-table>

**Table 3** presents the number of crashes by severity level. Most crashes (66%) were No-Injury crashes. In the five-year analysis period, there were no fatal crashes and only one A-injury crash along this segment. The crash locations and causes in this time period varied significantly, with no overwhelming concentrations.

<rpc-table url="Crash Type Severity.csv" table-title="Table 3 - Summary of crashes by crash type and severity"> </rpc-table>

For this study, the entire segment was divided into 4 sections, as listed in **Table 4**. **Table 4** also presents the location and severity level of all crashes.

{{<accordion>}}
  {{<accordion-content title="Table 4 - Summary of crashes by crash location and severity">}}
    <rpc-table url="Crash Location Severity.csv" table-title="Table 4 - Summary of crashes by crash location and severity"> </rpc-table>
  {{</accordion-content>}}
{{</accordion>}}

## Section 1:<br /> Veterans Parkway - East Grove Avenue

Section 1 is the southernmost section and is approximately 0.5 miles long. The land uses of this section are a mix of commercial and residential, which results in many vehicle accesses (i.e., driveways) along this section. Utility poles and streetlights are present along the roadway. The road runs north-south and does not curve at any point along this section. The width of the roadway is around 55 feet, and roadway markings are present along this section, with has two lanes per direction and a center turning lane in the middle of the roadway. The roadway has no shoulder. A school zone for Pleasant Acres Elementary School is located on the south side of this section, with a 20 miles per hour (mph) speed limit. Otherwise, the speed limit of this section is 30 mph. The road was resurfaced in September 2021.

From 2014 to 2018, no segment crashes occurred within this section, but 16 intersection crashes occurred. Five of the 16 intersection crashes occurred at the Maplewood Drive and Veterans Parkway signalized intersection, including one B-injury crash for impaired driving. Four of the crashes occurred at the Maplewood Drive and Fairlawn Drive intersection, including one that caused a B-injury to a pedestrian. Most of the intersection crashes resulted in no injuries. One C-injury crash occurred at the Maplewood Drive and Harmon Drive intersection. Most of the crashes were Angle- or Turning-related collision types; these are the typical crash types for intersections. The crash type and severity level for all crashes are listed in the table below. All the crashes shown in this table are intersection crashes.  

{{<accordion>}}
  {{<accordion-content title="Table 5 - Crashes in Section 1">}}
    <rpc-table url="Section 1.csv" table-title="Table 5 - Crashes in Section 1"> </rpc-table>
  {{</accordion-content>}}
{{</accordion>}}

The primary reasons for many of these crashes were improper lane usage, improper backing, and failure to yield right of way. Some drivers were also exceeding safe speeds. Based on the previous crashes in this section, a road diet would likely improve safety. A road diet will help reduce traffic speed, reduce the chance of sideswipe crashes, and provide a safe crossing distance to the bicyclists and pedestrians. Improving driver behavior may also reduce the chance of crashes at intersections.

## Section 2: <br /> East Grove Avenue – Hobson Drive

Section 2 is approximately 0.8 miles long. This section has many of the same physical characteristics as Section 1; however, the roadway width of Section 2 is 50 feet, with no center turning lane. There is a school zone speed limit for Eastlawn Elementary School of 20 mph between Clark Street and Briarcliff Drive. Otherwise, the speed limit is 30 mph throughout this section. The roadway here was also resurfaced in September 2021 (shown in **Figure 1**). 

{{<image src="section_2.png"
  alt="Photo of newly paved and painted road"
  caption="Figure 1 - Maplewood Drive section repaved in 2021"
  position="full">}}

From 2014 to 2018, a total of 36 crashes occurred within this section. Thirty-three of these were intersection crashes, while the remaining three were segment crashes—all three of these were turning-related. Among the 33 intersection crashes, 18 occurred at the signalized intersection of Maplewood Drive and East Grove Avenue. Within these 18 crashes, there were two B-injury crashes, six C-injury crashes, and 10 no injury crashes. Five crashes occurred at the intersection of Maplewood Drive and Clark Street. One of them was a pedalcyclist crash, and in another crash, the sudden movement of a pedestrian was responsible for that crash. Rear-end, angle, and turning-related crashes were the most common intersection crashes along this section of Maplewood Drive. In many cases, drivers failed to reduce their speed at these intersections. 

Eleven rear-end crashes occurred in Section 2 from 2014 to 2018. Six out of these 11 crashes occurred when the road surface was wet or there was ice on the road. The crash type and severity levels for all crashes are listed in the table below. In this table, all the crashes are intersection crashes, aside from the asterisk marked crashes, which are the segment crashes.

{{<accordion>}}
  {{<accordion-content title="Table 6 - Crashes in Section 2">}}
    <rpc-table url="Section 2.csv" table-title="Table 6 - Crashes in Section 2 (* indicates segment crash)"> </rpc-table>
  {{</accordion-content>}}
{{</accordion>}}

The primary reasons for the intersection crashes (with the number of crashes noted in parentheses) were failing to reduce speed (11), failing to yield right of way (8), and improper lane usage (4). The segment crashes were caused by failing to yield right of way, failing to reduce speed, and improper turning. A road diet, access control, and improved driving behavior may reduce the chance of future crashes occurring within this section.

## Section 3: <br /> Hobson Drive – County Road 3200 N

Section 3 is approximately 1.2 miles long, and the roadway has two lanes per direction. The total width of the roadway is approximately 50 feet. This section does not feature a road shoulder; however, there is a curb along its full length. **Figure 2** shows the roadway conditions. The speed limit here is 55 mph. The land uses along this section are mostly agricultural with a few residential parcels. Utility poles are present, but no street lights are present along this section. AADT in 2016 was measured at 850 vehicles/day [^1]. From 2014 to 2018, two crashes occurred within this section; the table below shows this section's crash details.

<rpc-table url="Section 3.csv"
table-title="Table 7 - Crashes in Section 3"> </rpc-table>

The first crash was a segment crash, where the driver was impaired and driving northbound and hit a 12-year-old bicyclist, causing an A-level injury to the child. The other crash was located at the intersectin of Maplewood Drive and Hobson Drive, where a driver sideswiped another car. Maplewood Drive is a four lane road in this section, but the AADT is very low. Reducing the number of lanes might improve road users' safety, by reducing vehicles’ speed and reducing the possibility of sideswipe crashes. A reduction in the number of lanes will not affect the overall operation of this roadway.

{{<image src="section_3.png"
  alt="Photo of 4-lane road"
  caption="Figure 2 - Maplewood Drive Section 3"
  position="full">}}

## Section 4: <br /> County Road 3200 N – County Road 3300 N

Section 4 is almost a mile long. **Figure 3** shows the roadway conditions. There are no streetlights present in this section. Roadway markings exist along this section, showing one lane per direction. The roadway width is around 20 feet, and an approximately one-foot wide shoulder is present on each side. The shoulder is covered with vegetation in several areas. The surrounding land is primarily used for agricultural and residential purposes, which results in some accesses along this section. The speed limit is 55 mph on the section's north side, which is reduced to 40 mph on the south portion of this section, creating a speed zone in this section. The AADT in Section 4, as measured in 2016, was 650 vehicles/day [^1]. From 2014 to 2018, two crashes occurred within this section: one at the intersection of Maplewood Drive and County Road 3300 N, and the other in subsegment 19 (see the Table 4 for subsegment descriptions). The intersection crash was angle-related, and occurred in the morning on a wet road surface. The intersection of Maplewood Drive and County Road 3300 N is a two-way stop-controlled intersection with stop signs in the east-west direction. The driver was driving westbound and failed to notice the southbound approaching car, causing a C-injury crash. The segment crash was a hit-and-run case, where the driver drove off of the roadway and hit a split rail fence. Improving driving behavior may improve the safety of roadway users and reduce the chance of crashes in this section.

{{<image src="section_4.png"
  alt="Photo of 2-lane rural road"
  caption="Figure 3 - Maplewood Drive Section 4"
  position="full">}}

<rpc-table url="Section 4.csv"
table-title="Table 8 - Crashes in Section 4"> </rpc-table>

## Findings and Proposed Countermeasures

Risk factors were identified based on the crash data analysis and site investigation results. The identified safety issues and the proposed countermeasures are listed as follows.

### (1) Road Diet

After analyzing the crash reports, it was found that many crashes occurred due to speeding. In addition, due to too many conflict points along this segment from Veterans Parkway to County Road 3200 N, drivers have to be very careful when making a left turn onto Maplewood Drive from intersections or driveways. As this roadway does not serve heavy traffic, a road diet on Maplewood Drive is recommended from Veterans Parkway to County Road 3200 N. The road diet will help reduce rear-end crashes, reduce traffic speed, reduce the chances for sideswipe crashes, provide travel space on the road for bicyclists, reduce the crossing distance for bicyclists and pedestrians, and reduce conflict points [^2]. A road diet with two travel lanes, one center turning lane, and two bike lanes is recommended along the southern end of this segment, from Maplewood Drive from Veterans Parkway to Hobson Drive. In **Figure 4 - Diagrams a and b** show the current roadway width for this segment, and **Figure 4 - Diagrams c and d** show the recommended road diet widths. To improve safety for kids walking and biking to school, median refuge islands should be installed in the center lane at Eastlawn School and Eater Drive, as recommended in the Safe Routes to School Plans for Eastlawn School [^3] and Pleasant Acres School [^4], respectively. Where the roadway width is 55 feet between Veterans Parkway and East Grove Avenue, the extra seven feet of space that a road diet would create should be used as buffers between the travel lanes and bike lanes. As AADT is very low (850 vehicles/day) in the northern segment between Hobson Drive to County Road 3200 N, a road diet with two lanes (one lane per direction) is recommended in that segment.

{{<image src="road_diet.png"
  alt="Diagram with three scenarios: The current conditions with two 11' lanes each way and turn lane, as well as two 12' lanes each way, followed by the recommended road diet, with a one lane in each direction, a turn lane, and bike lanes on each side"
  caption="Figure 4 - Maplewood Drive existing and recommended cross-sections"
  position="full">}}

### (2) Bike Lanes 

If a road diet is implemented, providing bike lanes along Maplewood Drive from Veterans Parkway to Hobson Drive should be considered to enhance connectivity and safety for bicyclists. Currently, a shared-use path is present on the west side of the road from Veterans Parkway to Clark Street. In the Village of Rantoul Transportation Plan [^5], a shared-use path is recommended from Clark Street to just south of Hobson Drive. However, the Eastlawn Safe Routes to School Plan recommends a safety study of Maplewood Drive to look at how safety can be improved for pedestrians, bicyclists, and vehicles [^3]. Adding bike lanes on Maplewood Drive will allow both bicyclists and vehicles to use the road, as well as shortening the crossing distance for pedestrians crossing the street, which reduces the time they are exposed to vehicles. There is space to install buffered bike lanes on Maplewood Drive between Veterans Parkway and Grove Avenue, improving bicyclists' safety and comfort by keeping them further away from vehicles. The map below shows the location of the recommended bike lanes and how they will connect to the existing and recommended shared-use paths. 

{{<image src="bike_lanes.png"
  alt="Map of recommended bike lanes in the northeastern side of Rantoul"
  caption="Figure 5 - Existing and Recommended Bicycle Facilities in the study section"
  position="full">}}

### (3) Access Control

Turning crashes were prevalent in this segment. Intersections are plentiful along this segment, with turning crashes occurring frequently at the intersections. Three out of five segment crashes were also turning-related crashes. The overabundance of road accesses contributes to this type of crash. There are many driveway accesses from residences on Maplewood Drive, especially in Section 1 on the south side of the study area. Many of these accesses are also close to intersections. There are two signalized intersections and eighteen unsignalized intersections between Veterans Parkway and County Road 3300 N. Generally, driveways within 250 feet of a signalized intersection are the most critical places where authorities can take a thorough look and take necessary measures such as median construction, driveway closures or consolidations, or imposing left-turning restrictions. Providing transverse rumble strips on the minor streets between County Road 3200 N and County Road 3300 N can also be considered.

### (4) Driving Behavior

Rear-end crashes were one of the predominant crash types at the intersections in this segment. Improving driver awareness is very important to avoid this type of crash. For this reason, a program can be arranged for local drivers to improve their driving awareness. A public awareness campaign can also be conducted to improve driving behavior. This will help drivers to remain focused while driving. Permanent vehicular speed feedback signs should also be installed on Maplewood Drive to make drivers aware of their speed, as recommended on streets where speeding is a problem in the Rantoul Transportation[^5] and Safe Routes to School Plans[^3] [^4]. In addition, imposing a sanction against repeat traffic violation offenders will improve safety for roadway users.

### (5) Intersection Safety Study

It is recommended to conduct a  detailed safety study at the Maplewood Drive and East Grove Avenue intersection. This intersection was also identified as one of the high priority intersections in the Village of Rantoul Transportation Plan 2020[^5]. At this signalized intersection, 18 crashes occurred between 2014 and 2018; approximately 35% of all intersection crashes along the study segment of Maplewood Drive occurred at this intersection. However, considering that East Grove Avenue is a State route, it is recommended for IDOT consideration for a future intersection safety study.


## References
[^1]:	Illinois Department of Transportation (https://www.gettingaroundillinois.com/Traffic%20Counts/index.html)
[^2]:	An Evaluation of "Road Diet" Projects on Five Lane and Larger Roadways (https://nacto.org/wp-content/uploads/2017/11/An-Evaluation-of-Road-Diet-Projects-on-Five-Lane-and-Larger-Roadways.pdf)
[^3]:	Eastlawn Elementary School | Safe Routes to School Plan 2020 (https://ccrpc.org/wp-content/uploads/2020/09/EastlawnSRTSPlan_2020_07.pdf)
[^4]:	Pleasant Acres Elementary School | Safe Routes to School Plan 2020 (https://ccrpc.org/wp-content/uploads/2020/09/PleasantAcresSRTSPlan_2020_07.pdf) 
[^5]:	Village of Rantoul Transportation Plan (https://ccrpc.org/wp-content/uploads/2020/07/Final_Rantoul-Transportation-Plan_07.15.2020.pdf)



