---
title: "Rural Segments"
date: 2018-01-24T10:48:01-06:00
draft: false
weight: 30
menu: main
bannerHeading: Rural Segments
bannerText: >
bannerAction: Provide feedback
bannerUrl: /contact
---

In the rural area, four segments were identified as high-priority locations. The locations are shown below:

<rpc-table url="rural_seg.csv" table-title="Rural Segments"> </rpc-table>


