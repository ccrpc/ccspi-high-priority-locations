---
title: "Safety"
date: 2018-01-18T11:21:07-06:00
draft: false
bannerHeading: Safety Plan Implementation.
bannerText: >
  This project aims to implement recommendations from the Urban and Rural Safety Plans through conducting detailed safety analysis for specific locations.
bannerAction: Provide Feedback


bannerUrl: /contact
---

For any questions or comments please contact:

RITA MOROCOIMA-BLACK
Title: Planning and Community Development Director
Email: RMorocoima-Black@ccrpc.org

This project aims to implement recommendations from the Urban and Rural Safety Plans through conducting detailed safety analysis for specific locations. Candidate intersections and segments were identified as high priority locations on county and township roadways in rural areas and city roadways in urban areas of Champaign County using the last five years’ of available crash data. As part of the safety analysis, detailed recommendations for improvements will be proposed so that the responsible agencies can make informed decisions to improve safety at the identified locations.
