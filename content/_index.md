---
title: "Safety"
date: 2018-01-18T11:21:07-06:00
draft: false
bannerHeading: Safety Plan Implementation
bannerText: >
bannerAction: Provide Feedback


bannerUrl: /contact
---

This project aims to implement recommendations from the Urban and Rural Safety Plans through conducting detailed safety analyses for specific locations. Candidate intersections and segments were identified as high-priority locations on county and township roadways in rural areas and city roadways in urban areas of Champaign County using the available crash data from 2014-2018. As part of the safety analysis, detailed recommendations for improvements are proposed so that the responsible agencies can make informed decisions to improve safety at the identified locations.
