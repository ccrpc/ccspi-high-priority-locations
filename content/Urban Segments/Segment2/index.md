---
title: "Bradley Avenue (Oak Street - County Road 600 E)"
draft: false
weight: 30
bannerHeading: Bradley Avenue (Oak Street - County Road 600 E).
bannerText: >
  Bradley Avenue (Oak Street - County Road 600 E)
bannerAction: Provide Feedback
bannerUrl: /contact
---
## Introduction

### Background
This study analyzes safety conditions along **Bradley Avenue (from Oak Street to County Road 600 E)**. This corridor runs east-west for approximately six miles, primarily within the City of Champaign but extending into unincorporated Champaign County for the two westernmost miles. **Map 1** shows the location of the study corridor. 

<iframe src="https://maps.ccrpc.org/ccspi-urban-seg-bradley/" width="100%"  height="600" allowfullscreen="true"></iframe>

The surrounding land uses around this roadway are a combination of residential, commercial, recreational, and industrial. In terms of functional classification, Bradley Avenue operates as a minor arterial from Oak Street to Staley Road and as a local road from Staley Road to County Road 600 E (the unincorporated portion). From Oak Street to just west of Duncan Road, Bradley Avenue has two lanes in each direction with varying widths, while from that point to the west, the corridor has one lane in each direction. The roadway has one-foot-wide marked medians from Oak Street to McKinley Avenue and four-foot-wide marked medians from McKinley Avenue to Pomona Drive. Road markings are present, and road surface condition is poor to average along the corridor. As this roadway serves traffic to several industrial locations, it has a moderately high traffic volume of heavy vehicles. This corridor’s annual average daily traffic (AADT) ranges from 750 to 15,600 vehicles per day, according to 2021 IDOT data [^1]. The highest AADT is along the middle portion of the corridor, and the lowest AADT is along the local road section on its western edge. There are 35 intersections along this corridor, including eight signalized intersections. There are streetlights present from the eastern end of the corridor through Crestwood Drive. This corridor has no bike lanes, with the exception of an approximately one-mile stretch from Duncan Road to Staley Road. 

## Transportation Safety Analysis

A preliminary analysis was conducted before identifying risk factors and proposing countermeasures for the entire corridor. From 2015 to 2019, 645 reported crashes occurred within this corridor. Five hundred and fifty-two were intersection crashes, and 93 were mid-block crashes. Most crashes (71.9%) were No-Injury crashes. For this study, the entire corridor was divided into five sections: 

- Section 1: Oak Street to State Street
- Section 2: State Street to Prospect Avenue
- Section 3: Prospect Avenue to Mattis Avenue
- Section 4: Mattis Avenue to Staley Road
- Section 5: Staley Road to County Road 600 E

**Map 2** shows the locations of each section. Detailed safety analysis for each section will be provided later in this report. The first four sections cover the minor arterial portion of this corridor, and the last section covers the local road portion.

{{<image src="Map2.png" 
    caption="Map 2 - Location of each section on Bradley Avenue"
    position="medium" 
    alt=" ">}}

The predominant crash types for this corridor were rear-end and turning crashes, which account for more than 65% of all crashes. **Table 1** and **Figure 1** present the number of crashes in this corridor by crash type and year.

{{<accordion>}}
  {{<accordion-content title="Summary of crashes by crash type and year">}}
    <rpc-table url="Table1.csv" table-title="Table 1 - Summary of crashes by crash type and year"> </rpc-table>
    <rpc-chart url="Figure1_table.csv"
    chart-title="Figure 1 - Summary of crashes by crash type and year"
    y-label="Crash Counts"
    type="barh"
    stacked="true"></rpc-chart>

  {{</accordion-content>}}
{{</accordion>}}

**Table 2** and **Figure 2** show the number of crashes by crash type and severity level. There were six fatal and 25 A-Injury crashes in the five-year analysis period along this corridor. The crash locations and causes in this period varied significantly, with overwhelming concentrations at several intersections. **Map 3** shows the location of mid-block fatal and A-Injury crashes, and **Map 4** shows the same information for intersection crashes.

{{<accordion>}}
  {{<accordion-content title="Summary of crashes by crash type and severity level">}}
    <rpc-table url="Table2.csv" table-title="Table 2 - Summary of crashes by crash type and severity level"> </rpc-table>
    <rpc-chart url="Figure2_table.csv"
    chart-title="Figure 2 - Summary of crashes by crash type and severity level"
    y-label="Crash Counts"
    type="barh"
    stacked="true"></rpc-chart>
  {{</accordion-content>}}
{{</accordion>}}

{{<image src="Map3.png" 
    caption="Map 3 - Location of fatal and A-Injury mid-block crashes on Bradley Avenue"
    position="medium" 
    alt=" ">}}

{{<image src="Map4.png" 
    caption="Map 4 - Location of fatal and A-Injury intersection crashes on Bradley Avenue"
    position="medium" 
    alt=" ">}}

## Section 1: Oak Street - State Street

Section 1 is the easternmost section and is approximately 0.6 miles long. The land uses surrounding this area are a mix of residential, recreational, and commercial, which result in many accesses (i.e., driveways) along this section. A train crossing is located between Oak Street and Chestnut Street on the eastern portion of this section. The train crossing has crossing signals on Bradley Avenue. There are four signalized intersections located within this small section. The roadway has a width of approximately 46 feet and two lanes per direction (**Figure 3**), with 11 feet per lane and no physical median (measured from aerial image). Roadway markings are present. The roadway has no shoulder. The speed limit of this section is 30 miles per hour (mph) from Oak Street to just east of Walnut Street and 35 mph for the rest of the section. Speed limit signs are present in both directions along this section. This section has an approximately four-foot-wide sidewalk on both sides of the Bradley Avenue roadway. The road surface condition is poor to average in this section. 

{{<image src="Figure3.png" 
    caption="Figure 3 - Bradley Avenue Section 1"
    position="medium" 
    alt=" ">}}

From 2015 to 2019, 167 crashes occurred within this section. **Table 3** summarizes the number of crashes and severity levels at each location.

{{<accordion>}}
  {{<accordion-content title="Summary of crashes by crash location and severity level (Section 1)">}}
    <rpc-table url="Table3.csv" table-title="Table 3 - Summary of crashes by crash location and severity level (Section 1)"> </rpc-table>
  {{</accordion-content>}}
{{</accordion>}}

Three fatal crashes occurred within this section. Two were intersection-related crashes, and the other was a mid-block fatal crash at the train crossing. Another B-Injury crash also occurred at this crossing. The train crossing has crossing signals on Bradley Avenue. A pedestrian fatality occurred at the Hickory Street intersection, where a motorbike hit a pedestrian. There was no marked pedestrian crossing at this intersection. All of the A-Injury crashes in this section occurred at intersections. Most of the intersection-related crashes happened at signalized intersections. The intersections of Market Street, Neil Street, Randolph Street, and State Street had the highest crash rates. These four signalized intersections are located within a 0.42-mile span of the section, and they have several access openings within the intersection’s functional area. A road diet, installing flashing yellow signals for left-turning movement at intersections, and consolidating some access openings will improve road user safety in this section. 

## Section 2: State Steet - Prospect Avenue

Section 2 is also approximately 0.6 miles long. The land uses surrounding this area are a mix of residential and commercial. There is a train crossing located east of the intersection of Prospect Avenue and Bradley Avenue. This train crossing also has crossing signals on Bradley Avenue. There are three unsignalized intersections and one signalized intersection situated in this section. The width of the roadway is approximately 45 feet and two lanes per direction, with 11 feet per lane and no physical median or shoulder (measured from aerial image). Roadway markings are present. The speed limit of this section is 35 mph, and speed limit signs are present in both directions along this section. This section has an approximately four-foot-wide sidewalk on both sides of the Bradley Avenue roadway. The road surface condition is poor to average in this section. **Figure 4** shows this roadway condition. This section also has many private properties, resulting in many access openings such as driveways.

{{<image src="Figure4.png" 
    caption="Figure 4 - Bradley Avenue Section 2"
    position="medium" 
    alt=" ">}}

From 2015 to 2019, 100 crashes occurred within Section 2. **Table 4** summarizes the number of crashes and severity levels at each location.

{{<accordion>}}
  {{<accordion-content title="Summary of crashes by crash location and severity level (Section 2)">}}
    <rpc-table url="Table4.csv" table-title="Table 4 - Summary of crashes by crash location and severity level (Section 2)"> </rpc-table>
  {{</accordion-content>}}
{{</accordion>}}

One fatal intersection crash occurred within this section. That crash was an angle crash and occurred due to speeding. Two A-Injury crashes occurred at the Prospect Avenue and Bradley Avenue intersection. This intersection is the primary problem intersection within Section 2. Several driveways are located within the intersection’s functional area, and standard driveway spacings are not maintained throughout this section, based on CUUATS Access Management Guidelines [^2]. Road safety improvements in this section include implementing a road diet with bike lanes, installing flashing yellow signals for left-turning movement at the Prospect Avenue and Bradley Avenue intersection, and consolidating some access openings.

## Section 3: Prospect Avenue - Mattis Avenue

Section 3 is approximately one mile long. This area's land uses are a mix of residential, industrial, and commercial. For this reason, many accesses are present within this section. There are ten unsignalized intersections and one signalized intersection located within this section. The intersection of McKinley Avenue and Bradley Avenue is the only all-way stop-controlled intersection within this section. The width of the roadway is approximately 40 feet and two lanes per direction, with ten feet per lane and no physical median and shoulder east of the McKinley Avenue and Bradley Avenue intersection (measured from aerial image and shown in **Figure 5**). West of McKinley Avenue, the roadway width increases to 48 feet with 11 feet per lane and a four-foot-wide marked median. Roadway markings are present. The speed limit of this section is 35 mph, and speed limit signs are present in both directions along this section. Section 3 has an approximately four-foot-wide sidewalk on both sides of the Bradley Avenue roadway. The road surface condition is poor to average in this section. 

{{<image src="Figure5.png" 
    caption="Figure 5 - Bradley Avenue Section 3"
    position="medium" 
    alt=" ">}}

From 2015 to 2019, 250 crashes occurred within Section 3. **Table 5** provides a summary of the number of crashes and severity levels at each location.

{{<accordion>}}
  {{<accordion-content title="Summary of crashes by crash location and severity level (Section 3)">}}
    <rpc-table url="Table5.csv" table-title="Table 5 - Summary of crashes by crash location and severity level (Section 3)"> </rpc-table>
  {{</accordion-content>}}
{{</accordion>}}

Two fatal crashes occurred within this section; both were pedestrian-related crashes at the intersection of Willis Avenue and Bradley Avenue in 2016 and 2017. The City of Champaign performed work on this section in 2019, and some improvements, such as pedestrian facilities and street lighting, were made. Some crash reports mentioned that heavy vehicles frequently blocked views both at intersections and mid-block locations in this section. Speeding was also an issue mentioned in some crash reports. Standard driveway spacings are not maintained throughout this section, according to CUUATS Access Management Guidelines [^2]. The Mattis Avenue and McKinley Avenue intersections were the most problematic intersections within this section, and both were identified as high-priority urban locations for this Safety Plan Implementation project. These two locations have therefore been analyzed, and the findings will be published in the final report. Overall, implementing a road diet, installing flashing yellow signals for left-turning movement at intersections, and consolidating some access openings will improve safety. 

## Section 4: Mattis Avenue - Staley Road

Section 4 is approximately two miles long. The land uses surrounding this area are a mix of residential, institutional, recreational, and commercial. This section serves traffic to Parkland College. There are seven unsignalized intersections and two signalized intersections located within this section. Section 4 has two lanes in each direction with varying widths from Mattis Avenue to Duncan Road, while just west of Duncan Road and beyond, this section has one lane in each direction. Roadway markings are present (shown in **Figure 6**). Bradley Avenue passes over Interstate 57 in the approximate middle of this section. The speed limit of this section is 35 mph, and speed limit signs are present in both directions along this section. Section 4 has an approximately four-foot-wide sidewalk on both sides of the Bradley Avenue roadway. At the time of this report, there is ongoing construction in this section. The road surface condition is good to average in Section 4. 

{{<image src="Figure6.png" 
    caption="Figure 6 - Bradley Avenue Section 4"
    position="medium" 
    alt=" ">}}

From 2015 to 2019, 124 crashes occurred within Section 4. **Table 6** provides a summary of the number of crashes and severity levels at each location.

{{<accordion>}}
  {{<accordion-content title="Summary of crashes by crash location and severity level (Section 4)">}}
    <rpc-table url="Table6.csv" table-title="Table 6 - Summary of crashes by crash location and severity level (Section 4)"> </rpc-table>
  {{</accordion-content>}}
{{</accordion>}}

No fatal crashes and four A-Injury crashes occurred within this section. Three A-Injury crashes occurred at intersections, while the other crash occurred mid-block between Crestwood Drive and Bluegrass Lane. Speeding was also an issue here, according to the crash data. The Country Fair Drive intersection—a signalized intersection—is the location with the highest number of crashes. The road section between Country Fair Drive and Pomona Drive had the highest number of mid-block crashes in Section 4. Implementing a road diet between Mattis Avenue and Duncan Road and installing flashing yellow signals at the signalized intersections will help improve road users’ safety. 

## Section 5: Staley Road - County Road 600 E

Section 5 is approximately 1.9 miles long. This area's land uses are primarily agricultural, with a few residential houses. The roadway within this section functionally operates as a local street. There are two unsignalized intersections located within this section. Section 5 has one lane in each direction with no shoulder, and road markings are absent. The speed limit of this section is 55 mph, and speed limit signs are absent along this section. Sidewalks are not present. The road surface condition is poor to average in this section. 

From 2015 to 2019, four crashes occurred within Section 5. **Table 7** provides a summary of the number of crashes and severity levels at each location.

{{<accordion>}}
  {{<accordion-content title="Summary of crashes by crash location and severity level (Section 5)">}}
    <rpc-table url="Table7.csv" table-title="Table 7 - Summary of crashes by crash location and severity level (Section 5)"> </rpc-table>
  {{</accordion-content>}}
{{</accordion>}}

A total of four crashes occurred within this section, all of which were mid-block crashes. No fatal crashes and one A-Injury crash occurred. All other crashes were no-injury crashes. As this section serves as a local road and has less average annual daily traffic than the other sections, a lower number of crashes within the study time is expected.  

## Findings and Proposed Countermeasures

### Study Findings

After analyzing the crash data and site investigation results, several risk factors were identified. The identified findings are listed below:

- Bike facilities are largely absent within this corridor.
- Large vehicles frequently obstruct the views of other drivers.
- Drivers are prone to speeding in the eastern part of the corridor.
- This corridor’s access spacings are not in line with CUUATS guidelines.
- Two crashes were related to rail crossings.
- Rear-end and turning crashes were the predominant types of crashes in the study period.
- Most of the crashes occurred at intersections.

### Proposed Countermeasures

#### (1) Road Diet

The review of crash reports showed that several crashes occurred due to speeding, including one fatal crash. Currently, Bradley Avenue has two lanes in each direction from Oak Street to just west of Duncan Road. The average annual daily traffic (AADT) of Bradley Avenue between Oak Street and Duncan Road is between 9,500 and 15,600. According to the Federal Highway Administration (FHWA), a roadway with average daily traffic between 10,000 and 15,000 is a good candidate for a road diet in many instances [^3]. However, the average daily traffic in this corridor is expected to increase in the future. Because of this, the City of Champaign may conduct intersection analyses and consider signal retiming at intersection locations, in conjunction with a possible road diet implementation. This consideration may be done either with the current average daily traffic, or with a detailed corridor study if the average daily traffic is between 15,000-20,000 at the time of road diet consideration. This road diet can extend to the east of Oak Street. The corridor between Mattis Avenue and Goodwin Avenue was also identified as a potential road diet candidate in the **Champaign Bicycle Vision Plan** [^4]. This will calm traffic along Bradley Avenue. A lane reduction would also require changing the intersection control at McKinley Avenue and Bradley Avenue to a traffic signal or a roundabout, due to the resulting delays and queueing that would result from reduced lanes with the current stop control. 

#### (2) Bike Lanes

As noted above, no bike facilities are available for the majority of this corridor. The Bicycle Vision Plan recommended bike lanes on Bradley Avenue from Perimeter Road east to Goodwin Avenue in Urbana [^3]. If a road diet is implemented, bike lanes can be provided within the roadway along this corridor. 

#### (3) Access Control

There are too many access points along this corridor, especially east of Mattis Avenue. Most of these accesses are located in the vicinity of intersections. Driveways and median openings should be limited as much as possible within intersections’ functional areas, according to CUUATS Access Management Guidelines [^2]. Four signalized intersections are located within a 0.42 mile span between Market Street and State Street. This is well above the signalized access spacing guidelines. As many private properties are located within the corridor, these result in many private driveways along Bradley Avenue. Drivers must be exceptionally cautious while driving on Bradley Avenue, in case the vehicles ahead of them stop unexpectedly due to a vehicle turning from or onto a neighborhood access. This situation should be avoided to minimize conflicts and maintain a safe environment for drivers. The City of Champaign should conduct a detailed access management study and consolidate some of these accesses to provide a safe and comfortable environment for roadway users.  

#### (4) Traffic Signal Modernization

No flashing yellow signals are available for permissive left-turning movement at any signalized intersections in this corridor. Flashing yellow signals mean drivers must yield to pedestrians and oncoming traffic before turning left. Many turning crashes occurred at signalized intersections. Adding flashing yellow signals for left-turn movement would improve safety and signal visibility at these intersections. This countermeasure has a crash modification factor (CMF) of 0.857 for left-turn crashes [^5], meaning that it should reduce the likelihood of left-turn crashes to 85.7% of the current frequency. Adding a signal head for each lane is also recommended.

#### (5) Bus Bays
There are many bus stops within the Bradley Avenue corridor. If a road diet is implemented, buses will frequently stop at the bus stops, blocking traffic flow and increasing the risk of rear-end crashes. The goal of a bus bay is not to block traffic while the bus is stopped. Therefore, installing bus bays at suitable locations along this corridor is recommended to minimize conflicts.

## References
[^1]: Illinois Department of Transportation. (https://www.gettingaroundillinois.com/Traffic%20Counts/index.html)
[^2]: Access Management Guidelines for the Urbanized Area (https://ccrpc.org/wp-content/uploads/2015/03/access-management-2013-04-17-final.pdf)
[^3]: Federal Highway Administration (https://safety.fhwa.dot.gov/road_diets/resources/pdf/fhwasa17021.pdf)
[^4]: Bicycle Vision Plan (http://champaignil.gov/wp-content/uploads/2008/02/tmp-bicycle_vision.pdf)
[^5]: CMF Clearinghouse (http://www.cmfclearinghouse.org/detail.cfm?facid=7730)




