---
title: "Urban Segments"
date: 2022-07-08T10:48:01-06:00
draft: false
weight: 60
menu: main
bannerHeading: Urban Segments
bannerText: >
bannerAction: Provide feedback
bannerUrl: /contact
---

In the urban area, three segments were identified as high-priority locations. The locations are shown below:

<rpc-table url="urban_seg.csv" table-title="Urban Segments"> </rpc-table>


