---
title: "University Avenue (Second Street - Sixth Street, Maple Street - Guardian Drive)"
draft: false
weight: 40
bannerHeading: University Avenue (Second Street - Sixth Street, Maple Street - Guardian Drive).
bannerText: >
  University Avenue (Second Street - Sixth Street, Maple Street - Guardian Drive)
bannerAction: Provide Feedback
bannerUrl: /contact
---
## Introduction

This study analyzed traffic safety conditions along **University Avenue** in the cities of Champaign and Urbana, focusing on two segments — **Second Street to Sixth Street** in Champaign (referred to as **Segment 1** in this report), and **Maple Street to Guardian Drive** in Urbana (referred to as **Segment 2** in this report). Illinois Department of Transportation (IDOT) performed work on a portion of road between these two segments—from Wright Street to Cunningham Avenue—in 2021; therefore, this portion was not analyzed. Segment 1 is approximately 0.35 miles long, while Segment 2 is one mile long; both run east-west. For this analysis, Segment 2 was further divided into three sections, which are described later in this document. **Map 1** shows the location of the study segments. 

<iframe src="https://maps.ccrpc.org/ccspi-urban-seg-university/" width="100%"  height="600" allowfullscreen="true"></iframe>

### Segment 1

The surrounding land uses around Segment 1 are a combination of residential and commercial. This segment operates as a minor arterial, with an annual average daily traffic (AADT) of approximately 15,500 to 17,000 vehicles per day, according to IDOT data from 2021 (i). There are no bike lanes along this segment. An approximately five-foot-wide sidewalk is present along both sides of this segment. From Second Street to Third Street, University Avenue has two lanes in each direction, with street parking on both sides. From Third Street to Sixth Street, University Avenue has two lanes in each direction and one central turning lane. There are seven intersections along this segment. **Table 1** shows the geometry and control type of each intersection. "2WSC" represents a two-way stop-controlled intersection, and "Signal" represents a signalized intersection. 

<rpc-table url="Table1.csv" table-title="Table 1 - Intersection geometry and control type (Segment 1)"> </rpc-table>

### Segment 2 

The surrounding land uses around Segment 2 are a combination of residential, commercial, and recreational. This segment operates as a principal arterial and has two lanes in each direction with 12-foot-wide lanes and no shoulders. The roadway features a marked median from Maple Street to Cottage Grove Avenue and a slightly raised median from Cottage Grove Avenue to Guardian Drive. The road surface condition is poor here, and road markings are available but faded in some places. As this roadway serves traffic traveling between the Champaign-Urbana area and Interstate 74, it has a moderately high traffic volume, with an annual average daily traffic (AADT) of 11,200 vehicles per day along its western portion, according to 2021 Illinois Department of Transportation (IDOT) data (i). Along the eastern portion of this segment, AADT was measured as low as 9,650 vehicles per day, according to 2021 IDOT data [^1]. 

There are seven intersections along Segment 2. **Table 2** shows the control type of each intersection. Urbana Park District’s Ambucs Park is located on the north side of this segment and is a major recreational attraction. Champaign-Urbana Mass Transit District (CUMTD) facilities are located on the southwest side of the intersection of Hickory Street and University Avenue. A gas station is located on the southeast side of the intersection of Lierman Avenue and University Avenue. There are no bike lanes present on the roadway along this segment. A sidewalk measuring approximately four feet wide is present from Cottage Grove Avenue to Maple Street on the north side of this segment; this sidewalk extends past the segment to Cunningham Avenue. In addition, a sidewalk measuring approximately five feet wide is present on the south side of this segment for 0.3 miles east of Hickory Street. Utility poles are present along the roadway, but streetlights are not available in this segment.

<rpc-table url="Table2.csv" table-title="Table 2 - Intersection geometry and control type (Segment 2)"> </rpc-table>

## Traffic Safety Analysis

Traffic crash data from 2015 to 2019 were analyzed to identify traffic safety issues and risk factors along these two segments. The traffic crash data were obtained from the IDOT Division of Traffic Safety.

### Segment 1

From 2015 to 2019, 99 reported crashes occurred within this segment. Ninety of these were intersection crashes, and nine were segment crashes. Most crashes (82%) led to no injuries. The predominant crash types for this segment were turning crashes (29%), rear-end crashes (25%), and angle crashes (21%). Table 3 shows the number of crashes in this segment by crash type and year. 

{{<accordion>}}
  {{<accordion-content title="Summary of crashes by crash type and year (Segment 1)">}}
    <rpc-table url="Table3.csv" table-title="Table 3 - Summary of crashes by crash type and year (Segment 1)"> </rpc-table>
  {{</accordion-content>}}
{{</accordion>}}

Table 4 shows the severity levels of these crashes. No fatal crashes and five A-Injury crashes occurred during the study period. The vast majority (82%) of crashes were No-injury crashes.

{{<accordion>}}
  {{<accordion-content title="Summary of crashes by crash type and severity level (Segment 1)">}}
    <rpc-table url="Table4.csv" table-title="Table 4 - Summary of crashes by crash type and severity level (Segment 1)"> </rpc-table>
  {{</accordion-content>}}
{{</accordion>}}

Table 5 and Table 6 summarize the number of crashes of each type and severity by location. Most crashes occurred at University Avenue’s intersections with Third Street, Fourth Street, and Fifth Street. 

{{<accordion>}}
  {{<accordion-content title="Crash types and severity levels by location (Segment 1)">}}
    <rpc-table url="Table5.csv" table-title="Table 5 - Crash locations and crash types (Segment 1)"> </rpc-table>
    <rpc-table url="Table6.csv" table-title="Table 6 - Crash locations and severity levels (Segment 1)"> </rpc-table>
  {{</accordion-content>}}
{{</accordion>}}

A detailed analysis of safety concerns along this segment is provided below.

{{<accordion>}}
  {{<accordion-content title="Safety concerns (Segment 1)">}}

  From Second Street to Third Street, street parking is provided on both sides of University Avenue. Due to the presence of parked cars, drivers may need to shift slightly left to proceed on this segment safely. Three parked motor vehicle crashes (counted as University Avenue and Third Street intersection crashes) occurred on University Avenue between 2015 and 2019. 
  <br></br>
  At the intersection of University Avenue and Third Street, the predominant types of crashes were rear-end crashes, turning crashes, and angle crashes. Rear-end crashes were often related to the need to wait for a turning vehicle. Turning crashes often occurred when a left-turning vehicle collided with a westbound through vehicle, while in the angle crashes, three crashes were related to westbound vehicles, and two were related to eastbound vehicles. The A-Injury crash at this intersection was a turning crash caused by the driver's view being blocked by a truck turning left. 
  <br></br>
  From Third Street to Fourth Street, University Avenue transitions from four lanes (two lanes in each direction) to five lanes (two lanes in each direction and a center left-turn lane). Four crashes occurred here between 2015 and 2019. Three were angle crashes related to vehicles exiting or entering the parking lots on the south side of University Avenue. In two of these crashes, traffic stopped to let the vehicle proceed. These crashes led to one B-Injury.
  <br></br>
  At the intersection of University Avenue and Fourth Street, the predominant crash types were turning crashes, rear-end crashes, and sideswipe same-direction crashes. Most turning crashes occurred when a vehicle on University Avenue was making a left turn and a vehicle on University Avenue was traveling through. Most rear-end crashes were between vehicles traveling on University Avenue. Most sideswipe crashes were on University Avenue, and two were associated with improper U-Turns. Three A-Injury crashes (one angle crash and two turning crashes) occurred at this intersection. The pedestrian crash at this intersection occurred when a pedestrian crossed against the light and without using the crosswalk. 
  <br></br>
  At the intersection of University Avenue and Fifth Street, the predominant types of crashes were angle crashes (eight involving a southbound vehicle and three involving a northbound vehicle) and turning crashes (including both left-turn vehicles and right-turn vehicles). The A-Injury crash at this intersection was a turning crash caused by driver distraction. 

  {{</accordion-content>}}
{{</accordion>}}

### Segment 2

From 2015 to 2019, 87 reported crashes occurred within this segment. Seventy-six of these were intersection crashes, and 11 were segment crashes away from intersections. Most crashes (71%) resulted in no injuries. The predominant crash types for this segment were turning and rear-end crashes, which accounted for 64% of all crashes. Thirty-four crashes (39%) at this location occurred due to wet or icy roadway conditions. Table 7 presents the number of crashes in this segment by crash type and year. Table 8 shows the number of crashes by crash type and severity level. There were no fatal crashes and four A-Injury crashes in the five-year analysis period along this segment. The crash locations and causes in this period varied significantly, with overwhelming concentrations at the intersections of University Avenue with Maple Street and Guardian Drive. Twenty-five of the crashes on this Segment (29%) occurred at nighttime.

{{<accordion>}}
  {{<accordion-content title="Summary of crashes by crash type and year (Segment 2)">}}
    <rpc-table url="Table7.csv" table-title="Table 7 - Summary of crashes by crash type and year (Segment 2)"> </rpc-table>
  {{</accordion-content>}}
{{</accordion>}}

{{<accordion>}}
  {{<accordion-content title="Summary of crashes by crash type and severity level (Segment 2)">}}
    <rpc-table url="Table8.csv" table-title="Table 8 - Summary of crashes by crash type and severity level (Segment 2)"> </rpc-table>
  {{</accordion-content>}}
{{</accordion>}}

Segment 2 was divided into three sections for this study, as shown in **Map 2**. 

{{<image src="Map2.png" caption="Map 2 - Location of different sections along University Avenue (Maple Street – Guardian Drive)" position="full">}}

**Table 9** presents the location and severity level of all crashes on this segment.

{{<accordion>}}
  {{<accordion-content title="Summary of Crash locations and severity levels (Segment 2)">}}
    <rpc-table url="Table9.csv" table-title="Table 9 - Summary of Crash locations and severity levels"> </rpc-table>
  {{</accordion-content>}}
{{</accordion>}}

#### Section 1: Maple Street - Cottage Grove Avenue

Section 1 is the westernmost section of Segment 2 and is approximately 0.35 miles long. The land uses of this section are a mix of commercial and residential, which results in many accesses (i.e., driveways) along this section. The road runs east-west and has a slight curve on the eastern portion of Section 1. The width of the roadway is approximately 54 feet, with two 12-foot lanes per direction (**Figure 1**) and a four-foot-wide marked median in the center (measured from an aerial image). The speed limit is 40 miles per hour (mph) for most of this section. Speed limit signs are present in both directions within Section 1. A speed limit sign indicating 45 mph is present west of Cottage Grove Avenue for eastbound traffic. The road surface condition is very poor in this section. The Champaign-Urbana Mass Transit District garage is located on the south side of the intersection of Hickory Street and University Avenue.

{{<image src="Figure1.png" caption="Figure 1- University Avenue section 1" position="full">}}

From 2015 to 2019, four segment crashes and 29 intersection crashes occurred within Section 1. Sixty-three percent of the crashes resulted in no injuries. An A-Injury crash on the Maple Street to Sycamore Street segment occurred when a driver failed to stop to avoid a crash. The other three segment crashes were No-Injury crashes. Eight B-Injury crashes, three C-Injury crashes, and 17 No-Injury crashes occurred in this section's intersections. One B-Injury crash was pedestrian related. Most of the crashes were angle or turning-related crashes, which are the typical crash types for intersections. The most problematic intersection within this section is located at Maple Street, where 22 crashes occurred, including 12 turning and seven angle crashes. The primary issue at this intersection was vehicles attempting to cross University Avenue. Crossing the two-way stop-controlled intersection with stop signs on the north-south leg and making left-turn movements for eastbound and westbound traffic proved problematic, due to failure to yield right of way to the cross-traffic or through traffic in many crashes.

Providing a dedicated left-turn lane for the east and west leg at Maple Street and University Avenue will remove left-turning vehicles from the flow of through traffic, reducing the possibility of turning crashes. Installing "Cross traffic does not stop" signs on the stop-controlled legs of this intersection will also guide drivers attempting to cross University Avenue in the north and south directions.

#### Section 2: Cottage Grove Avenue – Lierman Avenue

Section 2 is also approximately 0.35 miles long. The land uses of this section are a mix of commercial and recreational. The roadway has two lanes in each direction, left turning lanes for different accesses, and a slightly raised median within this section (**Figure 2**). Total roadway width increases to 66 feet (measured from an aerial image) on the eastern side of this section. Ambucs Park is located on the north side of  Section 2. The speed limit of this section is 45 mph. 

{{<image src="Figure2.png" caption="Figure 2- University Avenue section 2" position="full">}}

From 2015 to 2019, five segment crashes and two intersection crashes occurred within this section. Fifty-seven percent of the crashes resulted in no injuries. One A-Injury segment crash occurred due to impaired driving, and one C-Injury segment crash was a rear-end crash. Three No-Injury segment crashes occurred within this section. One B-Injury crash and one No-Injury crash occurred at the intersection of Lierman Avenue and University Avenue. The B-Injury crash occurred due to impaired driving. Most of the crashes in this section were rear-end crashes. The primary concern along this section was speeding. 

#### Section 3: Lierman Avenue – Guardian Drive

Section 3 is approximately 0.30 miles long, and this section also has two lanes per direction with left-turning lanes for different accesses. The land uses of this section are a mix of commercial and residential. The total width of the roadway is approximately 66 feet (measured from an aerial image), but the width increases as the roadway approaches Guardian Drive in the east. The roadway has a raised median (**Figure 3**). The speed limit here is 45 mph, and speed limit signs are present in both directions within Section 3. There is a gas station present on the western end of this section. 

{{<image src="Figure3.png" caption="Figure 3- University Avenue section 3" position="full">}}

From 2015 to 2019, 46 crashes occurred within Section 3, including two segment crashes and 44 intersection crashes. One of the segment crashes—a motor bike-related crash—resulted in a B-Injury. The other segment crash was a No-Injury crash. All of the intersection crashes occurred at Guardian Drive, including two A-Injury crashes, one B-Injury crash, five C-Injury crashes, and 36 No-Injury crashes. In 26 of these intersection crashes, drivers were driving westbound, mostly from the freeway. Speeding and failure to yield right of way were the primary concerns in this section. Some crashes occurred due to icy roadway conditions. Traffic signal modernization and pavement friction management would improve safety for roadway users along Section 3.

## Findings and Proposed Countermeasures

### Study Findings

Several risk factors were identified after analyzing the crash data and site investigation results. The findings are listed below:

#### Segment 1

- There are no left-turn lanes on the east and west legs of the intersection of Third Street and University Avenue.
- There is no flashing yellow signal for left-turn movement at the intersection of Fourth Street and University Avenue.
- Turning, rear-end, and angle crashes were the predominant crash types.

#### Segment 2

- There are no streetlights within this roadway segment, and 29%  of crashes occurred at nighttime.
- Road surface condition is very poor.
- No bike facilities are available within this segment, and sidewalks for pedestrians are only partially available.
- There are no left-turn lanes on the east and west legs of the Maple Street and University Avenue intersection.
- Speeding is an issue within this segment, especially for westbound traffic coming from the freeway.
- The western portion of this segment has several access openings via private driveways.
- Rear-end crashes and turning crashes were the predominant crash types.

### Proposed Countermeasures

#### Segment 1

##### (1)	Left Turning Lane

The intersection of Third Street and University Avenue is a two-way stop-controlled intersection and had five turning crashes during the five-year study period. In addition, many rear-end crashes that occurred at this intersection were also associated with turning—vehicles stopped in traffic to wait for other vehicles to turn left. For the east leg (Fourth Street to Third Street), providing a westbound left-turn lane is recommended. For the west leg (Second Street to Third Street), providing an eastbound left-turn lane is recommended, along with limiting the number of metered parking spaces near the intersection of Third Street and University Avenue. Installing left-turning lanes will remove left-turning vehicles from the flow of through traffic, which will help reduce both turning crashes and rear-end crashes at this intersection. 

##### (2)	Traffic Signal Modernization 

The intersection of Fourth Street and University Avenue is a signalized intersection and was the intersection with the most crashes within this segment. Many turning crashes occurred when an eastbound or westbound left-turning vehicle collided with a westbound or eastbound vehicle. Currently, there are no flashing yellow signals for left-turn movements at this intersection. Flashing yellow arrow signals direct drivers to yield to pedestrians and oncoming traffic before turning left. This would improve safety at the signalized intersection. This countermeasure has a CMF of 0.857 for left-turn crashes [^2], meaning that it should reduce left-turn crashes to 85.7% of their current frequency. 

#### Segment 2

##### (1)	Improvement of Roadway Markings and Signs

As identified in the Introduction, some road markings along Segment 2 are faded and cannot be seen from a distance; improvement of road markings is recommended along this road segment.
"Cross traffic does not stop" signs are not present on the stop-controlled legs within this segment. To remind drivers on Maple Street to stop completely, look carefully, and proceed only when it is safe, adding stop sign companion “Cross traffic does not stop" signs is recommended. This can be done on all of the stop-controlled legs.

##### (2)	Access Control

Segment 2 operates as a principal arterial, and according to CUUATS Access Management Guidelines, no driveway accesses should be allowed onto a principal arterial [^3]. This is because principal arterials carry high volumes of traffic and provide long-distance, high-speed travel with the highest level of mobility. The western portion of this segment has several private driveways providing direct property access from a principal arterial, which is not recommended. The City of Urbana should consider consolidating some driveway access openings within this segment.

##### (3)	Improvement of Pavement Conditions

Road surface condition is very poor in Segment 2. Resurfacing is recommended along this segment to improve the comfort level of roadway users. In addition, 34 out of 87 crashes (39%) at this location occurred due to wet or icy roadway conditions. For this reason, pavement friction management should be considered here. Pavement friction management includes providing adequate and durable pavement properties as well as collecting data and performing analysis to ensure the effectiveness of this countermeasure [^4]. The primary purpose of this management is to minimize pavement friction-related crashes. High friction surface treatment (HFST), which will increase friction and skid resistance, can be implemented at this intersection. This countermeasure has a CMF of 0.8 for intersection crashes [^5]. However, this countermeasure has a low service life and needs to be maintained periodically.

##### (4)	Left Turning Lane

The intersection of Maple Street and University Avenue is a dangerous crossing point where 22 crashes occurred within the study period, including 12 turning crashes. One of the primary issues here was left-turning movement for eastbound and westbound traffic, as many crashes happened due to failure to yield to through traffic. Providing a dedicated left-turning lane at the east and west leg of this intersection will remove left-turning vehicles from the flow of through traffic, reducing the possibility of turning crashes at this intersection. This countermeasure has a CMF of 0.79 for all crashes [^6]. According to the current intersection geometry, it is impossible to facilitate an extra left turning lane without extending the roadway width. 

##### (5)	Sidewalk and Bike Facility

A narrow sidewalk is present from Maple Street to Cottage Grove Avenue on the north side of University Avenue, and a five-foot-wide sidewalk is present on the south side of University Avenue from Hickory Street to 0.3 miles east. No other bicyclist or pedestrian facilities exist within this study segment. Therefore, constructing sidewalks on the south and north sides of this segment is recommended, providing sidewalks where they are not present and widening the existing sidewalks to provide better pedestrian access. Providing a shared-use path on at least one side of this roadway is also recommended, which would facilitate both bike and pedestrian activity.

##### (6)	Traffic Signal Modernization

The only traffic signal on this segment is present at the Guardian Drive and University Avenue intersection. No flashing yellow signal is available for permissive left-turning movement at this intersection. Flashing yellow signals mean drivers must yield to pedestrians and oncoming traffic before turning left. Adding flashing yellow signals for left turning movement would improve safety and signal visibility at the signalized intersection. This countermeasure has a CMF of 0.857 for left turn crashes [^2]. 

##### (7)	Street Lighting

As mentioned in the Introduction, streetlights are absent on Segment 2. In addition, the Traffic Safety Analysis found that 25 out of 87 crashes on this segment (29%) occurred at nighttime. In some crash reports, the darkness of the road was also mentioned. Therefore, installing streetlights along University Avenue in the vicinity of this segment is recommended. This would improve drivers' safety at night. This countermeasure has a CMF of 0.68 for all crashes in urban and suburban areas [^7].

## References

[^1]: Illinois Department of Transportation (https://www.gettingaroundillinois.com/Traffic%20Counts/index.html)
[^2]: CMF Clearinghouse (http://www.cmfclearinghouse.org/detail.cfm?facid=7730)
[^3]: Access Management Guidelines for the Urbanized Area (https://ccrpc.org/wp-content/uploads/2015/03/access-management-2013-04-17-final.pdf)
[^4]: Federal Highway Administration Friction Management (https://safety.fhwa.dot.gov/roadway_dept/pavement_friction/friction_management/#:~:text=Pavement%20friction%20management%20includes%20providing,the%20effectiveness%20of%20the%20program)
[^5]: Federal Highway Administration (https://safety.fhwa.dot.gov/provencountermeasures/pavement-friction.cfm)
[^6]: CMF Clearinghouse (https://www.cmfclearinghouse.org/detail.cfm?facid=3948)
[^7]: CMF Clearinghouse (https://www.cmfclearinghouse.org/detail.cfm?facid=11026#commentanchor)





