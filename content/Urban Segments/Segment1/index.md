---
title: "Lincoln Avenue (Oaks Road - Eads Street)"
draft: false
weight: 20
bannerHeading: Lincoln Avenue (Oaks Road - Eads Street).
bannerText: >
  Lincoln Avenue (Oaks Road - Eads Street)
bannerAction: Provide Feedback
bannerUrl: /contact
---
## Introduction

### Background
This study was performed to analyze transportation safety along **Lincoln Avenue (from Oaks Road to Eads Street)**.  This segment is approximately 1.7 miles long and runs north-south with no curves. The following map shows the location of the study segment. 

<iframe src="https://maps.ccrpc.org/ccspi-urban-seg-lincoln/" width="100%"  height="600" allowfullscreen="true"></iframe>

The surrounding land uses around Lincoln Avenue from Interstate-74 going north to Oaks Road are primarily agricultural and industrial. Traffic volume on this part of Lincoln Avenue is relatively moderate, with an AADT of about 3,050-6,650 vehicles, according to Illinois Department of Transportation (IDOT) data from 2021 [^1]. The surrounding land use of Lincoln Avenue from Interstate-74 going south to Eads Street are primarily commercial and residential. Traffic volume on this part of Lincoln Avenue is high, with an AADT of about 10,400-16,200 vehicles, according to Illinois Department of Transportation (IDOT) data from 2021  [^1]. 

There are 13 intersections along the study segment. The control types of these intersections are listed in **Table 1**. For this study, the entire segment was divided into three sections: (1) Oaks Road - Interstate -74 Ramp (2) Interstate -74 Ramp - Bradley Ave (3) Bradley Ave – Eads Street. Detailed safety analysis for each section will be provided in the following chapters.

{{<accordion>}}
  {{<accordion-content title="Table 1 - Location and control type">}}
    <rpc-table url="Table1.csv" table-title="Table 1 - Location and control type"> </rpc-table>
  {{</accordion-content>}}
{{</accordion>}}

## Transportation Safety Analysis

From 2015 to 2019, there were 179 crashes occurred along Lincoln Avenue from Oaks Road to Eads Street. **Figure 1.1-1.4** summarize the timing of these crashes. Most crashes occurred at intersections. Interstate-74 Ramp at Lincoln Avenue, the intersection of Lincoln Avenue and Kettering Park Drive, and the intersection of Lincoln Avenue and Bradley Ave are locations with the highest number of crashes. 

<rpc-chart url="crash_count_year.csv"
      chart-title="Figure 1.1 - Number of Crashes by Year"
      x-label="Year"
      y-label="Crash Counts"
      type="bar"></rpc-chart>

<rpc-chart url="crash_count_month.csv"
      chart-title="Figure 1.2 - Number of Crashes by Month"
      x-label="Month"
      y-label="Crash Counts"
      type="bar"></rpc-chart>

<rpc-chart url="crash_count_day.csv"
      chart-title="Figure 1.3 - Number of Crashes by Day of the Week"
      x-label="Day"
      y-label="Crash Counts"
      type="bar"></rpc-chart>

<rpc-chart url="crash_count_hour.csv"
      chart-title="Figure 1.4 - Number of Crashes by Hour of the Day"
      x-label="Hour"
      y-label="Crash Counts"
      type="bar"></rpc-chart>

## Section 1: Oaks Road - Interstate-74 Ramp

Section 1 is approximately 0.95 miles long. The land uses of this section are a mix of industry and agriculture. From Oaks Road to Wilbur Road, Lincoln Avenue has two travel lanes. From Wilbur Road to Anthony Drive, Lincoln Avenue changes from two travel lanes with one central turning lane to four travel lanes with one central turning lane. Lincoln Avenue at Interstate-74 ramp has four travel lanes, two left turn lanes and a physical median. 

From 2015 to 2019, there were 53 crashes occurred within this section. **Figure 2** provides a summary of the number of crashes at each location and the year in which they occurred. The two intersections of Interstate-74 Ramp and Lincoln Avenue have the highest number of crashes, most were rear-end crashes and turning crashes. Four B-Injury crashes occurred here, including two rear-end crashes and two turning crashes. **Figure 3** and **Figure 4** summarize the crash types and severity levels of crashes at each location.

<rpc-chart url="crash_year_location_chart_section1.csv"
      chart-title="Figure 2 - Number of Crashes by Year and Location (Oaks Road - Interstate-74 Ramp)"
      y-label="Crash Counts"
      type="barh"
      stacked="true"></rpc-chart>

<rpc-chart url="crash_type_location_chart_section1.csv"
      chart-title="Figure 3 - Number of Crashes by Crash Type and Location (Oaks Road - Interstate-74 Ramp)"
      y-label="Crash Counts"
      type="barh"
      stacked="true"></rpc-chart>

<rpc-chart url="crash_severity_location_chart_section1.csv"
      chart-title="Figure 4 - Number of Crashes by Severity Level and Location (Oaks Road - Interstate-74 Ramp)"
      y-label="Crash Counts"
      type="barh"
      stacked="true"></rpc-chart>

## Section 2: Interstate-74 Ramp - Bradley Avenue

Section 2 is approximately 0.46 miles long. There are several hotels and a gas station located at the north part of this section and there are several apartment complexes located at the south part of this section. Lincoln Avenue from Killarney Street to Kettering Park Drive begins with four travel lanes with one left turn lane and a physical median and changes to four travel lanes. Lincoln Avenue from Kettering Park Drive to Beverly Drive has four travel lanes. From Beverly Drive to Bradley Avenue, Lincoln Avenue has four travel lanes and one left turn lane. 

From 2015 to 2019, there were 57 crashes occurred within this section. **Figure 5** provides a summary of the number of crashes at each location and the year in which they occurred. **Figure 6** and **Figure 7** summarize the crash types and severity levels of crashes at each location.

The intersection of Lincoln Avenue and Kettering Park Drive has the highest number of crashes. The Capstone Quarters apartment complex is located on the west side of this intersection,  a company is located in the northeast quadrant of this intersection and the Hope Center (a Church Care Ministries, including a weekly food pantry) is located in the southeast quadrant. There are bus stops located in the northwest and southeast quadrants (see **Figure 8.1-8.2**). Some passengers may cross in the middle of the road. Woodlawn Cemetry is located about 0.15 miles east of this intersection. Rear-end crash is the major type of crash at this intersection and most crashes were associated with a vehicle entering or leaving the residential area, which is also the major cause of other crashes that occurred within this section. There were also many turning crashes at this intersection, half occurred when vehicles were turning left to enter/exit the residential areas and half occurred when vehicles were turning left to enter/exist Kettering Park Drive. There were four carshes led to A-Injuries at this intersection, including one rear-end crash, two turning crashes and one fixed object crash. 

<rpc-chart url="crash_year_location_chart_section2.csv"
      chart-title="Figure 5 - Number of Crashes by Year and Location (Interstate-74 Ramp - Bradley Avenue)"
      y-label="Crash Counts"
      type="barh"
      stacked="true"></rpc-chart>
  
<rpc-chart url="crash_type_location_chart_section2.csv"
      chart-title="Figure 6 - Number of Crashes by Crash Type and Location (Interstate-74 Ramp - Bradley Avenue)"
      y-label="Crash Counts"
      type="barh"
      stacked="true"></rpc-chart>

<rpc-chart url="crash_severity_location_chart_section2.csv"
      chart-title="Figure 7 - Number of Crashes by Severity Level and Location (Interstate-74 Ramp - Bradley Avenue)"
      y-label="Crash Counts"
      type="barh"
      stacked="true"></rpc-chart>

{{<image src="DSC_0666.JPG" 
    caption="Figure 8.1 - Bus stop located in the northwest quadrant of the intersection of Lincoln Avenue and Kettering Park Drive"
    position="medium" 
    alt=" ">}}

{{<image src="DSC_0651.JPG" 
    caption="Figure 8.2 - Bus stop located in the southeast quadrant of the intersection of Lincoln Avenue and Kettering Park Drive"
    position="medium" 
    alt=" ">}}


## Section 3: Bradley Avenue – Eads Street

Section 3 is approximately 0.29 miles long. There is a gas station and a church located at the north end of this section, and the roadway has four travel lanes, one left turn lane and a physical median. The south end of this section has four travel lanes and the surrounding land use is mainly residential. There are bus stops located on both sides of the segment. Again, some passengers may cross in the middle of the road.

From 2015 to 2019, there were 69 crashes occurred within this section. **Figure 9** provides a summary of the number of crashes at each location and the year in which they occurred. **Figure 10** and **Figure 11** summarize the crash types and severity levels of crashes at each location. Rear end crashes were the main crash type of this section, most were associated with vehicles stopping in heavy traffic and/or waiting for a vehicle turning into the nearby residential area. Most turning crashes were also associated with vehicles turning to entering or leaving nearby residential area. 

The intersection of Lincoln Avenue and Bradley Avenue has the highest number of crashes. **Figure 12.1-12.2** show the two bus stops located in the northwest and southeast quadrants. The major types of crashes at this intersection are rear-end crashes, angle crashes, and sideswipe same direction crashes. Between 2015 and 2019, fifteen rear-end crashes occurred at this intersection, most were related to northbound vehicles. Regarding the angle crashes, 7/8 of them were associated with drivers disregarding traffic signals.  All the six sideswipe same direction crashes were related to lane changing behavior.  

<rpc-chart url="crash_year_location_chart_section3.csv"
      chart-title="Figure 9 - Number of Crashes by Year and Location (Bradley Avenue – Eads Street)"
      y-label="Crash Counts"
      type="barh"
      stacked="true"></rpc-chart>

<rpc-chart url="crash_type_location_chart_section3.csv"
      chart-title="Figure 10 - Number of Crashes by Crash Type and Location (Bradley Avenue – Eads Street)"
      y-label="Crash Counts"
      type="barh"
      stacked="true"></rpc-chart>

<rpc-chart url="crash_severity_location_chart_section3.csv"
      chart-title="Figure 11 - Number of Crashes by Severity Level and Location (Bradley Avenue – Eads Street)"
      y-label="Crash Counts"
      type="barh"
      stacked="true"></rpc-chart>

{{<image src="DSC_0609.JPG" 
    caption="Figure 12.1 - Bus stop located in the northwest quadrant of the intersection of Lincoln Avenue and Bradley Avenue"
    position="medium" 
    alt=" ">}}

{{<image src="DSC_0600.JPG" 
    caption="Figure 12.2- Bus stop located in the southeast quadrant of the intersection of Lincoln Avenue and Bradley Avenue"
    position="medium" 
    alt=" ">}}

## Findings and Proposed Countermeasures

Risk factors are identified based on the crash data analysis and site investigation results. The identified safety issues and the proposed countermeasures are listed as follows.

### (1) Road Diet

There are many access points along this segment, especially from Kettering Park Drive to Eads Street. Drivers have to be very careful when driving on Lincoln Avenue in case the vehicles in front of them stop abruptly or a vehicle enters/exits a residential area. Conducting a detailed safety study and evaluating the possibility to implement a road diet to reduce four travel lanes to two travel lanes and one center turning lane is recommended. 

### (2) Midblock Crosswalks

There are bus stops in the vicinity of the residential areas. Although it is not able to draw conclusions about the impact of passengers and bus stops based on the available crash reports, it was observed that some passengers crossed in the middle of the road, which was dangerous and could lead to rear-end crashes. Therefore, it is recommened to conduct an assessment of the safety impact of the bus stops and consider installing mid-block crossings if a road diet is implemented  to safely facilitate the crossing of pedestrians and reduce the number of rear-end crashes along Lincoln Avenue.

### (3) Bike Lanes

Providing bike lanes along Lincoln Avenue from Killarney Street to Wascher Drive is also recommended. This would improve bicyclists’ safety and comfort and enhance connectivity. There are on-street bike facilities on Bradley Avenue from Lincoln Avenue to the Urbana city limits, but there are no bike facilities along Lincoln Avenue to facilitate accessibility and connectivity to the residents (many of them are students) living in this section of Lincoln Avenue.

## References
[^1]: Illinois Department of Transportation. (https://www.gettingaroundillinois.com/Traffic%20Counts/index.html)



