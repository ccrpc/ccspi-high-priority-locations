---
title: "Overview"
draft: false
menu: main
weight: 10
bannerHeading: Safety Plan Implementation
bannerText: >
bannerAction: Provide Feedback
bannerUrl: /contact
---

Champaign County supports the statewide safety goal of zero roadway fatalities. Champaign County agencies are committed to reducing fatalities and serious injuries on all public roads. The Champaign County Regional Planning Commission (CCRPC) is colloborating with the CUUATS Safety Committee members to implement two separate safety plans for the urban and rural areas of Champaign County. The Safety Plans are strategic, data-driven, and multidisciplinary. These plans also established performance-based measures and targets for the urban and rural areas of Champaign County. 

This project aims to implement the Safety Plans by providing a detailed safety analysis of specific locations. Detailed improvement recommendations were included, allowing the responsible agencies to make informed decisions to improve safety at the identified locations. These recommendations reflect the guidance from the CUUATS Access Management Guidelines for the Urbanized Area, the Manual of Uniform Traffic Control Devices (MUTCD), the CUUATS Complete Street Policy, the Illinois Strategic Highway Safety Plan, and other related resources. The recommendations address not only existing issues but also the possible impacts of potential future land use development. 
