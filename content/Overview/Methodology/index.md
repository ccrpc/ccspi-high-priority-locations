---
title: "Methodology"
draft: false
weight: 10
bannerHeading: Safety Plan Implementation
bannerText: >
  This project aims to implement recommendations from the Urban and Rural Safety Plans through conducting detailed safety analysis for specific locations.
bannerAction: Provide Feedback
bannerUrl: /contact
---

## High-Priority Locations Selection

Identification of high-priority intersections and segments was completed separately for the rural and urban areas. The urban area is defined as the area within the Champaign-Urbana Metropolitan Planning Area (MPA) boundary (seen below). The rural area is the area outside of the MPA and within Champaign County. A priority index methodology was developed as a component of the safety plans. Using this methodology, high-priority intersections and segments were determined based on crash frequency, equivalent crashes (the number of crashes, weighted by each crash’s severity), and crash frequency per length of segment (for segments only). When selecting the top priority locations, crash frequency and highest equivalent crashes were used as tiebreakers in the case of similar priority index values. If this did not result in clear top priority locations, then the top locations were chosen based on engineering judgement and discussion with the CUUATS Safety Committee members.

<iframe src="https://maps.ccrpc.org/ccspi-mpa-boundaries/" width="100%" height="600" allowfullscreen="true"></iframe>

## Safety Analysis

After identifying and selecting the high-priority locations, a detailed safety analysis was conducted for each location. The detailed safety analysis included historic crash analysis, as well as site analysis including study of geometric design, pavement conditions, and traffic operation. Detailed recommendations for transportation safety improvements were then provided, so the responsible agencies can make informed decisions at the identified locations. The intent of these improvements is not only to address existing issues, but also to address the potential impacts of future land use developments.
