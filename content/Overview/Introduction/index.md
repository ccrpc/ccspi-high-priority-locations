---
title: "Introduction"
draft: false
weight: 10
bannerHeading: Safety Plan Implementation
bannerText: >
  This project aims to implement recommendations from the Urban and Rural Safety Plans through conducting detailed safety analysis for specific locations.
bannerAction: Provide Feedback
bannerUrl: /contact
---

## Background

The members of the Champaign Urbana Urbanized Area Transportation Study (CUUATS)—the Metropolitan Planning Organization (MPO) for the Champaign-Urbana region—support the Illinois statewide safety goal of zero roadway fatalities. CUUATS agencies are committed to reducing fatalities and serious injuries on all public roads. The CUUATS Technical and Policy Committees—as well as the CUUATS Safety Committee members—approved the [Champaign-Urbana Urban Area Safety Plan](https://ccrpc.org/wp-content/uploads/2019/11/Urban-Safety-Plan_2019-11-25.pdf) and the [Champaign County Rural Area Safety Plan](https://ccrpc.org/wp-content/uploads/2020/08/Revised-Final-Rural-Safety-Plan_2020-08-20.pdf) in December 2019.

## Objectives

The intent of this project is to begin implementing the Champaign-Urbana Urban Area Safety Plan and the Champaign County Rural Area Safety Plan by conducting detailed safety analysis and providing specific safety recommendations regarding high-priority locations. These locations consist of intersections and segments that were identified as high-priority locations using five-year (2014-2018) crash data. In the rural area, five high-priority intersections and segments were selected from county and township roadways. Similarly, thirteen high-priority urban intersections and segments were selected from city, township, and county roadways.
