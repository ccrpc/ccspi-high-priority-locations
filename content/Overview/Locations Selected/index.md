---
title: "Locations Selected"
draft: false
weight: 30
bannerHeading: Safety Plan Implementation
bannerText: >
  This project aims to implement recommendations from the Urban and Rural Safety Plans through conducting detailed safety analysis for specific locations.
bannerAction: Provide Feedback
bannerUrl: /contact
---

## Rural Segments and Intersections

In the rural area, one intersection and four segments were selected as high-priority locations from county and township roadways. The locations are shown below:

<rpc-table url="rural_int.csv" table-title="Rural Unsignalized Intersection"> </rpc-table>

<rpc-table url="rural_seg.csv" table-title="Rural Segments"> </rpc-table>

<iframe src="https://maps.ccrpc.org/ccspi-rural-locations/" width="100%" height="600" allowfullscreen="true"></iframe>

## Urban Segments and Intersections

In the urban area, ten intersections and three segments were selected as high-priority locations from city, township, and county roadways. The locations are shown below:

<rpc-table url="urb_unsig.csv" table-title="Urban Unsignalized Intersections"> </rpc-table>

<rpc-table url="urb_sig.csv" table-title="Urban Signalized Intersections"> </rpc-table>

<rpc-table url="urb_seg.csv" table-title="Urban Segments"> </rpc-table>

<iframe src="https://maps.ccrpc.org/ccspi-urban-locations/" width="100%" height="600" allowfullscreen="true"></iframe>